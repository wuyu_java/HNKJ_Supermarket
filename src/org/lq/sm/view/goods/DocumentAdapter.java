package org.lq.sm.view.goods;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
/**
 * 	适配器设计模式
 * @author 杨培林
 * @phone 13603738558
 * 
 * @package org.lq.sm.view.goods
 * @date 2020年8月11日上午10:15:40
 *
 */
public abstract class DocumentAdapter implements DocumentListener {
	
	@Override
	public void changedUpdate(DocumentEvent e) {
	}
	
	@Override
	public void insertUpdate(DocumentEvent e) {
	}
	@Override
	public void removeUpdate(DocumentEvent e) {
	}
	
}
