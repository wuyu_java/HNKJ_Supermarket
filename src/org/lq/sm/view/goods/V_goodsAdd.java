package org.lq.sm.view.goods;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.Document;

import org.lq.sm.entity.Dictionary;
import org.lq.sm.entity.Goods;
import org.lq.sm.entity.TypeID;
import org.lq.sm.service.DictionaryService;
import org.lq.sm.service.GoodsService;
import org.lq.sm.service.iml.DictionaryServiceImpl;
import org.lq.sm.service.iml.GoodsServiceImpl;
import org.lq.sm.util.CastUtil;
import org.lq.sm.util.FileUpload;
import org.lq.sm.util.HttpUtil;
import org.lq.sm.util.PinYinUtil;
import org.lq.sm.view.dict.V_Dictionary;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import java.awt.event.InputMethodListener;
import java.awt.event.InputMethodEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.beans.PropertyChangeEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;

public class V_goodsAdd extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField txt_number;
	private JTextField txt_name;
	private JTextField txt_pinyin;
	private JTextField txt_purchase_price;
	private JTextField txt_sell_price;
	private JTextField txt_trade_price;
	private JTextField txt_cose_price;
	private JTextField txt_inventory;
	private JTextField txt_min_incentory;
	private JTextField txt_max_inventory;
	private JTextField txt_integral;
	private JTextField txt_sell_num;
	private JComboBox cb_type;
	private JComboBox cb_brand;
	private JComboBox cb_unit;
	private File selectFile;
	private JTextField txt_manuName;
	private JTextField txt_manuAddress;
	private JEditorPane txt_node;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_goodsAdd frame = new V_goodsAdd();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_goodsAdd() {
		setResizable(false);
		setTitle("商品添加");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 575, 418);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("序号:");
		label.setBounds(10, 25, 54, 15);
		contentPane.add(label);
		
		textField = new JTextField();
		textField.setEditable(false);
		textField.setBounds(74, 22, 66, 21);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel label_1 = new JLabel("编 号 :");
		label_1.setBounds(10, 60, 54, 15);
		contentPane.add(label_1);
		
		txt_number = new JTextField();
		txt_number.setBounds(74, 57, 105, 21);
		contentPane.add(txt_number);
		txt_number.setColumns(10);
		
		JLabel label_2 = new JLabel("名 称 :");
		label_2.setBounds(236, 60, 54, 15);
		contentPane.add(label_2);
		
		txt_name = new JTextField();
		//获取当前文本框中内容的对象
		Document doc = txt_name.getDocument();
		//添加内容改变事件
		doc.addDocumentListener(new DocumentAdapter() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				String py = PinYinUtil.getFirstPY(txt_name.getText());
				txt_pinyin.setText(py);
			}
			@Override
			public void removeUpdate(DocumentEvent e) {
				String py = PinYinUtil.getFirstPY(txt_name.getText());
				txt_pinyin.setText(py);
			}
		});
		txt_name.setBounds(300, 57, 95, 21);
		contentPane.add(txt_name);
		txt_name.setColumns(10);
		
		JLabel label_3 = new JLabel("拼音简码:");
		label_3.setBounds(236, 89, 54, 15);
		contentPane.add(label_3);
		
		txt_pinyin = new JTextField();
		txt_pinyin.setEditable(false);
		txt_pinyin.setBounds(300, 86, 95, 21);
		contentPane.add(txt_pinyin);
		txt_pinyin.setColumns(10);
		
		JLabel label_4 = new JLabel("商品图片:");
		label_4.setBounds(341, 25, 54, 15);
		contentPane.add(label_4);
		
		JLabel lbl_icon = new JLabel("");
		lbl_icon.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount() == 2) {
					JFileChooser choose = new JFileChooser(); 
					choose.showOpenDialog(V_goodsAdd.this);
					choose.setFileFilter(new FileNameExtensionFilter("JPG & PNG fiels", "jpg","png"));
					selectFile = choose.getSelectedFile();
					if(selectFile!=null && selectFile.exists()) {
						ImageIcon icon = new ImageIcon(selectFile.getAbsolutePath());
						icon.setImage(icon.getImage().getScaledInstance(125, 100, 1));
						lbl_icon.setIcon(icon);
					}
				}
						
			}
		});
		lbl_icon.setIcon(new ImageIcon(V_goodsAdd.class.getResource("/images/002.jpg")));
		lbl_icon.setBounds(425, 22, 125, 100);
		contentPane.add(lbl_icon);
		
		JLabel label_5 = new JLabel("类 别 :");
		label_5.setBounds(10, 92, 54, 15);
		contentPane.add(label_5);
		
		cb_type = new JComboBox();
		cb_type.setBounds(74, 88, 105, 23);
		contentPane.add(cb_type);
		
		JLabel label_6 = new JLabel("品 牌 :");
		label_6.setBounds(10, 121, 54, 15);
		contentPane.add(label_6);
		
		cb_brand = new JComboBox();
		cb_brand.setBounds(74, 117, 105, 23);
		contentPane.add(cb_brand);
		
		JLabel label_7 = new JLabel("单 位 :");
		label_7.setBounds(10, 154, 54, 15);
		contentPane.add(label_7);
		
		cb_unit = new JComboBox();
		cb_unit.setBounds(74, 150, 105, 23);
		contentPane.add(cb_unit);
		
		JLabel label_8 = new JLabel("进 价 :");
		label_8.setBounds(236, 121, 54, 15);
		contentPane.add(label_8);
		
		JLabel label_8_1 = new JLabel("售 价 :");
		label_8_1.setBounds(236, 154, 54, 15);
		contentPane.add(label_8_1);
		
		JLabel label_8_2 = new JLabel("批发价 :");
		label_8_2.setBounds(236, 179, 54, 15);
		contentPane.add(label_8_2);
		
		JLabel label_8_3 = new JLabel("成本价 :");
		label_8_3.setBounds(236, 204, 54, 15);
		contentPane.add(label_8_3);
		
		txt_purchase_price = new JTextField();
		txt_purchase_price.setColumns(10);
		txt_purchase_price.setBounds(300, 121, 95, 21);
		contentPane.add(txt_purchase_price);
		
		txt_sell_price = new JTextField();
		txt_sell_price.setColumns(10);
		txt_sell_price.setBounds(300, 151, 95, 21);
		contentPane.add(txt_sell_price);
		
		txt_trade_price = new JTextField();
		txt_trade_price.setColumns(10);
		txt_trade_price.setBounds(300, 176, 95, 21);
		contentPane.add(txt_trade_price);
		
		txt_cose_price = new JTextField();
		txt_cose_price.setColumns(10);
		txt_cose_price.setBounds(300, 201, 95, 21);
		contentPane.add(txt_cose_price);
		
		JLabel label_9 = new JLabel("库 存 :");
		label_9.setBounds(10, 190, 54, 15);
		contentPane.add(label_9);
		
		txt_inventory = new JTextField();
		txt_inventory.setBounds(74, 187, 105, 21);
		contentPane.add(txt_inventory);
		txt_inventory.setColumns(10);
		
		JLabel label_9_1 = new JLabel("最小库存 :");
		label_9_1.setBounds(10, 221, 66, 15);
		contentPane.add(label_9_1);
		
		txt_min_incentory = new JTextField();
		txt_min_incentory.setColumns(10);
		txt_min_incentory.setBounds(74, 218, 105, 21);
		contentPane.add(txt_min_incentory);
		
		JLabel label_9_2 = new JLabel("最大库存 :");
		label_9_2.setBounds(10, 249, 66, 15);
		contentPane.add(label_9_2);
		
		txt_max_inventory = new JTextField();
		txt_max_inventory.setColumns(10);
		txt_max_inventory.setBounds(74, 246, 105, 21);
		contentPane.add(txt_max_inventory);
		
		JLabel label_9_3 = new JLabel("积 分 :");
		label_9_3.setBounds(236, 232, 54, 15);
		contentPane.add(label_9_3);
		
		txt_integral = new JTextField();
		txt_integral.setColumns(10);
		txt_integral.setBounds(300, 229, 95, 21);
		contentPane.add(txt_integral);
		
		JLabel label_9_5 = new JLabel("销售数量 :");
		label_9_5.setBounds(236, 265, 66, 15);
		contentPane.add(label_9_5);
		
		txt_sell_num = new JTextField();
		txt_sell_num.setText("0");
		txt_sell_num.setEditable(false);
		txt_sell_num.setColumns(10);
		txt_sell_num.setBounds(300, 262, 95, 21);
		contentPane.add(txt_sell_num);
		
		JCheckBox cb_status = new JCheckBox("下架");
		cb_status.setBounds(10, 280, 103, 23);
		contentPane.add(cb_status);
		
		JButton btn_save = new JButton("保存");
		btn_save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Goods g = new Goods();
				
				Dictionary brand = (Dictionary) cb_brand.getSelectedItem();
				Dictionary type = (Dictionary) cb_type.getSelectedItem();
				Dictionary unit = (Dictionary) cb_unit.getSelectedItem();
				g.setBrand(brand.getId());
				g.setCategory(type.getId());
				g.setUnit(unit.getId());
				g.setCode(txt_pinyin.getText());
				g.setCost_price(CastUtil.castDouble(txt_cose_price.getText()));
				g.setGoods_number(txt_number.getText());
				g.setIntegral(CastUtil.castInt(txt_integral.getText()));
				g.setInventory(CastUtil.castInt(txt_inventory.getText()));
				g.setMax_inventory(CastUtil.castInt(txt_max_inventory.getText()));
				g.setMin_inventory(CastUtil.castInt(txt_min_incentory.getText()));
				g.setName(txt_name.getText());
//				g.setPurchase_num(CastUtil.castInt(txt_number.getText()));
				g.setPurchase_price(CastUtil.castDouble(txt_purchase_price.getText()));
				g.setSell_num(CastUtil.castInt(txt_sell_num.getText()));
				g.setSell_price(CastUtil.castDouble(txt_sell_price.getText()));
				g.setStatus(cb_status.isSelected() ? 0 : 1);
				g.setTrade_price(CastUtil.castDouble(txt_trade_price.getText()));
				g.setManuAddress(txt_manuAddress.getText());
				g.setManuName(txt_manuName.getText());
				g.setNote(txt_node.getText());
				//图片复制
				String dir = System.getProperty("user.dir");
				System.out.println(selectFile+"------------------------");
				String fileName = "/goods-imgs/"+System.currentTimeMillis()+selectFile.getName();
				FileUpload.copy(selectFile.getAbsolutePath(), dir+fileName);
				//网络图片修改
				g.setIcon(fileName);
				
				GoodsService gs = new GoodsServiceImpl();
				if(gs.saveGoods(g)) {
					JOptionPane.showMessageDialog(V_goodsAdd.this, "商品添加成功!");
				}else {
					JOptionPane.showMessageDialog(V_goodsAdd.this, "商品删除失败!");
				}
			}
		});
		btn_save.setBounds(118, 335, 93, 23);
		contentPane.add(btn_save);
		
		JButton button_1 = new JButton("取消");
		button_1.setBounds(260, 335, 93, 23);
		contentPane.add(button_1);
		
		JLabel label_10 = new JLabel("");
		label_10.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				addCB();
			}
		});
		label_10.setIcon(new ImageIcon(V_goodsAdd.class.getResource("/images/add.png")));
		label_10.setBounds(189, 92, 20, 15);
		contentPane.add(label_10);
		
		JLabel label_10_1 = new JLabel("");
		label_10_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				addCB();
			}
		});
		label_10_1.setIcon(new ImageIcon(V_goodsAdd.class.getResource("/images/add.png")));
		label_10_1.setBounds(189, 121, 20, 15);
		contentPane.add(label_10_1);
		
		JLabel label_10_2 = new JLabel("");
		label_10_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				addCB();
			}
		});
		label_10_2.setIcon(new ImageIcon(V_goodsAdd.class.getResource("/images/add.png")));
		label_10_2.setBounds(189, 154, 20, 15);
		contentPane.add(label_10_2);
		
		JLabel label_11 = new JLabel("生产厂家 : ");
		label_11.setBounds(405, 154, 66, 15);
		contentPane.add(label_11);
		
		txt_manuName = new JTextField();
		txt_manuName.setBounds(467, 151, 83, 21);
		contentPane.add(txt_manuName);
		txt_manuName.setColumns(10);
		
		JLabel label_11_1 = new JLabel("生产地址 : ");
		label_11_1.setBounds(405, 182, 66, 15);
		contentPane.add(label_11_1);
		
		txt_manuAddress = new JTextField();
		txt_manuAddress.setColumns(10);
		txt_manuAddress.setBounds(467, 179, 83, 21);
		contentPane.add(txt_manuAddress);
		
		JLabel label_12 = new JLabel("备注:");
		label_12.setBounds(404, 204, 54, 15);
		contentPane.add(label_12);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(405, 226, 145, 132);
		contentPane.add(scrollPane);
		
		txt_node = new JEditorPane();
		scrollPane.setViewportView(txt_node);
		
		JButton button = new JButton("网络搜索");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//先通过条形码查询,数据库中是否存在当前商品,如果存在不再从网络上查询,否则从网络上查询回填
				//数据回显和添加
				DictionaryService ds = new DictionaryServiceImpl();
				Map<String, String> map = ds.getMapByJson(txt_number.getText());
				initCombox();//初始化下拉列表
				txt_name.setText(map.get("goodsName"));
				txt_node.setText(map.get("note"));
				txt_manuName.setText(map.get("manuName"));
				txt_manuAddress.setText(map.get("manuAddress"));
				txt_sell_price.setText(map.get("price"));
				String brand = map.get("trademark");
				Dictionary d = new Dictionary();
				d.setName(brand);
				cb_brand.setSelectedItem(d);
				Dictionary dtype = new Dictionary();
				dtype.setName(map.get("goodsType"));
				cb_type.setSelectedItem(dtype);
			}
		});
		button.setBounds(209, 21, 93, 23);
		contentPane.add(button);
		
		initCombox();
	}
	
	
	
	
	/**
	 * 	初始化下拉列表(字典表处理)
	 */
	public void initCombox() {
		DictionaryService ds = new DictionaryServiceImpl();
		List<Dictionary> typeList = ds.findDictionaryByTypeID(TypeID.类别);
		List<Dictionary> brandList = ds.findDictionaryByTypeID(TypeID.品牌);
		List<Dictionary> unitList = ds.findDictionaryByTypeID(TypeID.单位);
		Dictionary def = new Dictionary();
		def.setId(-1);
		def.setName("--请选择--");
		def.setTypeid(TypeID.默认);
//		typeList.add(def);
//		typeList.set(0, def);
		typeList.add(0, def);
		brandList.add(0, def);
		unitList.add(0, def);
		
		//将集合强制转换成指定的类型数组
		Dictionary[] type_array = typeList.toArray(new Dictionary[] {});
		DefaultComboBoxModel<Dictionary> typeDBM = new DefaultComboBoxModel<Dictionary>(type_array);
		
		Dictionary[] brand_array = brandList.toArray(new Dictionary[] {});
		DefaultComboBoxModel<Dictionary> brandDBM = new DefaultComboBoxModel<Dictionary>(brand_array);
		
		DefaultComboBoxModel<Dictionary> unitDBM = new DefaultComboBoxModel<Dictionary>(unitList.toArray(new Dictionary[] {}));
		
		
		cb_brand.setModel(brandDBM);
		cb_unit.setModel(unitDBM);
		cb_type.setModel(typeDBM);
		
		
	}
	
	/**
	 * 为下拉列表中添加新的值
	 */
	public void addCB() {
		V_Dictionary type = new V_Dictionary();
		type.setBounds(V_goodsAdd.this.getX()+30, V_goodsAdd.this.getY()+10,type.getWidth(), type.getHeight());
		type.setVisible(true);
		type.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				initCombox();
			}
		});
	}
}
