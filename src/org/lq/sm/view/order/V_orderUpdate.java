package org.lq.sm.view.order;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.lq.sm.view.dict.V_Dictionary;
import org.lq.sm.view.employee.V_EmpMain;
import org.lq.sm.view.employee.V_empAdd;

import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class V_orderUpdate extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_orderUpdate frame = new V_orderUpdate();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_orderUpdate() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(V_orderUpdate.class.getResource("/images/reset.png")));
		setTitle("进货修改");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 432, 357);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label_3_1 = new JLabel("积 分：");
		label_3_1.setBounds(250, 108, 54, 15);
		contentPane.add(label_3_1);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(303, 105, 74, 21);
		contentPane.add(textField);
		
		JLabel label_3_2 = new JLabel("折 扣：");
		label_3_2.setBounds(250, 133, 54, 15);
		contentPane.add(label_3_2);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(303, 133, 74, 21);
		contentPane.add(textField_1);
		
		JLabel label_3_3 = new JLabel("单 位：");
		label_3_3.setBounds(250, 158, 54, 15);
		contentPane.add(label_3_3);
		
		JLabel label_3_4 = new JLabel("品 牌：");
		label_3_4.setBounds(250, 191, 54, 15);
		contentPane.add(label_3_4);
		
		JComboBox comboBox_2_1 = new JComboBox();
		comboBox_2_1.setModel(new DefaultComboBoxModel(new String[] {"件", "套", "份"}));
		comboBox_2_1.setBounds(313, 160, 59, 21);
		contentPane.add(comboBox_2_1);
		
		JComboBox comboBox_2_2 = new JComboBox();
		comboBox_2_2.setModel(new DefaultComboBoxModel(new String[] {"美的", "华硕", "格力"}));
		comboBox_2_2.setBounds(314, 188, 59, 21);
		contentPane.add(comboBox_2_2);
		
		JLabel label = new JLabel("编 号:");
		label.setBounds(23, 29, 93, 15);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("类 别：");
		label_1.setBounds(23, 55, 54, 15);
		contentPane.add(label_1);
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setModel(new DefaultComboBoxModel(new String[] {"吃的", "喝的"}));
		comboBox_2.setBounds(82, 54, 59, 17);
		contentPane.add(comboBox_2);
		
		JLabel label_2 = new JLabel("名 称：");
		label_2.setBounds(23, 80, 54, 15);
		contentPane.add(label_2);
		
		JLabel label_3 = new JLabel("进 价：");
		label_3.setBounds(23, 105, 54, 15);
		contentPane.add(label_3);
		
		JLabel label_4 = new JLabel("售 价：");
		label_4.setBounds(23, 135, 54, 15);
		contentPane.add(label_4);
		
		JLabel label_5 = new JLabel("数 量：");
		label_5.setBounds(23, 160, 54, 15);
		contentPane.add(label_5);
		
		JLabel lblNewLabel = new JLabel("备 注：");
		lblNewLabel.setBounds(23, 191, 54, 15);
		contentPane.add(lblNewLabel);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(82, 188, 119, 21);
		contentPane.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(82, 157, 74, 21);
		contentPane.add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(82, 132, 74, 21);
		contentPane.add(textField_4);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(82, 105, 74, 21);
		contentPane.add(textField_5);
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		textField_6.setBounds(82, 81, 119, 21);
		contentPane.add(textField_6);
		
		textField_7 = new JTextField();
		textField_7.setColumns(10);
		textField_7.setBounds(82, 26, 119, 21);
		contentPane.add(textField_7);
		
		JRadioButton radioButton = new JRadioButton("特价商品（不打折）");
		radioButton.setBounds(244, 86, 151, 23);
		contentPane.add(radioButton);
		
		JButton button = new JButton("基本设置");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				V_empAdd add=new V_empAdd();
//				add.setBounds(V_EmpMain.this.getX()+200,V_EmpMain.this.getY()+20,add.getWidth(),add.getHeight());
//				add.setVisible(true);
				V_Dictionary dd=new V_Dictionary();
				dd.setBounds(V_orderUpdate.this.getX(),V_orderUpdate.this.getY(),dd.getWidth(),dd.getHeight());
				dd.setVisible(true);
				
			}
		});
		button.setBounds(23, 244, 93, 23);
		contentPane.add(button);
		
		JButton btnNewButton = new JButton("确认");
		btnNewButton.setBounds(170, 244, 93, 23);
		contentPane.add(btnNewButton);
		
		JButton button_1 = new JButton("退出");
		button_1.setBounds(273, 244, 93, 23);
		contentPane.add(button_1);
	}
}
