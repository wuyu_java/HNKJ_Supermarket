package org.lq.sm.view.order;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JSplitPane;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JRadioButton;
import javax.swing.JTree;
import java.awt.Toolkit;

public class V_orderChose extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_orderChose frame = new V_orderChose();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_orderChose() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(V_orderChose.class.getResource("/images/me.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 655, 488);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel label = new JLabel("款号/条码/编号：");
		panel.add(label);
		
		textField = new JTextField();
		panel.add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("查询");
		btnNewButton.setIcon(new ImageIcon(V_orderChose.class.getResource("/images/search.png")));
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("全部商品");
		panel.add(btnNewButton_1);
		
		JRadioButton radioButton = new JRadioButton("只显示库存>0的");
		panel.add(radioButton);
		
		JButton button = new JButton("关 闭");
		panel.add(button);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JSplitPane splitPane = new JSplitPane();
		panel_1.add(splitPane);
		
		JScrollPane scrollPane = new JScrollPane();
		splitPane.setRightComponent(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null},
			},
			new String[] {
				"\u7F16\u53F7", "\u540D\u79F0", "\u552E\u4EF7", "\u6298\u6263", "\u5E93\u5B58", "\u79EF\u5206"
			}
		));
		scrollPane.setViewportView(table);
		
		JPanel panel_2 = new JPanel();
		splitPane.setDividerLocation(100);
		splitPane.setLeftComponent(panel_2);
		panel_2.setLayout(null);
		
		JTree tree = new JTree();
		tree.setBounds(0, 0, 99, 402);
		panel_2.add(tree);

		//设置jtree中的值
		//设置根节点
		DefaultMutableTreeNode dmtn=new DefaultMutableTreeNode("所有类别");
		//设置二级子节点
		DefaultMutableTreeNode dmtn_ls=new DefaultMutableTreeNode("衣");
		dmtn_ls.add(new DefaultMutableTreeNode("零食"));
		dmtn.add(dmtn_ls);
		//设置一级子节点
		dmtn.add(new DefaultMutableTreeNode("食"));
		dmtn.add(new DefaultMutableTreeNode("住"));
		dmtn.add(new DefaultMutableTreeNode("行"));
		DefaultTreeModel dtm=new DefaultTreeModel(dmtn);
		
		tree.setModel(dtm);
	}
}
