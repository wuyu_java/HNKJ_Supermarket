package org.lq.sm.view.order;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.Document;

import org.lq.sm.entity.Dictionary;
import org.lq.sm.entity.Goods;
import org.lq.sm.entity.OrderList;
import org.lq.sm.entity.Supplier;
import org.lq.sm.entity.TypeID;
import org.lq.sm.service.DictionaryService;
import org.lq.sm.service.GoodsService;
import org.lq.sm.service.OrderListService;
import org.lq.sm.service.SupplierService;
import org.lq.sm.service.iml.DictionaryServiceImpl;
import org.lq.sm.service.iml.GoodsServiceImpl;
import org.lq.sm.service.iml.OrderListServiceImpl;
import org.lq.sm.service.iml.SupplierServiceImpl;
import org.lq.sm.util.CastUtil;
import org.lq.sm.util.AutoNo;
import org.lq.sm.view.goods.DocumentAdapter;
import org.lq.sm.view.goods.V_goods;

import com.eltima.components.ui.DatePicker;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JTextPane;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Date;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.awt.event.ActionEvent;

public class V_orderList extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField txt_number;
	private JTextField txt_name;
	private JTextField txt_purchase_price;
	private JTextField txt_sell_price;
	private JTextField txt_integral;
	private JTextField txt_num;
	private JComboBox cb_type;
	private JComboBox cb_unit;
	private JComboBox cb_brand;
	private JTextPane txt_node;
	private JComboBox cb_supplier;
	private Goods g;//选择入库商品的对象
	private JTextField txt_totalPrice;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_orderList frame = new V_orderList();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_orderList() {
		setTitle("进货管理");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1007, 670);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, 10, 971, 153);
		contentPane.add(panel);
		panel.setLayout(null);

		JComboBox cb_date = new JComboBox();
		cb_date.setModel(new DefaultComboBoxModel(new String[] {"显示所有", "显示当前", "显示当月", "显示当年"}));
		cb_date.setBounds(10, 10, 97, 23);
		panel.add(cb_date);

		DatePicker startDP = new DatePicker();
		startDP.setBounds(117, 11, 126, 21);
		panel.add(startDP);

		DatePicker endDP = new DatePicker();
		endDP.setBounds(253, 11, 126, 21);
		panel.add(endDP);

		JButton button = new JButton("查询");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String value = cb_date.getSelectedItem().toString();
				/*
				 * 显示所有
					显示当前
					显示当月
					显示当年
				 */
				initTable(new OrderListServiceImpl().getByDateName(value));
				OrderListService os = new OrderListServiceImpl();
				if("显示所有".equals(value)) {
					String txt_start = startDP.getInnerTextField().getText();
					String txt_end = endDP.getInnerTextField().getText();
					//有开始时间,没有结束时间
					if(!"".equals(txt_start) && "".equals(txt_end)){
						try {
							Date start = CastUtil.parse2(txt_start, CastUtil.TIME_DATE_PATTERN);
							Date end = new Date(System.currentTimeMillis());//获取当前时间
							initTable(os.getOrderByDate(start, end));
						} catch (ParseException e1) {
							e1.printStackTrace();
						}
						//有结束时间,没有开始时间
					}else if("".equals(txt_start) && !"".equals(txt_end)){
						try {
							Date start = new Date(0,0,0);//获取当前时间
							Date end = CastUtil.parse2(txt_end, CastUtil.TIME_DATE_PATTERN);
							initTable(os.getOrderByDate(start, end));
						} catch (ParseException e1) {
							e1.printStackTrace();
						}
						// 都没有添加
					}else if("".equals(txt_start) && "".equals(txt_end)){
						initTable(os.findOrderListAll());
					}else {
						//两个都有值
						try {
							Date start = CastUtil.parse2(txt_start, CastUtil.TIME_DATE_PATTERN);;//获取当前时间
							Date end = CastUtil.parse2(txt_end, CastUtil.TIME_DATE_PATTERN);
							initTable(os.getOrderByDate(start, end));
						} catch (ParseException e1) {
							e1.printStackTrace();
						}
					}
				}
			}
		});
		button.setBounds(395, 10, 93, 23);
		panel.add(button);

		JLabel label = new JLabel("商 品 :");
		label.setBounds(10, 43, 54, 15);
		panel.add(label);

		txt_number = new JTextField();
		txt_number.setBounds(74, 40, 66, 21);
		panel.add(txt_number);
		txt_number.setColumns(10);

		JButton btnNewButton = new JButton("");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				V_goods goods = new V_goods();
				goods.setBounds(V_orderList.this.getX(),V_orderList.this.getY(),goods.getWidth(),goods.getHeight());
				goods.setVisible(true);

				//获取商品的编号

				goods.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosed(WindowEvent e) {
						int gid= goods.getGoodsId();
						GoodsService gs = new GoodsServiceImpl();
						g = gs.getGoodsByIdInDication(gid);
						initCombox();//初始化下拉列表
						txt_name.setText(g.getName());
						txt_node.setText(g.getNote());
						txt_sell_price.setText(g.getSell_price()+"");
						txt_integral.setText(g.getIntegral()+"");
						txt_number.setText(g.getGoods_number());
						txt_purchase_price.setText(g.getPurchase_price()+"");
						cb_brand.setSelectedItem(g.getBrandObj());
						cb_type.setSelectedItem(g.getTypeObj());
						cb_unit.setSelectedItem(g.getUnitObj());
					}
				});
			}
		});
		btnNewButton.setIcon(new ImageIcon(V_orderList.class.getResource("/images/login.png")));
		btnNewButton.setBounds(140, 39, 17, 23);
		panel.add(btnNewButton);

		JLabel label_1 = new JLabel("类 别 :");
		label_1.setBounds(10, 80, 54, 15);
		panel.add(label_1);

		cb_type = new JComboBox();
		cb_type.setBounds(74, 76, 83, 23);
		panel.add(cb_type);

		JLabel label_2 = new JLabel("名 称 :");
		label_2.setBounds(10, 113, 54, 15);
		panel.add(label_2);

		txt_name = new JTextField();
		txt_name.setBounds(74, 109, 83, 21);
		panel.add(txt_name);
		txt_name.setColumns(10);

		JLabel label_3 = new JLabel("进 价 :");
		label_3.setBounds(212, 43, 54, 15);
		panel.add(label_3);

		txt_purchase_price = new JTextField();
		txt_purchase_price.setBounds(276, 40, 103, 21);
		panel.add(txt_purchase_price);
		txt_purchase_price.setColumns(10);

		JLabel label_3_1 = new JLabel("售 价 :");
		label_3_1.setBounds(212, 77, 54, 15);
		panel.add(label_3_1);

		txt_sell_price = new JTextField();
		txt_sell_price.setColumns(10);
		txt_sell_price.setBounds(276, 74, 103, 21);
		panel.add(txt_sell_price);

		JLabel label_3_2 = new JLabel("积 分 :");
		label_3_2.setBounds(212, 110, 54, 15);
		panel.add(label_3_2);

		txt_integral = new JTextField();
		txt_integral.setColumns(10);
		txt_integral.setBounds(276, 107, 103, 21);
		panel.add(txt_integral);

		JLabel label_4 = new JLabel("单 位 :");
		label_4.setBounds(425, 43, 54, 15);
		panel.add(label_4);

		cb_unit = new JComboBox();
		cb_unit.setBounds(489, 39, 77, 23);
		panel.add(cb_unit);

		JLabel label_5 = new JLabel("品 牌 :");
		label_5.setBounds(425, 80, 54, 15);
		panel.add(label_5);

		cb_brand = new JComboBox();
		cb_brand.setBounds(489, 76, 77, 23);
		panel.add(cb_brand);

		JLabel label_6 = new JLabel("数 量 :");
		label_6.setBounds(425, 113, 54, 15);
		panel.add(label_6);

		txt_num = new JTextField();
		Document doc = txt_num.getDocument();
		doc.addDocumentListener(new DocumentAdapter() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				double totalPrice = CastUtil.castDouble(txt_purchase_price.getText()) * CastUtil.castInt(txt_num.getText());
				txt_totalPrice.setText("￥"+totalPrice+"");
			}
			@Override
			public void removeUpdate(DocumentEvent e) {
				double totalPrice = CastUtil.castDouble(txt_purchase_price.getText()) * CastUtil.castInt(txt_num.getText());
				txt_totalPrice.setText("￥"+totalPrice+"");
			}
		});
		txt_num.setColumns(10);
		txt_num.setBounds(489, 110, 83, 21);
		panel.add(txt_num);

		JLabel label_7 = new JLabel("备 注 :");
		label_7.setBounds(792, 43, 54, 15);
		panel.add(label_7);

		JButton button_1 = new JButton("保 存");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				OrderList order = new OrderList();
				Dictionary brand = (Dictionary) cb_brand.getSelectedItem();
				Dictionary type = (Dictionary) cb_type.getSelectedItem();
				order.setBrand(brand.getName());
				order.setgId(g.getId());
				order.setNumber(CastUtil.castInt(txt_num.getText()));
				//				order.setOnsale(); 是否是特价
				//订单名称(哪一个用户入库的订单)
				order.setOrderName("无语");
				order.setOrderNo(AutoNo.getOrderListNo().intValue());
				order.setPursePrice(g.getPurchase_price());
				order.setRemark(txt_node.getText());
				order.setSellPrice(CastUtil.castDouble(g.getSell_price()));
				Supplier sup = (Supplier) cb_supplier.getSelectedItem();
				order.setSupplier(sup.getId());

				double totalPrice = CastUtil.castDouble(txt_purchase_price.getText()) * CastUtil.castInt(txt_num.getText());
				order.setTotalPrice(totalPrice);
				order.setTradePrice(g.getTrade_price());
				order.setType(type.getName());

				OrderListService os = new OrderListServiceImpl();
				if(os.saveOrderList(order)) {
					JOptionPane.showMessageDialog(V_orderList.this, "订单添加成功!");
					initTable(os.findOrderListAll());
				};

			}
		});
		button_1.setIcon(new ImageIcon(V_orderList.class.getResource("/images/保存 (1).png")));
		button_1.setBounds(498, 10, 93, 23);
		panel.add(button_1);

		JButton button_1_1 = new JButton("取 消");
		button_1_1.setIcon(new ImageIcon(V_orderList.class.getResource("/images/reset.png")));
		button_1_1.setBounds(604, 10, 93, 23);
		panel.add(button_1_1);

		JLabel label_8 = new JLabel("供应商 : ");
		label_8.setBounds(604, 43, 54, 15);
		panel.add(label_8);

		cb_supplier = new JComboBox();
		cb_supplier.setBounds(668, 39, 114, 23);
		panel.add(cb_supplier);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(837, 43, 124, 85);
		panel.add(scrollPane_1);

		txt_node = new JTextPane();
		scrollPane_1.setViewportView(txt_node);

		JLabel label_9 = new JLabel("总价 : ");
		label_9.setBounds(604, 113, 54, 15);
		panel.add(label_9);

		txt_totalPrice = new JTextField();
		txt_totalPrice.setEditable(false);
		txt_totalPrice.setBounds(668, 110, 114, 21);
		panel.add(txt_totalPrice);
		txt_totalPrice.setColumns(10);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 184, 971, 437);
		contentPane.add(scrollPane);

		table = new JTable();
		table.setModel(new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
						"\u7F16\u53F7", "\u540D\u79F0", "\u6570\u91CF", "\u8FDB\u4EF7", "售价", "总金额", "\u65E5\u671F"
				}
				));
		scrollPane.setViewportView(table);


		initCombox();
		initTable(new OrderListServiceImpl().findOrderListAll());
	}

	public void initTable(List<OrderList> list) {
		DefaultTableModel dtm = (DefaultTableModel) table.getModel();
		int count = dtm.getRowCount();
		for (int i = 0; i < count; i++) {
			dtm.removeRow(0);
		}

		for (OrderList o : list) {
			dtm.addRow(new Object[] {
					o.getOrderNo(),
					o.getOrderName(),
					o.getNumber(),
					o.getPursePrice(),
					o.getSellPrice(),
					o.getTotalPrice(),
					o.getDate()
			});
		}
	}

	/**
	 * 	初始化下拉列表(字典表处理)
	 */
	public void initCombox() {
		DictionaryService ds = new DictionaryServiceImpl();
		List<Dictionary> typeList = ds.findDictionaryByTypeID(TypeID.类别);
		List<Dictionary> brandList = ds.findDictionaryByTypeID(TypeID.品牌);
		List<Dictionary> unitList = ds.findDictionaryByTypeID(TypeID.单位);
		Dictionary def = new Dictionary();
		def.setId(-1);
		def.setName("--请选择--");
		def.setTypeid(TypeID.默认);
		//		typeList.add(def);
		//		typeList.set(0, def);
		typeList.add(0, def);
		brandList.add(0, def);
		unitList.add(0, def);

		//将集合强制转换成指定的类型数组
		Dictionary[] type_array = typeList.toArray(new Dictionary[] {});
		DefaultComboBoxModel<Dictionary> typeDBM = new DefaultComboBoxModel<Dictionary>(type_array);

		Dictionary[] brand_array = brandList.toArray(new Dictionary[] {});
		DefaultComboBoxModel<Dictionary> brandDBM = new DefaultComboBoxModel<Dictionary>(brand_array);

		DefaultComboBoxModel<Dictionary> unitDBM = new DefaultComboBoxModel<Dictionary>(unitList.toArray(new Dictionary[] {}));


		cb_brand.setModel(brandDBM);
		cb_unit.setModel(unitDBM);
		cb_type.setModel(typeDBM);

		//供应商下拉列表填充
		SupplierService ss = new SupplierServiceImpl();
		List<Supplier> supplierList = ss.findSupplierAll();
		Supplier s = new Supplier();
		s.setId(-1);
		s.setName("--请选择--");
		supplierList.add(0, s);
		DefaultComboBoxModel<Supplier> supplierDBM = new DefaultComboBoxModel<Supplier>(supplierList.toArray(new Supplier[] {}));
		cb_supplier.setModel(supplierDBM);
	}



}
