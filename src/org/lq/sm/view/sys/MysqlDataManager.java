package org.lq.sm.view.sys;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.lq.sm.util.CastUtil;

public class MysqlDataManager extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MysqlDataManager frame = new MysqlDataManager();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MysqlDataManager() {
		setTitle("数据库导入导出");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("恢复数据");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser open = new JFileChooser();
				String dir = System.getProperty("user.dir");
				open.setSelectedFile(new File(dir+"/data/"));
				open.setFileFilter(new FileNameExtensionFilter("SQL fiels", "sql"));
				open.showOpenDialog(null);
				File file = open.getSelectedFile();
				try {
					Runtime.getRuntime().exec("cmd.exe /c mysql -uroot -proot < "+file.getAbsolutePath());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(74, 119, 285, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("备份数据");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				JFileChooser save = new JFileChooser();
				String dir = System.getProperty("user.dir");
				save.setSelectedFile(new File(dir+"/data/supermarket-"+CastUtil.format(new Date(), CastUtil.TIME_DATE_PATTERN2)+".sql"));
				save.showSaveDialog(null);
				File file = save.getSelectedFile();
				try {
					Runtime.getRuntime().exec("cmd.exe /c mysqldump -uroot -proot --databases hnkj_supermarket > "+file.getAbsolutePath());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnNewButton_1.setBounds(74, 63, 285, 23);
		contentPane.add(btnNewButton_1);
	}

}
