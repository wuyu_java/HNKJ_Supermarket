package org.lq.sm.view.employee;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.lq.sm.entity.Employee;
import org.lq.sm.service.EmployeeService;
import org.lq.sm.service.iml.EmployeeServiceImpl;
import org.lq.sm.util.CastUtil;

import com.eltima.components.ui.DatePicker;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class V_empAdd extends JFrame {

	private JPanel contentPane;
	private JTextField txt_id;
	private JTextField txt_name;
	private JTextField txt_phone;
	private JTextField txt_salary;
	private String iconPath = "";//存储图片路径
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_empAdd frame = new V_empAdd();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private Employee update_emp = null;//修改的员工对象
	private DatePicker dp;
	private JSpinner num_comm;
	private JTextPane txt_desc;
	private JLabel lbl_icon;
	/**
	 * 带参构造
	 * @param id 员工编号
	 */
	public V_empAdd(int id) {
		this();//调用无参构造
		update_emp = new EmployeeServiceImpl().getById(id);
		String dir = System.getProperty("user.dir");
		txt_id.setText(CastUtil.castString(update_emp.getEmpid()));
		txt_name.setText(update_emp.getName());
		txt_phone.setText(update_emp.getPhone());
		txt_salary.setText(CastUtil.castString(update_emp.getSalary()));
		dp.getInnerTextField().setText(CastUtil.format(update_emp.getTime()));
		num_comm.setValue(CastUtil.castDouble(update_emp.getCommratio()));
		txt_desc.setText(update_emp.getRemarks());
		ImageIcon icon = new ImageIcon(dir+"/"+update_emp.getIcon());
		icon.setImage(icon.getImage().getScaledInstance(125, 93, 1));
		lbl_icon.setIcon(icon);
	}

	/**
	 * Create the frame.
	 */
	public V_empAdd() {
		setTitle("维护员工");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 427, 446);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "\u5458\u5DE5\u4FE1\u606F", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		JLabel label = new JLabel("工 号 : ");
		label.setBounds(23, 29, 54, 15);
		panel.add(label);

		txt_id = new JTextField();
		txt_id.setEditable(false);
		txt_id.setBounds(73, 26, 66, 21);
		panel.add(txt_id);
		txt_id.setColumns(10);

		JLabel label_1 = new JLabel("姓 名:");
		label_1.setBounds(23, 54, 54, 15);
		panel.add(label_1);

		txt_name = new JTextField();
		txt_name.setBounds(73, 51, 129, 21);
		panel.add(txt_name);
		txt_name.setColumns(10);

		JLabel label_1_1 = new JLabel("电 话 :");
		label_1_1.setBounds(23, 82, 54, 15);
		panel.add(label_1_1);

		txt_phone = new JTextField();
		txt_phone.setColumns(10);
		txt_phone.setBounds(73, 79, 129, 21);
		panel.add(txt_phone);

		JLabel label_1_2 = new JLabel("底 薪 :");
		label_1_2.setBounds(23, 113, 54, 15);
		panel.add(label_1_2);

		txt_salary = new JTextField();
		txt_salary.setText("0");
		txt_salary.setColumns(10);
		txt_salary.setBounds(73, 110, 129, 21);
		panel.add(txt_salary);

		JLabel label_1_3 = new JLabel("提成比例:");
		label_1_3.setBounds(23, 141, 74, 15);
		panel.add(label_1_3);

		JLabel label_1_4 = new JLabel("入职日期 :");
		label_1_4.setBounds(23, 174, 74, 15);
		panel.add(label_1_4);

		num_comm = new JSpinner();
		num_comm.setBounds(95, 138, 56, 22);
		panel.add(num_comm);

		JLabel label_2 = new JLabel("(1%代表提成0.01)");
		label_2.setForeground(Color.BLUE);
		label_2.setBounds(166, 141, 136, 15);
		panel.add(label_2);

		dp = new DatePicker();
		dp.getInnerButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		dp.setBounds(100, 171, 150, 21);
		dp.getInnerTextField().setText(CastUtil.format(new Date()));
		panel.add(dp);

		JLabel label_3 = new JLabel("备 注 :");
		label_3.setBounds(23, 199, 54, 15);
		panel.add(label_3);

		txt_desc = new JTextPane();
		txt_desc.setBounds(73, 199, 229, 132);
		panel.add(txt_desc);

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "\u5934\u50CF", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBounds(256, 29, 114, 98);
		panel.add(panel_2);
		panel_2.setLayout(new BorderLayout(0, 0));

		lbl_icon = new JLabel("双击,上传");
		lbl_icon.setToolTipText("");
		lbl_icon.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//双击
				if(e.getClickCount()==2) {
					JFileChooser jf = new JFileChooser();
					jf.setFileFilter(new FileNameExtensionFilter("JPG & PNG fiels", "jpg","png"));
					jf.showOpenDialog(null);
					File f = jf.getSelectedFile();
					String path = f.getAbsolutePath();
					iconPath = path;//将用户选择的图片路径赋值给字符串
					ImageIcon icon = new ImageIcon(path);
					icon.setImage(icon.getImage().getScaledInstance(125, 93, 1));
					lbl_icon.setIcon(icon);
				}
			}
		});
		panel_2.add(lbl_icon, BorderLayout.CENTER);

		JPanel panel_1 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		flowLayout.setHgap(20);
		flowLayout.setAlignment(FlowLayout.RIGHT);
		contentPane.add(panel_1, BorderLayout.SOUTH);

		JButton btn_save = new JButton("保 存");
		btn_save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				
				
				//将图片存储到项目的指定路径下
				EmployeeService empService = new EmployeeServiceImpl();
				
				if(update_emp!=null) {
					//修改
					update_emp.setAuthority("1");//所有添加的员工默认都是普通员工
					update_emp.setCommratio(CastUtil.castString(num_comm.getValue()));
					update_emp.setName(txt_name.getText());
					update_emp.setPhone(txt_phone.getText());
					update_emp.setRemarks(txt_desc.getText());
					update_emp.setSalary(CastUtil.castInt(txt_salary.getText()));
					String hiredate = dp.getInnerTextField().getText();
					//hiredate
					try {
						update_emp.setTime(CastUtil.parse(hiredate));
					} catch (ParseException e1) {
						e1.printStackTrace();
					}
					if(!"".equals(iconPath) ) {
						//头像赋值
						String icon = updateImg();
						update_emp.setIcon(icon);
					}
					if(empService.updateEmployee(update_emp)) {
						JOptionPane.showMessageDialog(V_empAdd.this, "保存成功!");
						dispose();//关闭当前窗体
					}else {
						JOptionPane.showMessageDialog(V_empAdd.this, "保存失败!");
					}
				}else {
					//添加
					Employee emp = new Employee();
					emp.setAuthority("1");//所有添加的员工默认都是普通员工
					emp.setCommratio(CastUtil.castString(num_comm.getValue()));
					emp.setName(txt_name.getText());
					emp.setPhone(txt_phone.getText());
					emp.setRemarks(txt_desc.getText());
					emp.setSalary(CastUtil.castInt(txt_salary.getText()));
					String hiredate = dp.getInnerTextField().getText();
					//hiredate
					try {
						emp.setTime(CastUtil.parse(hiredate));
					} catch (ParseException e1) {
						e1.printStackTrace();
					}
					//头像赋值
					String icon = updateImg();
					emp.setIcon(icon);
					if(empService.saveEmployee(emp)) {
						JOptionPane.showMessageDialog(V_empAdd.this, "保存成功!");
						dispose();//关闭当前窗体
					}else {
						JOptionPane.showMessageDialog(V_empAdd.this, "保存失败!");
					}
				}
			}
		});
		btn_save.setIcon(new ImageIcon(V_empAdd.class.getResource("/images/保存 (1).png")));
		panel_1.add(btn_save);

		JButton btnNewButton_1 = new JButton("取 消");
		btnNewButton_1.setIcon(new ImageIcon(V_empAdd.class.getResource("/images/reset.png")));
		panel_1.add(btnNewButton_1);
	}
	
	/**
	 * 上传图片到指定的路径下
	 */
	public String updateImg() {
		String icon = "";
		//动态获取当前项目路径
		String dir = System.getProperty("user.dir");
		File file = new File(iconPath);
		String fileName = file.getName();
		InputStream is = null;
		OutputStream os = null;
		try {
			//拼接输出路径
			icon = "/icons/"+System.currentTimeMillis()+"-"+fileName;
			//源图片
			is = new FileInputStream(file);
			os = new FileOutputStream(dir+icon);
			byte [] b = new byte[1024];
			int len = 0;
			while((len=is.read(b))!=-1) {
				os.write(b,0,len);
			}


		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				os.flush();
				os.close();
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return icon;
	}



}
