package org.lq.sm.view.employee;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import org.lq.sm.entity.Employee;
import org.lq.sm.service.EmployeeService;
import org.lq.sm.service.iml.EmployeeServiceImpl;
import org.lq.sm.util.CastUtil;
/**
 * 员工视图
 * @author 杨培林
 * @phone 13603738558
 * 
 * @package org.lq.sm.view.employee
 * @date 2020年8月10日下午5:32:27
 *
 */
public class V_EmpMain extends JFrame {

	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_EmpMain frame = new V_EmpMain();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_EmpMain() {
		setTitle("员工资料管理");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 723, 493);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);

		table = new JTable();
		table.setModel(new DefaultTableModel(
				new Object[][] {

				},
				new String[] {
						"\u5DE5\u53F7", "\u59D3\u540D", "\u7535\u8BDD", "\u5E95\u85AA", "\u63D0\u6210\u6BD4\u4F8B", "\u5165\u804C\u65E5\u671F", "\u5907\u6CE8"
				}
				));
		scrollPane.setViewportView(table);

		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setHgap(20);
		flowLayout.setAlignment(FlowLayout.RIGHT);
		contentPane.add(panel, BorderLayout.SOUTH);

		JButton btn_add = new JButton("增加");
		btn_add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				V_empAdd add = new V_empAdd();
				add.setBounds(V_EmpMain.this.getX()+100, V_EmpMain.this.getY(), add.getWidth(), add.getHeight());
				add.setVisible(true);
				//
				//				add.addWindowListener(new MyWindowAdapter() {
				//					@Override
				//					public void windowClosing(WindowEvent e) {
				//					}
				//					@Override
				//					public void windowClosed(WindowEvent e) {
				//					}
				//				});
				//监听窗体是否被关闭
				add.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosed(WindowEvent e) {
						//当添加窗体关闭的时候,调用表格初始化方法
						initTable();
					}

					@Override
					public void windowClosing(WindowEvent e) {
						//						System.out.println("**************");
					}
				});
			}
		});
		btn_add.setIcon(new ImageIcon(V_EmpMain.class.getResource("/images/add.png")));
		panel.add(btn_add);

		JButton btn_update = new JButton("修改");
		btn_update.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index =  table.getSelectedRow();
				if(index >= 0) {
					Object v = table.getValueAt(index, 0);
					int id = CastUtil.castInt(v);
					V_empAdd empupdate = new V_empAdd(id);
					empupdate.setBounds(V_EmpMain.this.getX()+100, V_EmpMain.this.getY(), empupdate.getWidth(), empupdate.getHeight());
					empupdate.setVisible(true);
					empupdate.addWindowListener(new WindowAdapter() {
						public void windowClosed(WindowEvent e) {
							initTable();
						};
					});
				}else {
					JOptionPane.showMessageDialog(V_EmpMain.this, "请选择员工后,单击修改!");
				}
			}
		});
		btn_update.setIcon(new ImageIcon(V_EmpMain.class.getResource("/images/modify.png")));
		panel.add(btn_update);

		JButton btn_delete = new JButton("删除");
		btn_delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = table.getSelectedRow();
				if(index >=0) {
					int n =JOptionPane.showConfirmDialog(V_EmpMain.this, "确定删除"+table.getValueAt(index, 1)+"员工吗?","删除",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
					if(n == 0) {
						Object v = table.getValueAt(index, 0);
						int id = CastUtil.castInt(v);
						EmployeeService es = new EmployeeServiceImpl();
						if(es.deleteEmployeeById(id)) {
							JOptionPane.showMessageDialog(V_EmpMain.this, "删除成功!");
							initTable();
						}else {
							JOptionPane.showMessageDialog(V_EmpMain.this, "删除失败!");
						}
					}
				}else {
					JOptionPane.showMessageDialog(V_EmpMain.this, "请选择删除的行!");
				}

			}
		});
		btn_delete.setIcon(new ImageIcon(V_EmpMain.class.getResource("/images/delete.png")));
		panel.add(btn_delete);

		initTable();
	}





	public void initTable() {
		DefaultTableModel dtm = (DefaultTableModel) table.getModel();

		int count = dtm.getRowCount();
		for(int i = 0;i<count;i++) {
			dtm.removeRow(0);
		}


		EmployeeService empService = new EmployeeServiceImpl();
		List<Employee> list = empService.findEmployeeAll();
		for (Employee e : list) {
			dtm.addRow(new Object[] {
					e.getEmpid(),
					e.getName(),
					e.getPhone(),
					e.getSalary(),
					e.getCommratio(),
					e.getTime(),
					e.getRemarks()
			});
		}
	}













}
