package org.lq.sm.view.supplier;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import org.lq.sm.entity.Supplier;
import org.lq.sm.service.SupplierService;
import org.lq.sm.service.iml.SupplierServiceImpl;
import org.lq.sm.util.CastUtil;
import org.lq.sm.view.employee.V_EmpMain;

import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import java.awt.event.ActionEvent;
/**
 * 供应商视图界面
 * @author 杨培林
 * @phone 13603738558
 * 
 * @package org.lq.sm.view.supplier
 * @date 2020年8月10日下午5:31:06
 *
 */
public class V_supplier extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	private JTextField txt_search_value;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_supplier frame = new V_supplier();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_supplier() {
		setTitle("供应商管理");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 898, 530);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)), "\u6570\u636E\u64CD\u4F5C", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));

		JScrollPane scrollPane = new JScrollPane();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
				gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(panel, GroupLayout.DEFAULT_SIZE, 852, Short.MAX_VALUE)
								.addGroup(gl_contentPane.createSequentialGroup()
										.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 850, Short.MAX_VALUE)
										.addGap(2)))
						.addContainerGap())
				);
		gl_contentPane.setVerticalGroup(
				gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
						.addContainerGap()
						.addComponent(panel, GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE)
						.addGap(18)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 355, Short.MAX_VALUE)
						.addGap(15))
				);

		JLabel label = new JLabel("名 称  :");
		panel.add(label);

		txt_search_value = new JTextField();
		panel.add(txt_search_value);
		txt_search_value.setColumns(10);

		JButton btn_search = new JButton("查询");
		btn_search.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String value = txt_search_value.getText();
				SupplierService ss = new SupplierServiceImpl();
				List<Supplier> list = ss.likeSupplierByName(value);
				initTable(list);
			}
		});
		panel.add(btn_search);

		JButton btn_save = new JButton("添加");
		btn_save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				V_SupplierAdd add = new V_SupplierAdd();
				add.setBounds(V_supplier.this.getX()+50, V_supplier.this.getY()+20, add.getWidth(), add.getHeight());
				add.setVisible(true);
				add.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosed(WindowEvent e) {
						initTable();
					}
				});

			}
		});
		panel.add(btn_save);

		JButton btn_update = new JButton("修改");
		btn_update.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = table.getSelectedRow();
				if(index>=0) {
					int id = CastUtil.castInt(table.getValueAt(index, 0));
					V_SupplierAdd add = new V_SupplierAdd(id,true);
					add.setBounds(V_supplier.this.getX()+50, V_supplier.this.getY()+20, add.getWidth(), add.getHeight());
					add.setVisible(true);
					add.addWindowListener(new WindowAdapter() {
						@Override
						public void windowClosed(WindowEvent e) {
							initTable();
						}
					});
				}else {
					JOptionPane.showMessageDialog(V_supplier.this, "请选择修改的供应商!");
				}



			}
		});
		panel.add(btn_update);

		JButton btn_delete = new JButton("删除");
		btn_delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = table.getSelectedRow();
				if(index>=0) {
					int n =JOptionPane.showConfirmDialog(V_supplier.this, "确定删除"+table.getValueAt(index, 1)+"供应商吗?","删除",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
					if(n==0) {
						int id = CastUtil.castInt(table.getValueAt(index, 0));
						if(new SupplierServiceImpl().deleteSupplierById(id)) {
							JOptionPane.showMessageDialog(V_supplier.this, "删除供应商成功!");
							initTable();
						}else {
							JOptionPane.showMessageDialog(V_supplier.this, "删除供应商失败!");
						}
					}
				}else {
					JOptionPane.showMessageDialog(V_supplier.this, "请选择供应商!");
				}

			}
		});
		panel.add(btn_delete);

		JButton btn_detailed = new JButton("详细信息");
		btn_detailed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int index = table.getSelectedRow();
				if(index>=0) {
					int id = CastUtil.castInt(table.getValueAt(index, 0));
					V_SupplierAdd add = new V_SupplierAdd(id,false);
					add.setBounds(V_supplier.this.getX()+50, V_supplier.this.getY()+20, add.getWidth(), add.getHeight());
					add.setVisible(true);
				}else {
					JOptionPane.showMessageDialog(V_supplier.this, "请选择供应商!");
				}
			}
		});
		panel.add(btn_detailed);

		table = new JTable();
		table.setModel(new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
						"\u7F16\u53F7", "\u7B80\u79F0", "\u540D\u79F0", "\u8054\u7CFB\u4EBA", "\u7535\u8BDD", "\u624B\u673A\u53F7"
				}
				));
		scrollPane.setViewportView(table);
		contentPane.setLayout(gl_contentPane);

		initTable();
	}


	public void initTable(List<Supplier> list) {
		DefaultTableModel dtm = (DefaultTableModel) table.getModel();
		int count = dtm.getRowCount();
		for(int i = 0;i< count;i++) {
			dtm.removeRow(0);
		}

		for (Supplier s : list) {
			dtm.addRow(new Object[] {
					s.getId(),
					s.getAbbre(),
					s.getName(),
					s.getContacts(),
					s.getTnumber(),
					s.getCnumber()
			});
		}
	}
	

	public void initTable() {
		DefaultTableModel dtm = (DefaultTableModel) table.getModel();
		int count = dtm.getRowCount();
		for(int i = 0;i< count;i++) {
			dtm.removeRow(0);
		}

		SupplierService service = new SupplierServiceImpl();
		List<Supplier> list = service.findSupplierAll();
		for (Supplier s : list) {
			dtm.addRow(new Object[] {
					s.getId(),
					s.getAbbre(),
					s.getName(),
					s.getContacts(),
					s.getTnumber(),
					s.getCnumber()
			});
		}
	}








}
