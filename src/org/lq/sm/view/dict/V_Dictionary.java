package org.lq.sm.view.dict;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;

import org.lq.sm.entity.Dictionary;
import org.lq.sm.entity.TypeID;
import org.lq.sm.service.DictionaryService;
import org.lq.sm.service.iml.DictionaryServiceImpl;
import org.lq.sm.util.CastUtil;
/**
 * 	字典界面
 * @author 杨培林
 * @phone 13603738558
 * 
 * @package org.lq.sm.view.dict
 * @date 2020年8月8日下午2:40:23
 *
 */
public class V_Dictionary extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DictionaryService dictService = new DictionaryServiceImpl();
	private JPanel contentPane;
	private JTree tree_type;
	private JTree tree_brand;
	private JTree tree_unit;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_Dictionary frame = new V_Dictionary();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_Dictionary() {
		setTitle("数据字典");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 389, 444);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);

		JPanel p_type = new JPanel();
		/*
		 * 1. tab标题
		 * 2. tab图标
		 * 3. 	添加到tab中的组件
		 * 4. 	鼠标悬停到tab选项卡上面的提示
		 */
		tabbedPane.addTab("类别设置", null, p_type, "aaaa");
		p_type.setLayout(new BorderLayout(0, 0));

		tree_type = new JTree();
		p_type.add(tree_type, BorderLayout.CENTER);

		JPanel p_brand = new JPanel();
		tabbedPane.addTab("品牌设置", null, p_brand, null);
		p_brand.setLayout(new BorderLayout(0, 0));

		tree_brand = new JTree();
		p_brand.add(tree_brand, BorderLayout.CENTER);

		JPanel p_unit = new JPanel();
		tabbedPane.addTab("单位设置", null, p_unit, null);
		p_unit.setLayout(new BorderLayout(0, 0));

		tree_unit = new JTree();
		p_unit.add(tree_unit, BorderLayout.CENTER);




		// 设置jtree中的值
		//代表是一个节点
		//		DefaultMutableTreeNode dmtn = new DefaultMutableTreeNode("根节点");
		//		DefaultMutableTreeNode c = new DefaultMutableTreeNode("C:\\");
		//		c.add(new DefaultMutableTreeNode("aaa"));
		//		dmtn.add(c);
		//		dmtn.add(new DefaultMutableTreeNode("D:\\"));
		//		dmtn.add(new DefaultMutableTreeNode("E:\\"));
		//		dmtn.add(new DefaultMutableTreeNode("F:\\"));
		//
		//		DefaultTreeModel dtm = new DefaultTreeModel(dmtn);
		//
		//		tree_type.setModel(dtm);

		JPanel p_btns_1 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) p_btns_1.getLayout();
		flowLayout.setHgap(20);
		contentPane.add(p_btns_1, BorderLayout.SOUTH);

		JButton btn_new = new JButton("新建类别");
		btn_new.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//根据获取的选项卡判断添加的值内容
				int tabIndex = tabbedPane.getSelectedIndex();
				Dictionary d = new Dictionary();
				String title = "";
				switch (tabIndex) {
				case 0:
					title = "类别";
					d.setTypeid(TypeID.类别);
					break;
				case 1:
					title = "品牌";
					d.setTypeid(TypeID.品牌);
					break;
				case 2:
					title = "单位";
					d.setTypeid(TypeID.单位);
					break;
				}
				String value = JOptionPane.showInputDialog(V_Dictionary.this, "请输入"+title+"的名称:",title,JOptionPane.PLAIN_MESSAGE);
				if(value!=null && !"".equals(value)) {
					d.setName(value);
					showMessage(dictService.saveDictionary(d), "添加");
				}

			}
		});
		p_btns_1.add(btn_new);

		JButton btn_reset = new JButton("重命名");
		btn_reset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//获取的JTree对象
				//				DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) tree_type.getModel();
				//				System.out.println(tree_type.getSelectionCount());// 获取选中的节点数
				//				System.out.println(tree_type.getSelectionModel()); 
				//				System.out.println(tree_type.getSelectionPath()); //获取选中的节点
				//				System.out.println(Arrays.toString(tree_type.getSelectionPaths()));//获取当前节点和父节点
				//				System.out.println(Arrays.toString(tree_type.getSelectionRows()));//获取所有选中节点的位置

				JTree tree = null;
				int tabIndex = tabbedPane.getSelectedIndex();
				String title = "";
				switch (tabIndex) {
				case 0:
					title = "类别";
					tree = tree_type;
					break;
				case 1:
					title = "品牌";
					tree = tree_brand;
					break;
				case 2:
					title = "单位";
					tree = tree_unit;
					break;
				}
				if(tree.getSelectionPath()!=null) {
					//获取当前选中的节点的值
					String selectValue =  CastUtil.castString(tree.getSelectionPath().getLastPathComponent());
					Object value =JOptionPane.showInputDialog(V_Dictionary.this, "请输入"+title+"的名称:",title,JOptionPane.PLAIN_MESSAGE,null,null,selectValue);
					if(value!=null && !"".equals(value)) {
						//获取选中的节点
						DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) tree.getSelectionPath().getLastPathComponent();
						Dictionary d= (Dictionary) dmtn.getUserObject();
						d.setName(CastUtil.castString(value));

						showMessage(dictService.updateDictionaryById(d), "修改");;
					}
				}else {
					showMessage(false, "请选择修改的节点,");
				}
			}
		});
		p_btns_1.add(btn_reset);

		JButton btn_delete = new JButton("删除类别");
		btn_delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JTree tree = null;
				int tabIndex = tabbedPane.getSelectedIndex();
				switch (tabIndex) {
				case 0:
					tree = tree_type;
					break;
				case 1:
					tree = tree_brand;
					break;
				case 2:
					tree = tree_unit;
					break;
				}
				//	获取树节点的数据模型
				DefaultTreeModel dtm = (DefaultTreeModel) tree.getModel();
				//通过节点的数据模型,删除选中的节点对象
				TreePath selectionPath = tree.getSelectionPath();
				if(selectionPath!=null) {
					//获取删除节点对象的最后一节点
					MutableTreeNode mtn = (MutableTreeNode) selectionPath.getLastPathComponent();
					//询问是否删除
					int n = JOptionPane.showConfirmDialog(V_Dictionary.this, "你确定删除吗?","删除",JOptionPane.OK_CANCEL_OPTION);
					if(n==0) {
						//删除根据编号删除
						DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) mtn;
						//获取添加到节点中的对象,节点存储的是什么,获取的就是什么
						Dictionary dict = (Dictionary) dmtn.getUserObject();//被选中的字典对象
						showMessage(dictService.deleteDictionaryById(dict.getId()), "删除");
						//删除节点
						dtm.removeNodeFromParent(mtn);
					}
				}else {
					//未选中节点
					JOptionPane.showMessageDialog(V_Dictionary.this, "请选择节点后删除!");
				}

			}
		});
		p_btns_1.add(btn_delete);
		//初始化三颗
		initTree();
	}

	/**
	 * 消息提示
	 * @param bool
	 * @param msg
	 */
	public void showMessage(boolean bool,String msg) {
		if(bool) {
			JOptionPane.showMessageDialog(V_Dictionary.this, msg+"成功!",msg,JOptionPane.INFORMATION_MESSAGE);
			initTree();//成功后,调用初始化树
		}else {
			JOptionPane.showMessageDialog(V_Dictionary.this, msg+"失败!",msg,JOptionPane.ERROR_MESSAGE);
		}
	}
	/**
	 * 初始化三颗树节点对象
	 */
	public void initTree() {
		initTypeTree();
		initBrandTree();
		initUnitTree();
	}
	/**
	 * 初始化单位的方法
	 */
	public void initUnitTree() {
		List<Dictionary> brandList = dictService.findDictionaryByTypeID(TypeID.单位);
		DefaultMutableTreeNode dmtn = new DefaultMutableTreeNode("单位节点");
		for (Dictionary unit : brandList) {
			dmtn.add(new DefaultMutableTreeNode(unit));

		}
		DefaultTreeModel dtm = new DefaultTreeModel(dmtn);
		tree_unit.setModel(dtm);
	}
	/**
	 * 初始化类别的方法
	 */
	public void initTypeTree() {
		List<Dictionary> brandList = dictService.findDictionaryByTypeID(TypeID.类别);
		DefaultMutableTreeNode dmtn = new DefaultMutableTreeNode("类别节点");
		for (Dictionary type : brandList) {
			dmtn.add(new DefaultMutableTreeNode(type));

		}
		DefaultTreeModel dtm = new DefaultTreeModel(dmtn);
		tree_type.setModel(dtm);
	}
	/**
	 * 初始化品牌的方法
	 */
	public void initBrandTree() {
		List<Dictionary> brandList = dictService.findDictionaryByTypeID(TypeID.品牌);
		DefaultMutableTreeNode dmtn = new DefaultMutableTreeNode("品牌节点");
		for (Dictionary brand : brandList) {
			dmtn.add(new DefaultMutableTreeNode(brand));
		}
		DefaultTreeModel dtm = new DefaultTreeModel(dmtn);
		tree_brand.setModel(dtm);
	}
}
