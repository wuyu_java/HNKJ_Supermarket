package org.lq.sm.view.sales;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import org.lq.sm.entity.Employee;
import org.lq.sm.entity.SalesOrder;
import org.lq.sm.entity.Vip;
import org.lq.sm.service.SalesOrderService;
import org.lq.sm.service.iml.SalesOrderServiceImpl;

public class v_salesDocumens extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JComboBox cb_code;
	private Map<String,Integer> orderMap = null;
	private JLabel lab_count;
	private List<SalesOrder> saleOrderList = new ArrayList<>();
	public List<SalesOrder> getSaleOrderList() {
		return saleOrderList;
	}
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					v_salesDocumens frame = new v_salesDocumens();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public v_salesDocumens() {
		setTitle("取单");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 677, 403);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		cb_code = new JComboBox();
		cb_code.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
//				System.out.println(e.getStateChange()+"\t"+e.getItem());
				if(e.getStateChange()==1) {
					String code = e.getItem().toString();
					lab_count.setText(orderMap.get(code)+"");
					initTable(code);
				}
			}
		});
		cb_code.setBounds(77, 30, 121, 23);
		contentPane.add(cb_code);
		
		JLabel label = new JLabel("单号:");
		label.setBounds(10, 34, 54, 15);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("商品数量:");
		label_1.setBounds(247, 34, 54, 15);
		contentPane.add(label_1);
		
		lab_count = new JLabel("0");
		lab_count.setBounds(338, 34, 54, 15);
		contentPane.add(lab_count);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 78, 641, 276);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"\u8BA2\u5355\u53F7", "\u5546\u54C1\u540D\u79F0", "\u5BA2\u6237\u540D\u79F0", "\u8BA2\u5355\u65F6\u95F4"
			}
		));
		scrollPane.setViewportView(table);
		
		button = new JButton("取单");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		button.setBounds(447, 30, 93, 23);
		contentPane.add(button);
		
		initCb();
	}
	
	
	
	SalesOrderService sos = new SalesOrderServiceImpl();
	private JButton button;
	public void initCb() {
		DefaultComboBoxModel<String> cbm = new DefaultComboBoxModel<String>();
		cbm.addElement("--请选择--");
		orderMap = sos.getMountSaleOrderMap();
		Set<String> kets = orderMap.keySet();
		for (String key : kets) {
			cbm.addElement(key);
		}
		cb_code.setModel(cbm);
	}
	
	
	public void initTable(String code) {
		saleOrderList.clear();
		DefaultTableModel dtm =  (DefaultTableModel) table.getModel();
		int count = dtm.getRowCount();
		for (int i = 0; i < count; i++) {
			dtm.removeRow(0);
		}
		List<SalesOrder> list = sos.getMountSaleOrder();
		for (SalesOrder s : list) {
			if(s.getOrderid().equals(code)) {
				saleOrderList.add(s);
				if(s.getVid()==0) {
					Vip vip = new Vip();
					vip.setName("匿名");
					s.setVip(vip);
				}
				if(s.getEid() == -1) {
					Employee emp = new Employee();
					emp.setName("--请选择--");
					s.setEmp(emp);
				}
				dtm.addRow(new Object[] {
						s.getOrderid(),
						s.getGoods().getName(),
						s.getVip().getName(),
						s.getCreatedate()
						
				});
			}
		}
		
	}
	
	
}
