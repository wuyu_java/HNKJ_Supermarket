package org.lq.sm.view.sales;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Date;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;

import org.lq.sm.entity.Employee;
import org.lq.sm.entity.Vip;
import org.lq.sm.service.EmployeeService;
import org.lq.sm.service.VipService;
import org.lq.sm.service.iml.EmployeeServiceImpl;
import org.lq.sm.service.iml.VipServiceImpl;
import org.lq.sm.util.AutoNo;
import org.lq.sm.util.CastUtil;
import org.lq.sm.view.vip.V_selectVIP;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class v_salesGoods extends JPanel {
	private JTable tab_sales;
	private VipService vs = new VipServiceImpl();
	private JButton btn_select;
	private JLabel lab_name;
	private JLabel lab_zk;
	private JLabel lab_score;
	private JComboBox cb_emp;
	private JLabel lbl_currtime;
	private JLabel lbl_no;
	private Vip vip=null;
	private JTextField txt_phone;
	public Vip getVip() {
		return vip;
	}
	public JLabel getLbl_no() {
		return lbl_no;
	}
	public JTable getTab_sales() {
		return tab_sales;
	}
	public JComboBox getCb_emp() {
		return cb_emp;
	}
	/**
	 * Create the panel.
	 */
	public v_salesGoods() {
		setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		add(panel, BorderLayout.NORTH);

		JLabel lblNewLabel = new JLabel("销售员 :");

		cb_emp = new JComboBox();

		JLabel label = new JLabel("会员 :");

		txt_phone = new JTextField();
		txt_phone.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				//按的回车
				if(e.getKeyCode()==10) {
					List<Vip> vips = vs.findVipLike(txt_phone.getText());
					if(vips.size()>1) {
						selectVip();
					}else if(vips.size() == 1){
						Vip vip =  vips.get(0);
						v_salesGoods.this.vip = vip;
						lab_name.setText(vip.getName());
						lab_score.setText(vip.getIntegral()+"");
						lab_zk.setText(vip.getDiscount()+"");
					}else {
						JOptionPane.showMessageDialog(v_salesGoods.this, "没有找到此会员!");
					}
				}
			}
		});

		JLabel label_1 = new JLabel("姓名:");

		lab_name = new JLabel("张三");
		lab_name.setForeground(Color.BLUE);

		JLabel label_3 = new JLabel("折扣:");

		lab_zk = new JLabel("1");

		JLabel label_5 = new JLabel("积分 :");

		lab_score = new JLabel("55");
		lab_score.setForeground(Color.RED);

		JLabel label_7 = new JLabel("销售日期:");

		lbl_no = new JLabel("123456");

		JLabel label_9 = new JLabel("流水号:");

		btn_select = new JButton("选择");
		btn_select.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectVip();
			}
		});

		lbl_currtime = new JLabel("yyyy-MM-dd HH:mm:ss");
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
						.addContainerGap()
						.addComponent(lblNewLabel)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(cb_emp, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
						.addGap(18)
						.addComponent(label)
						.addGap(18)
						.addComponent(txt_phone, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(btn_select)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(label_1)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(lab_name)
						.addGap(29)
						.addComponent(label_3)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(lab_zk)
						.addGap(18)
						.addComponent(label_5)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(lab_score)
						.addPreferredGap(ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
						.addComponent(label_9)
						.addGap(18)
						.addComponent(lbl_no)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(label_7)
						.addGap(18)
						.addComponent(lbl_currtime)
						.addGap(36))
				);
		gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNewLabel)
								.addComponent(cb_emp, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(label)
								.addComponent(txt_phone, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(label_1)
								.addComponent(lab_name)
								.addComponent(label_3)
								.addComponent(lab_zk)
								.addComponent(label_5)
								.addComponent(lab_score)
								.addComponent(label_7)
								.addComponent(lbl_no)
								.addComponent(label_9)
								.addComponent(btn_select)
								.addComponent(lbl_currtime))
						.addGap(0, 0, Short.MAX_VALUE))
				);
		panel.setLayout(gl_panel);

		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);

		tab_sales = new JTable();
		tab_sales.setModel(new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
						"\u7F16\u53F7", "\u540D\u79F0", "\u6570\u91CF", "\u539F\u4EF7", "\u552E\u4EF7", "\u5C0F\u8BA1", "\u79EF\u5206", "\u9500\u552E\u5458", "\u5907\u6CE8"
				}
				));
		scrollPane.setViewportView(tab_sales);
		initEmp();

		new Thread() {
			public void run() {
				while(true) {
					lbl_currtime.setText(CastUtil.format(new Date(), CastUtil.TIME_DATE_PATTERN));
					try {
						sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			};
		}.start();
		
		lbl_no.setText(AutoNo.getOrderListNo()+"");
	}

	public void initEmp() {
		EmployeeService es = new EmployeeServiceImpl();
		List<Employee> list = es.findEmployeeAll();
		Employee e = new Employee();
		e.setEmpid(-1);
		e.setName("--请选择--");
		list.add(0,e);
		DefaultComboBoxModel<Employee> emps = new DefaultComboBoxModel<Employee>(list.toArray(new Employee[] {}));
		cb_emp.setModel(emps);
	}

	/**
	 * 选择会员
	 */
	public void selectVip() {
		V_selectVIP select = new V_selectVIP();
		select.setBounds(v_salesGoods.this.getX(), v_salesGoods.this.getY(), select.getWidth(), select.getHeight());
		select.setVisible(true);
		select.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				Vip vip =vs.getById(select.getVipId());
				v_salesGoods.this.vip = vip;
				if(vip!=null) {
					lab_name.setText(vip.getName());
					lab_score.setText(vip.getIntegral()+"");
					lab_zk.setText(vip.getDiscount()+"");
				}
			}
		});
	}
	
	public void clear() {
		DefaultTableModel dtm = (DefaultTableModel) getTab_sales().getModel();
		int count = dtm.getRowCount();
		for(int i=0;i<count;i++) {
			dtm.removeRow(0);
		}
		txt_phone.setText("");
		this.vip=null;
		lab_name.setText("匿名");
		lab_zk.setText("");
		lab_score.setText("0");
		lbl_no.setText(AutoNo.getOrderListNo()+"");
		
	}
	
	public void showVip(Vip vip) {
		
		lab_name.setText(vip.getName());
		lab_score.setText(vip.getIntegral()+"");
		lab_zk.setText(vip.getDiscount()+"");
	}
}
