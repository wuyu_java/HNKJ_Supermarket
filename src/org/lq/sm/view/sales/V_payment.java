package org.lq.sm.view.sales;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;

import org.lq.sm.util.CastUtil;
import org.lq.sm.util.MathUtil;
import org.lq.sm.view.goods.DocumentAdapter;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class V_payment extends JFrame {

	private JPanel contentPane;
	private JTextField txt_totalPrice;
	private JTextField txt_money;
	private JLabel label_2;
	private JLabel lab_result2;
	private double totalPrice;
	private JLabel label_3;
	private JLabel lab_result1;
	private double actual;
	private double returnMoney;

	public double getActual() {
		return actual;
	}

	public double getReturnMoney() {
		return returnMoney;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_payment frame = new V_payment();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_payment() {
		setTitle("结算");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 380);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("应收:");
		label.setFont(new Font("宋体", Font.BOLD, 36));
		label.setBounds(68, 10, 99, 72);
		contentPane.add(label);
		
		txt_totalPrice = new JTextField();
		txt_totalPrice.setForeground(Color.RED);
		txt_totalPrice.setEditable(false);
		txt_totalPrice.setFont(new Font("宋体", Font.BOLD, 26));
		txt_totalPrice.setBounds(177, 26, 170, 37);
		contentPane.add(txt_totalPrice);
		txt_totalPrice.setColumns(10);
		
		JLabel label_1 = new JLabel("现金:");
		label_1.setFont(new Font("宋体", Font.BOLD, 36));
		label_1.setBounds(68, 72, 99, 72);
		contentPane.add(label_1);
		
		txt_money = new JTextField();
		Document document = txt_money.getDocument();
		document.addDocumentListener(new DocumentAdapter() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				double moeny =  CastUtil.castDouble(txt_money.getText());
				lab_result1.setText(moeny+"");
				double rs = moeny - totalPrice;
				lab_result2.setText(MathUtil.round(rs, 2)+"");
			}
			@Override
			public void removeUpdate(DocumentEvent e) {
			}
		});
		txt_money.setFont(new Font("宋体", Font.BOLD, 26));
		txt_money.setColumns(10);
		txt_money.setBounds(177, 92, 170, 37);
		contentPane.add(txt_money);
		
		label_2 = new JLabel("找零:");
		label_2.setFont(new Font("宋体", Font.BOLD, 36));
		label_2.setBounds(253, 154, 99, 72);
		contentPane.add(label_2);
		
		lab_result2 = new JLabel("000000");
		lab_result2.setForeground(new Color(220, 20, 60));
		lab_result2.setFont(new Font("宋体", Font.BOLD, 28));
		lab_result2.setBounds(263, 219, 107, 50);
		contentPane.add(lab_result2);
		
		JButton button = new JButton("结算");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
 				actual = CastUtil.castDouble(txt_money.getText());
 				double moeny =  CastUtil.castDouble(txt_money.getText());
 				returnMoney = moeny - totalPrice;
 				dispose();
			}
		});
		button.setBounds(87, 293, 93, 23);
		contentPane.add(button);
		
		JButton button_1 = new JButton("取消");
		button_1.setBounds(253, 293, 93, 23);
		contentPane.add(button_1);
		
		label_3 = new JLabel("实收:");
		label_3.setFont(new Font("宋体", Font.BOLD, 36));
		label_3.setBounds(68, 154, 99, 72);
		contentPane.add(label_3);
		
		lab_result1 = new JLabel("000000");
		lab_result1.setForeground(Color.ORANGE);
		lab_result1.setFont(new Font("宋体", Font.BOLD, 28));
		lab_result1.setBounds(73, 219, 107, 50);
		contentPane.add(lab_result1);
	}

	public V_payment(double totalPrice) {
		this();
		this.totalPrice = totalPrice;
		this.txt_totalPrice.setText("￥"+totalPrice+"");
		this.txt_money.setText(totalPrice+"");
	}
}
