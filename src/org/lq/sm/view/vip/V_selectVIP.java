package org.lq.sm.view.vip;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JSplitPane;
import javax.swing.JLabel;
import java.awt.FlowLayout;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import org.lq.sm.entity.Vip;
import org.lq.sm.service.VipService;
import org.lq.sm.service.iml.VipServiceImpl;
import org.lq.sm.util.CastUtil;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Dialog.ModalExclusionType; 

public class V_selectVIP extends JFrame {

	private JPanel contentPane;
	private JTextField txt_value;
	private JTable table;
	private JTable tab_vip;
	private int vipId;
	public int getVipId() {
		return vipId;
	}
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_selectVIP frame = new V_selectVIP();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_selectVIP() {
		setTitle("选择会员");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 674, 445);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JSplitPane splitPane = new JSplitPane();
		//设置分割容器的,平分大小
		splitPane.addComponentListener(new ComponentAdapter() {  
			@Override  
			public void componentResized(ComponentEvent e) {  
				splitPane.setDividerLocation(0.3);  
			}  
		});  
		splitPane.setBackground(Color.WHITE);
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		contentPane.add(splitPane, BorderLayout.CENTER);

		JScrollPane scrollPane = new JScrollPane();
		splitPane.setRightComponent(scrollPane);

		table = new JTable();
		table.setBackground(Color.WHITE);
		table.setModel(new DefaultTableModel(
				new Object[][] {
					{null, null, null, null, null},
				},
				new String[] {
						"\u65E5\u671F", "\u540D\u79F0", "\u6570\u91CF", "\u91D1\u989D", "\u5907\u6CE8"
				}
				));
		scrollPane.setViewportView(table);

		JScrollPane scrollPane_1 = new JScrollPane();
		splitPane.setLeftComponent(scrollPane_1);

		tab_vip = new JTable();
		tab_vip.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount()==2) {
					int index = tab_vip.getSelectedRow();
					vipId = CastUtil.castInt(tab_vip.getValueAt(index, 0));
					dispose();
				}
			}
		});
		tab_vip.setModel(new DefaultTableModel(
				new Object[][] {
					{null, null, null, null, null, null, null, null},
				},
				new String[] {
						"id", "\u5361\u53F7", "\u59D3\u540D", "\u79EF\u5206", "\u6298\u6263", "\u751F\u65E5", "\u624B\u673A\u53F7", "\u6D88\u8D39\u91D1\u989D"
				}
				) {
			boolean[] columnEditables = new boolean[] {
					true, false, false, false, false, false, false, false
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		tab_vip.getColumnModel().getColumn(0).setMinWidth(1);
		tab_vip.getColumnModel().getColumn(0).setMaxWidth(1);
		scrollPane_1.setViewportView(tab_vip);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		panel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));

		JLabel lblNewLabel = new JLabel("卡号/姓名/电话：");
		panel.add(lblNewLabel);

		txt_value = new JTextField();
		panel.add(txt_value);
		txt_value.setColumns(10);

		JButton btnNewButton = new JButton("查询");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				initVIPTable(new VipServiceImpl().findVipLike(txt_value.getText()));
			}
		});
		btnNewButton.setBackground(Color.ORANGE);

		panel.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("全部");
		btnNewButton_1.setBackground(Color.ORANGE);
		panel.add(btnNewButton_1);

		JButton btnNewButton_2 = new JButton("新增会员");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				V_vipAdd add = new V_vipAdd();
				add.setBounds(V_selectVIP.this.getX(), V_selectVIP.this.getY(), add.getWidth(), add.getHeight());
				add.setVisible(true);
				add.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosed(WindowEvent e) {
						initVIPTable(new VipServiceImpl().findVipAll());
					}
				});
			}
		});
		btnNewButton_2.setBackground(Color.ORANGE);
		panel.add(btnNewButton_2);

		JButton btnNewButton_3 = new JButton("关闭");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnNewButton_3.setBackground(Color.ORANGE);
		panel.add(btnNewButton_3);
		VipService vs = new VipServiceImpl();
		initVIPTable(vs.findVipAll());
	}


	public void initVIPTable(List<Vip> list) {
		DefaultTableModel dtm = (DefaultTableModel) tab_vip.getModel();
		int count = dtm.getRowCount();
		for (int i = 0; i < count; i++) {
			dtm.removeRow(0);
		}

		for (Vip vip : list) {
			dtm.addRow(new Object[] {
					vip.getNo(),
					vip.getCard(),
					vip.getName(),
					vip.getIntegral(),
					vip.getDiscount(),
					vip.getBirthday(),
					vip.getPhoneNumber(),
					vip.getBaseamount()
			});
		}
	}
}
