package org.lq.sm.view.vip;

import java.awt.BorderLayout;
import java.util.List;

import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import org.lq.sm.entity.Vip;
import org.lq.sm.service.VipService;
import org.lq.sm.service.iml.VipServiceImpl;
import org.lq.sm.util.CastUtil;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.ListSelectionModel;

public class V_vipdata extends JPanel {
	private JTable table;
	private JEditorPane content;

	/**
	 * Create the panel.
	 */
	public V_vipdata() {
		setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane_1 = new JScrollPane();
		add(scrollPane_1, BorderLayout.NORTH);

		JSplitPane splitPane = new JSplitPane();
		add(splitPane, BorderLayout.CENTER);

		JScrollPane scrollPane = new JScrollPane();
		splitPane.setRightComponent(scrollPane);

		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setShowVerticalLines(false);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int index = table.getSelectedRow();
				VipService vs = new VipServiceImpl();
				content.setContentType("text/html");
 				content.setText(vs.getByIdRuntenHTML(CastUtil.castInt(table.getValueAt(index, 0))));
			}
		});
		table.setModel(new DefaultTableModel(
				new Object[][] {
					{"", null, null, null, null, null, null, null, null, null, null},
					{null, null, null, null, null, null, null, null, null, null, null},
				},
				new String[] {
						"id", "\u5361\u53F7", "\u59D3\u540D", "\u7EA7\u522B", "\u6298\u6263", "\u79EF\u5206", "\u6D88\u8D39\u603B\u989D", "\u751F\u65E5", "\u624B\u673A", "\u529E\u5361\u65E5\u671F", "\u5907\u6CE8"
				}
				));
		table.getColumnModel().getColumn(0).setMinWidth(1);
		table.getColumnModel().getColumn(0).setMaxWidth(1);
		scrollPane.setViewportView(table);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		splitPane.setLeftComponent(scrollPane_2);
		
		content = new JEditorPane();
		scrollPane_2.setViewportView(content);
		
		initVIPTable(new VipServiceImpl().findVipAll());
	}

	public void initVIPTable(List<Vip> list) {
		DefaultTableModel dtm = (DefaultTableModel) table.getModel();
		int count = dtm.getRowCount();
		for (int i = 0; i < count; i++) {
			dtm.removeRow(0);
		}

		for (Vip vip : list) {
			dtm.addRow(new Object[] {
					vip.getNo(),
					vip.getCard(),
					vip.getName(),
					vip.getLevelObj().getName(),
					vip.getDiscount(),
					vip.getIntegral(),
					vip.getBaseamount(),
					vip.getBirthday(),
					vip.getPhoneNumber(),
					vip.getHandleCardDate(),
					vip.getDesc()
			});
		}
	}

}
