package org.lq.sm.view.vip;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.Date;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import org.lq.sm.entity.Dictionary;
import org.lq.sm.entity.TypeID;
import org.lq.sm.entity.Vip;
import org.lq.sm.service.DictionaryService;
import org.lq.sm.service.VipService;
import org.lq.sm.service.iml.DictionaryServiceImpl;
import org.lq.sm.service.iml.VipServiceImpl;
import org.lq.sm.util.AutoNo;
import org.lq.sm.util.CastUtil;

import com.eltima.components.ui.DatePicker;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.awt.event.ActionEvent;

public class V_vipAdd extends JFrame {

	private JPanel contentPane;
	private JComboBox cb_level;
	private JCheckBox cb_auto;
	private JTextField text_card;
	private JTextField text_name;
	private JTextField text_phone;
	private DatePicker dp_brithday;
	private JTextField text_discount;
	private JTextField text_integral;
	private DatePicker dp_cadedata;
	private JTextField text_address;
	private JTextField text_desc;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_vipAdd frame = new V_vipAdd();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_vipAdd() {
		setTitle("会员资料维护");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 315, 438);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(5, 5, 289, 356);
		panel.setBorder(new TitledBorder(null, "\u4F1A\u5458\u8D44\u6599", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblNewLabel = new JLabel("卡号：");
		lblNewLabel.setBounds(34, 32, 45, 15);
		panel.add(lblNewLabel);

		text_card = new JTextField();
		text_card.setBounds(78, 29, 118, 21);
		panel.add(text_card);
		text_card.setColumns(10);

		cb_auto = new JCheckBox("自动");
		cb_auto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(cb_auto.isSelected()) {
					text_card.setText(AutoNo.getVipNo().toString());
				}else {
					text_card.setText("");
				}
			}
		});
		cb_auto.setBounds(202, 28, 60, 23);
		panel.add(cb_auto);

		JLabel lblNewLabel_1 = new JLabel("姓名：");
		lblNewLabel_1.setBounds(34, 66, 54, 15);
		panel.add(lblNewLabel_1);

		text_name = new JTextField();
		text_name.setBounds(78, 63, 93, 21);
		panel.add(text_name);
		text_name.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel("手机：");
		lblNewLabel_2.setBounds(34, 104, 54, 15);
		panel.add(lblNewLabel_2);

		text_phone = new JTextField();
		text_phone.setBounds(78, 101, 150, 21);
		panel.add(text_phone);
		text_phone.setColumns(10);

		JLabel lblNewLabel_3 = new JLabel("生日：");
		lblNewLabel_3.setBounds(34, 139, 54, 15);
		panel.add(lblNewLabel_3);

		//时间插件
		dp_brithday = new DatePicker();
		dp_brithday.setBounds(78, 139, 150, 21);
		panel.add(dp_brithday);

		JLabel lblNewLabel_4 = new JLabel("类别：");
		lblNewLabel_4.setBounds(34, 178, 54, 15);
		panel.add(lblNewLabel_4);

		cb_level = new JComboBox();
		cb_level.setBounds(78, 174, 93, 23);
		panel.add(cb_level);

		JLabel lblNewLabel_5 = new JLabel("会员折扣：");
		lblNewLabel_5.setBounds(10, 214, 67, 15);
		panel.add(lblNewLabel_5);

		text_discount = new JTextField();
		text_discount.setBounds(78, 211, 66, 21);
		panel.add(text_discount);
		text_discount.setColumns(10);

		JLabel lblNewLabel_6 = new JLabel("积分：");
		lblNewLabel_6.setBounds(154, 214, 54, 15);
		panel.add(lblNewLabel_6);

		text_integral = new JTextField();
		text_integral.setBounds(202, 211, 60, 21);
		text_integral.setText("0");
		panel.add(text_integral);
		text_integral.setColumns(10);

		JLabel lblNewLabel_7 = new JLabel("开卡日期：");
		lblNewLabel_7.setBounds(10, 247, 64, 15);
		panel.add(lblNewLabel_7);

		//时间插件
		dp_cadedata = new DatePicker();
		dp_cadedata.getInnerButton().setEnabled(false);
		dp_cadedata.getInnerTextField().setEditable(false);
		dp_cadedata.setBounds(78, 241, 150, 21);
		panel.add(dp_cadedata);

		JLabel lblNewLabel_8 = new JLabel("地址：");
		lblNewLabel_8.setBounds(34, 285, 54, 15);
		panel.add(lblNewLabel_8);

		text_address = new JTextField();
		text_address.setBounds(78, 282, 150, 21);
		panel.add(text_address);
		text_address.setColumns(10);

		JLabel lblNewLabel_9 = new JLabel("备注：");
		lblNewLabel_9.setBounds(34, 317, 54, 15);
		panel.add(lblNewLabel_9);

		text_desc = new JTextField();
		text_desc.setBounds(78, 314, 150, 21);
		panel.add(text_desc);
		text_desc.setColumns(10);

		JCheckBox cb_save = new JCheckBox("保存新增");
		cb_save.setBounds(5, 367, 103, 23);
		contentPane.add(cb_save);

		JButton btn_save = new JButton("保存");
		btn_save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Vip v = new Vip();
				v.setAddress(text_address.getText());
				try {
					v.setBirthday(CastUtil.parse2(dp_brithday.getInnerTextField().getText(), CastUtil.TIME_DATE_PATTERN));
					v.setHandleCardDate(CastUtil.parse2(dp_cadedata.getInnerTextField().getText(), CastUtil.TIME_DATE_PATTERN));
				} catch (ParseException e1) {
					e1.printStackTrace();
				}
				v.setCard(CastUtil.castInt(text_card.getText()));
				v.setDesc(text_desc.getText());
				v.setDiscount(text_discount.getText());
				//					v.setGender(gender);
				v.setIntegral(CastUtil.castInt(text_integral.getText()));
				v.setName(text_name.getText());
				v.setPhoneNumber(text_phone.getText());
				Dictionary level = (Dictionary) cb_level.getSelectedItem();
				v.setLevel(level.getId());
				v.setIntegral(CastUtil.castInt(text_integral.getText()));
				v.setAddress(text_address.getText());
				VipService vs = new VipServiceImpl();
				if(cb_save.isSelected()) {
					if(vs.saveVip(v)) {
						reset();
					}
				}else {
					if(vs.saveVip(v)) {
						dispose();//没有选中,保存完成后关闭窗体
					}
				}
			}
		});
		btn_save.setIcon(null);
		btn_save.setBounds(114, 367, 82, 23);
		contentPane.add(btn_save);

		JButton btn_off = new JButton("取消");
		btn_off.setIcon(null);
		btn_off.setBounds(206, 367, 82, 23);
		contentPane.add(btn_off);
		initLevel();
		dp_cadedata.getInnerTextField().setText(CastUtil.format(new Date(),CastUtil.TIME_DATE_PATTERN));
	}

	public void initLevel() {
		DictionaryService ds = new DictionaryServiceImpl();
		List<Dictionary> list = ds.findDictionaryByTypeID(TypeID.会员级别);
		DefaultComboBoxModel<Dictionary> dcbm = new DefaultComboBoxModel<Dictionary>(list.toArray(new Dictionary[] {}));
		cb_level.setModel(dcbm);
	}
	/**
	 * 	重置
	 */
	public void reset() {
		text_address.setText("");
		text_card.setText("");
		text_desc.setText("");
		text_discount.setText("");
		text_integral.setText("");
		text_name.setText("");
		text_phone.setText("");
		dp_brithday.getInnerTextField().setText("");
		dp_cadedata.getInnerTextField().setText(CastUtil.format(new Date()));
		cb_auto.setSelected(false);
		cb_level.setSelectedIndex(0);

	}
}
