package org.lq.sm.view.vip;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import org.lq.sm.entity.Dictionary;
import org.lq.sm.entity.TypeID;
import org.lq.sm.service.DictionaryService;
import org.lq.sm.service.iml.DictionaryServiceImpl;
import org.lq.sm.util.CastUtil;
import org.lq.sm.view.dict.V_Dictionary;

import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.JButton;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class V_level extends JFrame {

	private JPanel contentPane;
	private static JTree tree;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_level frame = new V_level();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_level() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 281, 380);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		tree = new JTree();
		scrollPane.setViewportView(tree);
		
		JPopupMenu popupMenu = new JPopupMenu();
		addPopup(tree, popupMenu);
		
		JMenuItem btn_update = new JMenuItem("修改");
		btn_update.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) tree.getSelectionPath().getLastPathComponent();
				Dictionary d = (Dictionary) dmtn.getUserObject();
				Object value =JOptionPane.showInputDialog(V_level.this, "请输入会员级别的名称:","会员级别",JOptionPane.PLAIN_MESSAGE,null,null,d.getName());
				d.setName(CastUtil.castString(value));
				DictionaryService ds = new DictionaryServiceImpl();
				if(ds.updateDictionaryById(d)) {
					initTree();
				}
			}
		});
		popupMenu.add(btn_update);
		
		JMenuItem btn_del = new JMenuItem("删除");
		btn_del.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) tree.getSelectionPath().getLastPathComponent();
				Dictionary d = (Dictionary) dmtn.getUserObject();
				int n = JOptionPane.showConfirmDialog(V_level.this, "确定删除吗?");
				if(n==0) {
					DictionaryService ds = new DictionaryServiceImpl();
					if(ds.deleteDictionaryById(d.getId())) {
						initTree();
					};
				}
			}
		});
		popupMenu.add(btn_del);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		
		JButton btn_add = new JButton("添加");
		btn_add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
 				String value = JOptionPane.showInputDialog(V_level.this, "请输入会员的级别名称");
 				DictionaryService ds = new DictionaryServiceImpl();
 				Dictionary d = new Dictionary();
 				d.setName(value);
 				d.setTypeid(TypeID.会员级别);
 				if(ds.saveDictionary(d)) {
 					initTree();
 				};
			}
		});
		panel.add(btn_add);
		initTree();
		
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				int index = tree.getRowForLocation(e.getX(), e.getY());
				if(index > 0) {
					tree.setSelectionRow(index);
					popup.show(e.getComponent(), e.getX()+20, e.getY()+5);
				}
			}
		});
	}
	
	/**
	 * 	初始化树
	 */
	public void initTree() {
		DictionaryService ds = new DictionaryServiceImpl();
		List<Dictionary> list = ds.findDictionaryByTypeID(TypeID.会员级别);
		DefaultMutableTreeNode dmtn = new DefaultMutableTreeNode("会员级别");
		for (Dictionary d : list) {
			dmtn.add(new DefaultMutableTreeNode(d));
		}
		DefaultTreeModel dtm = new DefaultTreeModel(dmtn);
		
		tree.setModel(dtm);
	}
	
	
	
	
	
	
	
	
	
}
