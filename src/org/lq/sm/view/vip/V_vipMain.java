package org.lq.sm.view.vip;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.lq.sm.service.iml.VipServiceImpl;

import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class V_vipMain extends JFrame {

	private JPanel contentPane;
	private JTextField txt_value;
	private V_vipdata panel_vipdata;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_vipMain frame = new V_vipMain();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_vipMain() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 883, 634);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		contentPane.add(panel, BorderLayout.NORTH);
		
		JButton btnNewButton = new JButton("添加");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				V_vipAdd add = new V_vipAdd();
				add.setVisible(true);
			}
		});
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("修改");
		panel.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("删除");
		panel.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("级别设置");
		panel.add(btnNewButton_3);
		
		JComboBox cb_query = new JComboBox();
		cb_query.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange()==2) {
					txt_value.setText("");
				}
			}
		});
		cb_query.setModel(new DefaultComboBoxModel(new String[] {"所有", "卡号", "用户名", "级别"}));
		panel.add(cb_query);
		
		txt_value = new JTextField();
		panel.add(txt_value);
		txt_value.setColumns(10);
		
		JButton btnNewButton_4 = new JButton("查询");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
 				String type = cb_query.getSelectedItem().toString();
 				String value = txt_value.getText();
 				panel_vipdata.initVIPTable(new VipServiceImpl().findVipByValue(type, value));
			}
		});
		btnNewButton_4.setIcon(new ImageIcon(V_vipMain.class.getResource("/images/search.png")));
		panel.add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("导入导出");
		panel.add(btnNewButton_5);
		
		panel_vipdata = new V_vipdata();
		contentPane.add(panel_vipdata, BorderLayout.CENTER);
		
		JPanel panel_2 = new V_vipdetail();
		contentPane.add(panel_2, BorderLayout.SOUTH);
	}

}
