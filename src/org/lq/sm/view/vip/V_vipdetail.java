package org.lq.sm.view.vip;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class V_vipdetail extends JPanel {
	private JTable table;

	/**
	 * Create the panel.
	 */
	public V_vipdetail() {
		setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null, null, null},
			},
			new String[] {
				"\u65E5\u671F", "\u7F16\u53F7", "\u540D\u79F0", "\u7C7B\u522B", "\u54C1\u724C", "\u7968\u4EF7", "\u6298\u6263", "\u552E\u4EF7", "\u6570\u91CF", "\u91D1\u989D", "\u5907\u6CE8", "\u79EF\u5206"
			}
		));
		scrollPane.setViewportView(table);

	}

}
