package org.lq.sm.view.reportform;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.title.LegendTitle;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.lq.sm.service.ReportFormService;
import org.lq.sm.service.iml.ReportFormServiceImpl;

public class V_reportForm extends JFrame {

	private JPanel contentPane;
	private JTabbedPane tabbedPane;
	public JTabbedPane getTabbedPane() {
		return tabbedPane;
	}
	
	private List<JPanel> panels = new ArrayList<>();
	public List<JPanel> getPanels() {
		return panels;
	}
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					V_reportForm frame = new V_reportForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public V_reportForm() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 722, 461);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		tabbedPane = new JTabbedPane(JTabbedPane.RIGHT);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		ReportFormService rfs = new ReportFormServiceImpl();
		DefaultCategoryDataset brandCount = rfs.getBrandCount();
		JFreeChart brand = getBarChart3D(brandCount);
		//将报表显示到窗体中
		ChartFrame cf = new ChartFrame("", brand);
		cf.pack();
		JPanel panel = (JPanel) cf.getContentPane();
		panels.add(panel);
		tabbedPane.addTab("品牌商品统计", new ImageIcon(V_reportForm.class.getResource("/images/sale_48p.png")), panel, null);
		
		JFreeChart type = getPieChart3D(rfs.getGoodsTypeCount());
		ChartFrame cf2 = new ChartFrame("", type);
		cf2.pack();
		JPanel panel_1 = (JPanel) cf2.getContentPane();
		panels.add(panel_1);
		tabbedPane.addTab("类型商品统计", new ImageIcon(V_reportForm.class.getResource("/images/stock_48.png")), panel_1, null);
		
		JFreeChart inventory = getBarChart3D(rfs.getInventory());
		//将报表显示到窗体中
		ChartFrame cf3 = new ChartFrame("", inventory);
		cf3.pack();
		JPanel panel_2 = (JPanel) cf3.getContentPane();
		panels.add(panel_2);
		tabbedPane.addTab("库存数量统计", new ImageIcon(V_reportForm.class.getResource("/images/calendar_48.png")), panel_2, null);
		JFreeChart total = getPieChart3D(rfs.getTotalPrice());
		ChartFrame cf4 = new ChartFrame("", total);
		cf4.pack();
		JPanel panel_3 = (JPanel) cf4.getContentPane();
		panels.add(panel_3);
		tabbedPane.addTab("资金利润统计", new ImageIcon(V_reportForm.class.getResource("/images/Shopping_Basket_48.png")), panel_3, null);


	}
	public JFreeChart getPieChart3D(DefaultPieDataset dataset) {
		JFreeChart chart = ChartFactory.createPieChart3D("水果产量图", dataset, 
				true,// 显示图例
				false, false);
		Font font = new Font("黑体", Font.BOLD, 20);
		Font font2 = new Font("宋体", Font.PLAIN, 15);

		LegendTitle legend = chart.getLegend();
		legend.setItemFont(font2);
		TextTitle title = chart.getTitle();
		title.setFont(font);
		PiePlot plot = (PiePlot) chart.getPlot();
		plot.setLabelFont(font2);
		return chart;
	}

	public JFreeChart getBarChart3D(DefaultCategoryDataset dataset) {
		//设置中文
		StandardChartTheme chartTheme = new StandardChartTheme("CN");
		//设置标题字体
		chartTheme.setExtraLargeFont(new Font("隶书", Font.BOLD, 20));
		//设置图例的字体
		chartTheme.setRegularFont(new Font("宋体", Font.PLAIN, 15));
		//设置纵向轴字体
		chartTheme.setLargeFont(new Font("宋体", Font.PLAIN, 15));

		//设置主题样式
		ChartFactory.setChartTheme(chartTheme);
		JFreeChart  chart = ChartFactory.createBarChart3D(
				"水果产量图", // 图表标题 
				"水果", //目录轴的显示标题X
				"产量", //数值轴的显示标签Y
				dataset,//数据集
				PlotOrientation.VERTICAL,//图表的方向 : 水平,垂直
				true,//是否显示图例
				false,//是否生成工具(窗体中)
				false//是否生成URL连接 (网页中)
				);

		return chart;
	}

}
