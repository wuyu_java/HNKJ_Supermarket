package org.lq.sm.view.main;

import java.awt.Component;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.apache.log4j.Logger;
import org.lq.sm.view.dict.V_Dictionary;
import org.lq.sm.view.employee.V_EmpMain;
import org.lq.sm.view.goods.V_goods;
import org.lq.sm.view.order.V_orderList;
import org.lq.sm.view.reportform.V_reportForm;
import org.lq.sm.view.sales.V_sales;
import org.lq.sm.view.sys.MysqlDataManager;
import org.lq.sm.view.vip.V_vipMain;

public class BtnActionListener implements ActionListener{
	private Logger log = Logger.getLogger(BtnActionListener.class);
	private JTabbedPane tab;
	public BtnActionListener(JTabbedPane tab) {
		this.tab = tab;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton) e.getSource();
		log.info("进入单击事件");
		log.debug(btn);
		switch (btn.getText()) {
		case "日常事务":
			tab.removeAll();
			tab.addTab("商品销售", new ImageIcon(SuperMarketMain.class.getResource("/images/gallery_32.png")), new V_sales().getContentPane(), null);
			tab.addTab("采购进货", new ImageIcon(SuperMarketMain.class.getResource("/images/gallery_32.png")), new V_orderList().getContentPane(), null);
			tab.addTab("商品管理", new ImageIcon(SuperMarketMain.class.getResource("/images/gallery_32.png")), new V_goods().getContentPane(), null);
			tab.addTab("会员管理", new ImageIcon(SuperMarketMain.class.getResource("/images/gallery_32.png")), new V_vipMain().getContentPane(), null);
			tab.updateUI();
			break;
		case "统计分析":
			tab.removeAll();
			V_reportForm form = new V_reportForm();
			List<JPanel> panels = form.getPanels();
			tab.addTab("品牌商品统计", new ImageIcon(SuperMarketMain.class.getResource("/images/sale_48p.png")), panels.get(0), null);
			tab.addTab("类型商品统计", new ImageIcon(SuperMarketMain.class.getResource("/images/stock_48.png")), panels.get(1), null);
			tab.addTab("库存数据统计", new ImageIcon(SuperMarketMain.class.getResource("/images/calendar_48.png")), panels.get(2), null);
			tab.addTab("资金利润统计", new ImageIcon(SuperMarketMain.class.getResource("/images/Shopping_Basket_48.png")), panels.get(3), null);
			tab.updateUI();
			break;
		case "系统设置":
			tab.removeAll();
			tab.addTab("数据字典管理", new ImageIcon(SuperMarketMain.class.getResource("/images/gallery_32.png")), new V_Dictionary().getContentPane(), null);
			tab.addTab("员工数据管理", new ImageIcon(SuperMarketMain.class.getResource("/images/gallery_32.png")), new V_EmpMain().getContentPane(), null);
			tab.addTab("数据备份初始化", new ImageIcon(SuperMarketMain.class.getResource("/images/gallery_32.png")), new MysqlDataManager().getContentPane(), null);
			tab.updateUI();
			break;
		case "软件教程":
			openCHM();
		default:
			break;
		}
	}

	/**
	 * 打开帮助文档
	 */
	public void openCHM() {
		try {
			//调用本地应用程序
			Desktop.getDesktop().open(new File("Help/help.chm"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}


}
