package org.lq.sm.view.main;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.lq.sm.util.CastUtil;
import org.pushingpixels.substance.api.SubstanceLookAndFeel;
import org.pushingpixels.substance.api.skin.OfficeBlack2007Skin;

import javax.swing.JMenuBar;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JTabbedPane;
import java.awt.event.ActionListener;
import java.util.logging.Logger;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class SuperMarketMain extends JFrame {

	private JPanel contentPane;
	private JTabbedPane tabbedPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		JFrame.setDefaultLookAndFeelDecorated(true);
		JDialog.setDefaultLookAndFeelDecorated(true);
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					org.apache.log4j.Logger.getLogger(SuperMarketMain.class).error("进入");
					SubstanceLookAndFeel.setSkin(new OfficeBlack2007Skin());
					SuperMarketMain frame = new SuperMarketMain();
					frame.setVisible(true);
				} catch (Exception e) {
					org.apache.log4j.Logger.getLogger(SuperMarketMain.class).error("错误",e);
					e.printStackTrace();
					
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SuperMarketMain() {
		setTitle("超市管理系统");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds(0, 0, Double.valueOf(screenSize.getWidth()).intValue(), Double.valueOf(screenSize.getHeight()).intValue()-30);
		tabbedPane = new JTabbedPane(JTabbedPane.RIGHT);
		BtnActionListener btnClick = new BtnActionListener(tabbedPane);
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JButton button = new JButton("日常事务");
		button.setIcon(new ImageIcon(SuperMarketMain.class.getResource("/images/monitor_32.png")));
		button.addActionListener(btnClick);
		menuBar.add(button);
		
		JButton button_1 = new JButton("统计分析");
		button_1.addActionListener(btnClick);
		menuBar.add(button_1);
		
		JButton button_2 = new JButton("系统设置");
		button_2.setIcon(new ImageIcon(SuperMarketMain.class.getResource("/images/settings_32.png")));
		button_2.addActionListener(btnClick);
		menuBar.add(button_2);
		
		JButton button_3 = new JButton("软件教程");
		button_3.addActionListener(btnClick);
		menuBar.add(button_3);
		
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(SuperMarketMain.class.getResource("/images/002.jpg")));
		tabbedPane.addTab("", null, lblNewLabel, null);
		
	}

}
