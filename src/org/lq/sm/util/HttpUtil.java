package org.lq.sm.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpUtil {

	/**
	 * 发送网络请求
	 * @param code
	 * @return
	 */
	public static String getHTTP(String code) {
		String msg = "";
		BufferedReader br = null;
		try {
			//创建远程URL连接对象
			URL url = new URL("https://ali-barcode.showapi.com/barcode?code="+code);
			//通过远程URL连接对象打开一个连接,强制转成HttpURLConnection
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestProperty("Authorization", "APPCODE ");
			//发送请求
			conn.connect();
			//			if(200 == conn.getResponseCode()) {
			InputStream is = conn.getInputStream();
			br = new BufferedReader(new InputStreamReader(is));
			String line = "";
			while((line=br.readLine())!=null) {
				msg+=line;
			}
			//			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return msg;
	}

}
