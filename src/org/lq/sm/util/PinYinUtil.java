package org.lq.sm.util;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

public class PinYinUtil {
	
	/**
	 * 	根据字符串进行拼音转换
	 * @param str : 每个汉字的首拼音并大写
	 * @return
	 */
	public static String getFirstPY(String str) {
		StringBuilder sbu = new StringBuilder();
		HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
		format.setCaseType(HanyuPinyinCaseType.UPPERCASE);
		format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
		
		for (int i = 0; i < str.length(); i++) {
			try {
				String py [] = PinyinHelper.toHanyuPinyinStringArray(str.charAt(i),format);
				String p = py[0];
				sbu.append(p.charAt(0));
			} catch (Exception e) {
				sbu.append(str.charAt(i));
			}
		}
		
		return sbu.toString();
	}

}
