package org.lq.sm.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Util {
	
	/**
	 * 字符MD5加密
	 * @param str
	 * @return
	 */
	public static String getMD5Str(String str) {
		//获取加密规则对象
		byte[] digest = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			digest = md.digest(str.getBytes());//10进制数
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// 第一个参数,符号位. 负数 -1,零,0,整数,1 . 第二个参数,整数大数.超过基本数据类型的数据,超出整数位数的二进制数可以处理32位的整运行
		String md5str = new BigInteger(1,digest).toString(16);
		return md5str;
	}

}
