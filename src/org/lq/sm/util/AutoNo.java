package org.lq.sm.util;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

public class AutoNo {


	public static Long getOrderListNo() {
		DataInputStream dis = null;
		DataOutputStream oos = null;
		Date date = new Date();
		//202008121
		String str= CastUtil.format(date, CastUtil.DATE_PATTERN);

		try {
			String dir = System.getProperty("user.dir");
			dis = new DataInputStream(new FileInputStream(dir+"/data/number.bat"));
			long no = dis.readLong();
			str+=no;
			oos = new DataOutputStream(new FileOutputStream(dir+"/data/number.bat"));
			oos.writeLong(++no);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				oos.close();
				dis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return Long.valueOf(str);
	}
	public static Long getVipNo() {
		DataInputStream dis = null;
		DataOutputStream oos = null;
		Date date = new Date();
		//202008121
		long no = 10000;
		try {
			String dir = System.getProperty("user.dir");
			if(new File(dir+"/data/vip.bat").exists()) {
				dis = new DataInputStream(new FileInputStream(dir+"/data/vip.bat"));
				no = dis.readLong();
			}
			oos = new DataOutputStream(new FileOutputStream(dir+"/data/vip.bat"));
			oos.writeLong(++no);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				oos.close();
				if(dis!=null) {
					dis.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return Long.valueOf(no);
	}
}
