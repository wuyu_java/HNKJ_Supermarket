package org.lq.sm.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * 	文件上传工具类
 * @author 杨培林
 * @phone 13603738558
 * 
 * @package org.lq.sm.util
 * @date 2020年8月11日上午10:37:08
 *
 */
public class FileUpload {
	
	
	/**
	 * 	文件复制
	 * @param oldpath 原始路径
	 * @param newpath 新路径
	 */
	public static void copy(String oldpath,String newpath) {
		File oldFile = new File(oldpath);
		File newFile = new File(newpath);
		try(
				InputStream is = new FileInputStream(oldFile);
				BufferedInputStream bis = new BufferedInputStream(is);
				OutputStream os = new FileOutputStream(newFile);
				BufferedOutputStream bos = new BufferedOutputStream(os);
				){
			
			
			int len;
			byte [] bs = new byte[1024];
			while((len = bis.read(bs))!=-1) {
				bos.write(bs,0,len);
			}
			bos.flush();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}






