package org.lq.sm.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *  	小数工具类四舍五入
 * @author 杨培林
 * @phone 13603738558
 * 
 * @package org.lq.sm.util
 * @date 2020年8月8日上午11:36:37
 *
 */
public class MathUtil {
	/**
	 * 进行准确位数的四舍五入处理操作
	 * @param num 要进行四舍五入的数字
	 * @param scale 保留的小数位数
	 * @return 四舍五入的处理结果
	 */
	public static double round(double num,int scale) {
		return Math.round(num * Math.pow(10.0, scale)) / Math.pow(10.0, scale);
	}
	/**
	 * BigDecimal 四舍五入
	 * @param numa
	 * @param scale
	 * @return
	 */
	public static double round(BigDecimal num,int scale) {
		return num.divide(new BigDecimal(1.0),scale,RoundingMode.HALF_UP).doubleValue();
	}

}
