package org.lq.sm.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 	类型转换工具类
 * @author 杨培林
 * @phone 13603738558
 * 
 * @package org.lq.sm.util
 * @date 2020年8月8日上午10:30:15
 *
 */
public class CastUtil {

	/**
	 * 年月入日
	 */
	public static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";
	public static final String DATE_PATTERN = "yyyyMMdd";
	/**
	 * 	带时分秒的
	 */
	public static final String TIME_DATE_PATTERN="yyyy-MM-dd hh:mm:ss";
	public static final String TIME_DATE_PATTERN2="yyyy-MM-dd-hh-mm-ss";


	/**
	 * 	时间格式化器
	 * @param date
	 * @return
	 */
	public static String format(Date date) {
		return date == null ? "" : format(date, DEFAULT_DATE_PATTERN);
	}


	/**
	 * 	时间格式化器
	 * @param date
	 * @param pattern
	 * @return
	 */
	public static String format(Date date,String pattern) {
		//		new Date()//当前时间
		//		System.currentTimeMillis() 时间错

		return date == null ? "" : new SimpleDateFormat(pattern).format(date);
	}

	/**
	 * 	将字符串转换成java.util时间
	 * @param strDate
	 * @return yyyy-MM-dd
	 * @throws ParseException
	 */
	public static java.util.Date parse(String strDate) throws ParseException{
		return parse(strDate, DEFAULT_DATE_PATTERN);
	}

	/**
	 * 将字符串转换成时间类型
	 * @param strDate
	 * @param pattern
	 * @return
	 * @throws ParseException
	 */
	public static java.util.Date parse(String strDate,String pattern) throws ParseException {
		return strDate == null && "".equals(strDate) ? null : new SimpleDateFormat(pattern).parse(strDate);
	}

	/**
	 * 将字符串转换成时间类型
	 * @param strDate
	 * @param pattern
	 * @return
	 * @throws ParseException
	 */
	public static java.sql.Date parse2(String strDate,String pattern) throws ParseException {
		return strDate == null && "".equals(strDate) ? null : new java.sql.Date(new SimpleDateFormat(pattern).parse(strDate).getTime());
	}
	/**
	 * 	将java.util.date转换成java.sql.date
	 * @param date
	 * @return
	 */
	public static java.sql.Date utilDateToSqlDate(java.util.Date date){
		if(date == null) {
			date = new Date();
		}
		//		java.sql.Date.parse(s)
		return new java.sql.Date(date.getTime());
	}



	/**
	 * 	转成字符串
	 * @param obj
	 * @return
	 */
	public static String castString(Object obj) {
		return castString(obj, "");
	}

	/**
	 * 
	 * @param obj
	 * @param defaultValue
	 * @return
	 */
	public static String castString(Object obj,String defaultValue) {
		return obj != null ? String.valueOf(obj) : defaultValue;
	}

	/**
	 * 
	 * @param obj
	 * @param defaultValue
	 * @return
	 */
	public static double castDouble(Object obj,double defaultValue) {
		double value = defaultValue;
		if(obj!=null) {
			String strVal = castString(obj);
			if(!"".equals(strVal)) {
				try {
					value = Double.parseDouble(strVal);
				} catch (NumberFormatException e) {
					value = defaultValue;
					e.printStackTrace();
				}
			}
		}
		return value;
	}
	/**
	 * 
	 * @param obj
	 * @return
	 */
	public static double castDouble(Object obj) {
		return castDouble(obj,0);
	}

	/**
	 * 
	 * @param obj
	 * @param defaultValue
	 * @return
	 */
	public static int castInt(Object obj,int defaultValue) {
		int value = defaultValue;
		if(obj!=null) {
			String strVal = castString(obj);
			if(!"".equals(strVal)) {
				try {
					value = Integer.valueOf(strVal);
				} catch (NumberFormatException e) {
					value = defaultValue;
					e.printStackTrace();
				}
			}
		}
		return value;
	}
	/**
	 * 
	 * @param obj
	 * @return
	 */
	public static int castInt(Object obj) {
		return castInt(obj, 0);
	}









}
