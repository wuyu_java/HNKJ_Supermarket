package org.lq.sm.util;

import java.sql.ResultSet;

/**
 *  实例化对象
 * @author 杨培林
 * @phone 13603738558
 * 
 * @package org.lq.sm.util
 * @date 2020年8月8日上午11:08:05
 *
 */
public interface Instantiation<T> {
	/**
	 * 	实例化对象
	 * @param rs 数据库返回结果集
	 * @return
	 */
	T instance(ResultSet rs);

}
