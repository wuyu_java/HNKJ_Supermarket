package org.lq.sm.service;

import java.sql.Date;
import java.util.List;

import org.lq.sm.entity.OrderList;
import org.lq.sm.entity.TypeID;

/**
 *  订单表业务接口
 * @author 杨培林
 * @phone 13603738558
 * 
 * @package org.lq.sm.service
 * @date 2020年8月12日下午4:53:06
 *
 */
public interface OrderListService {
	/** 添加
	 * @param t
	 * @return
	 */
	boolean saveOrderList(OrderList d);
	
	/**
	 * 修改
	 * @param t
	 * @return
	 */
	boolean updateOrderListById(OrderList d);
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	boolean deleteOrderListById(int id);
	/**
	 * 查询全部
	 * @return
	 */
	List<OrderList> findOrderListAll();
	/**
	 * 通过ID查询
	 * @param id
	 * @return
	 */
	OrderList getOrderListById(int id);
	/**
	 * 根据typeId查询
	 * @param tid
	 * @return
	 */
	public List<OrderList> getByTypeId(TypeID tid);
	/**
	 * 根据时间查询
	 * @param type 1: 当天 2: 当月 3: 当年
	 * @return
	 */
	public List<OrderList> getByDateName(String type);
	
	

	/**
	 * 根据时间区间查询
	 * @param start
	 * @param end
	 * @return
	 */
	public List<OrderList> getOrderByDate(Date start, Date end);

	
}
