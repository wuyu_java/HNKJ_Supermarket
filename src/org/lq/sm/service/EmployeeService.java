package org.lq.sm.service;

import java.util.List;

import org.lq.sm.entity.Employee;

/**
 * 	员工业务定义接口
 * @author 杨培林
 * @phone 13603738558
 * 
 * @package org.lq.sm.service
 * @date 2020年8月10日下午2:39:58
 *
 */
public interface EmployeeService {
	/**
	 * 	添加
	 * @param t
	 * @return
	 */
	boolean saveEmployee(Employee t);
	/**
	 * 修改
	 * @param t
	 * @return
	 */
	boolean updateEmployee(Employee t);
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	boolean deleteEmployeeById(int id);
	/**
	 * 查询全部
	 * @return
	 */
	List<Employee> findEmployeeAll();
	/**
	 * 通过ID查询
	 * @param id
	 * @return
	 */
	Employee getById(int id);
}
