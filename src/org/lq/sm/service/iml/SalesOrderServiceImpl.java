package org.lq.sm.service.iml;

import java.util.List;
import java.util.Map;

import org.lq.sm.dao.SalesOrderDao;
import org.lq.sm.dao.impl.SalesOrderDaoImpl;
import org.lq.sm.entity.SalesOrder;
import org.lq.sm.entity.SalesOrderStatus;
import org.lq.sm.service.EmployeeService;
import org.lq.sm.service.GoodsService;
import org.lq.sm.service.SalesOrderService;
import org.lq.sm.service.VipService;
/**
 * 挂单业务实现类
 * @author 宋铀
 * @phone	1192030079
 * 
 * @package	org.lq.sm.service.iml
 * @date 2020年8月14日下午12:18:55
 */
public class SalesOrderServiceImpl implements SalesOrderService {
	
	private SalesOrderDao sod = new SalesOrderDaoImpl();
	private GoodsService gs = new GoodsServiceImpl();
	private VipService vs = new VipServiceImpl();
	private EmployeeService es = new EmployeeServiceImpl();
	
	@Override
	public boolean save(SalesOrder t) {
		return sod.save(t) > 0;
	}

	@Override
	public boolean update(SalesOrder t) {
		return sod.update(t) > 0;
	}

	@Override
	public boolean delete(int id) {
		return sod.delete(id) > 0;
	}

	@Override
	public List<SalesOrder> findAll() {
		return sod.findAll();
	}

	@Override
	public SalesOrder getById(int id) {
		return sod.getById(id);
	}

	@Override
	public List<SalesOrder> getMountSaleOrder() {
		List<SalesOrder> list = sod.getSaleOrderByStatus(SalesOrderStatus.挂单.getSatatus());
		for (SalesOrder o : list) {
			o.setVip(vs.getById(o.getVid()));
			o.setGoods(gs.getById(o.getGid()));
			o.setEmp(es.getById(o.getEid()));
		}
		return list;
	}

	@Override
	public List<SalesOrder> getSalesSaleOrder() {
		return sod.getSaleOrderByStatus(SalesOrderStatus.结算.getSatatus());
	}

	@Override
	public Map<String, Integer> getMountSaleOrderMap() {
		return sod.getOrderCount(SalesOrderStatus.挂单.getSatatus());
	}

}
