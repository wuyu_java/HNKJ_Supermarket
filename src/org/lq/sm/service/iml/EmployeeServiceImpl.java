package org.lq.sm.service.iml;

import java.util.List;

import org.lq.sm.dao.EmployeeDao;
import org.lq.sm.dao.impl.EmployeeDaoImpl;
import org.lq.sm.entity.Employee;
import org.lq.sm.service.EmployeeService;
/**
 * 员工业务功能
 * @author 岳鑫
* @phone 159
 *@data 14:51:58
*@package org.lq.sm.service.iml
 */
public class EmployeeServiceImpl implements EmployeeService {
	private EmployeeDao epd = new EmployeeDaoImpl(); 
	@Override
	public boolean saveEmployee(Employee t) {
		return epd.save(t) > 0;
	}

	@Override
	public boolean updateEmployee(Employee t) {
		return epd.update(t) > 0;
	}

	@Override
	public boolean deleteEmployeeById(int id) {
		return epd.delete(id)> 0;
	}

	@Override
	public List<Employee> findEmployeeAll() {
		return epd.findAll();
	}

	@Override
	public Employee getById(int id) {
		return epd.getById(id);
	}

}
