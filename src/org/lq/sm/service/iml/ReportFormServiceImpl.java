package org.lq.sm.service.iml;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.lq.sm.dao.ReportFormDao;
import org.lq.sm.dao.impl.ReportFormDaoImpl;
import org.lq.sm.service.ReportFormService;
import org.lq.sm.util.CastUtil;

public class ReportFormServiceImpl implements ReportFormService {
	private ReportFormDao rfd = new ReportFormDaoImpl();

	@Override
	public DefaultCategoryDataset getBrandCount() {
		Map<String, Integer> map = rfd.getBrandCount();
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		map.forEach((k,v)->{
			dataset.addValue(v, k, k);
		});
		return dataset;
	}

	@Override
	public DefaultPieDataset getGoodsTypeCount() {
		DefaultPieDataset dataset = new DefaultPieDataset();
		Map<String, Integer> map = rfd.getGoodsTypeCount();
		map.forEach((k,v)->{
			dataset.setValue(k, v);
		});
		return dataset;
	}

	@Override
	public DefaultCategoryDataset getInventory() {
		List<Map<String, Object>> list = rfd.getInventory();
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		for (Map<String, Object> map : list) {
			dataset.addValue(CastUtil.castDouble(map.get("inventory")), "库存数量", CastUtil.castString(map.get("name")));
			dataset.addValue(CastUtil.castDouble(map.get("s_count")), "销售数量", CastUtil.castString(map.get("name")));
			dataset.addValue(CastUtil.castDouble(map.get("o_count")), "入库数量", CastUtil.castString(map.get("name")));
		}
		return dataset;
	}
	@Override
	public DefaultPieDataset getTotalPrice() {
		Map<String,String> title = new HashMap<String, String>();
		title.put("goodsintotalprice", "商品入库价钱");
		title.put("saletotalprice", "商品销售总价钱");
		title.put("purchasetotalprice", "商品销售本金价钱");
		DefaultPieDataset dataset = new DefaultPieDataset();
		Map<String, Integer> map = rfd.getTotalPrice();
		map.forEach((k,v)->{
			dataset.setValue(title.get(k), v);
		});
		dataset.setValue("超市利润", CastUtil.castDouble(map.get("saletotalprice"))-CastUtil.castDouble(map.get("purchasetotalprice")));
		return dataset;
	}

}
