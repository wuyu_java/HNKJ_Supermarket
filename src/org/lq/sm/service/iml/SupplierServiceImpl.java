package org.lq.sm.service.iml;

import java.util.List;

import org.lq.sm.dao.SupplierDao;
import org.lq.sm.dao.impl.SupplierDaoImpl;
import org.lq.sm.entity.Supplier;
import org.lq.sm.service.SupplierService;
/**
 * 供应商业务实现类
 * @author 马秋阳
 * @phone 14783803157
 * @date 2020年8月10日下午4:49:39
 * @package org.lq.sm.service.iml
 */
public class SupplierServiceImpl implements SupplierService {
	private SupplierDao sd = new SupplierDaoImpl();
	@Override
	public boolean saveSupplier(Supplier s) {
		return sd.save(s)>0;
	}

	@Override
	public boolean updateSupplier(Supplier s) {
		return sd.update(s)>0;
	}

	@Override
	public boolean deleteSupplierById(int id) {
		return sd.delete(id)>0;
	}

	@Override
	public List<Supplier> findSupplierAll() {
		return sd.findAll();
	}

	@Override
	public Supplier getSupplierById(int id) {
		return sd.getById(id);
	}

	@Override
	public List<Supplier> likeSupplierByName(String name) {
		return sd.likeSupplierByName(name);
	}

}
