package org.lq.sm.service.iml;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.lq.sm.dao.GoodsDao;
import org.lq.sm.dao.impl.GoodsDaoImpl;
import org.lq.sm.entity.Dictionary;
import org.lq.sm.entity.Goods;
import org.lq.sm.service.DictionaryService;
import org.lq.sm.service.GoodsService;
/**
 * 	商品业务层实现
 * @author 冯志远
 * @phone 13253819466
 * 
 * @package org.lq.sm.service.iml
 * @date 2020年8月11日上午9:38:38
 *
 */
public class GoodsServiceImpl implements GoodsService {
	private GoodsDao gd = new GoodsDaoImpl();
	/**
	 * 添加商品
	 */
	@Override
	public boolean saveGoods(Goods t) {
		return gd.save(t) > 0;
	}
	/**
	 * 修改商品
	 */
	@Override
	public boolean updateGoods(Goods t) {
		return gd.update(t) > 0;
	}
	/**
	 * 删除商品
	 */
	@Override
	public boolean deleteGoodsById(int id) {
		return gd.delete(id) > 0;
	}
	/**
	 * 查询全部
	 */
	@Override
	public List<Goods> findAll() {
		return gd.findAll();
	}
	/**
	 * 根据ID查询
	 */
	@Override
	public Goods getById(int id) {
		//将字典对象封装进商品表中
		return gd.getById(id);
	}
	@Override
	public List<Goods> findGoodsByCategoryID(int cid) {
		return gd.findGoodsByCategoryID(cid);
	}
	@Override
	public List<Goods> query(String name) {
		List<Goods> list = new ArrayList<Goods>();
		String codePattern = "\\d{13}";
		String pinYinPattern = "\\w+";
		String chinesePattern = ".*[\\u4e00-\\u9fa5].*";
		if(Pattern.matches(codePattern, name)) {
			//你是条形码查询
			list = gd.getByNumber(name);
			System.out.println("条形码查询");
		}else if(Pattern.matches(pinYinPattern, name)) {
			//拼音查询
			list = gd.getByPinYin(name);
			System.out.println("拼音查询");
		}else if(Pattern.matches(chinesePattern, name)){
			//商品名称查询
			list = gd.getByName(name);
			System.out.println("名称查询");
		}else {
			list = findAll();
		}
		return list;
	}
	@Override
	public Goods getGoodsByIdInDication(int id) {
		Goods g = getById(id);
		DictionaryService ds = new DictionaryServiceImpl();
		if(g.getBrand()!=-1){
			Dictionary brand= ds.getDictionaryById(g.getBrand());
			g.setBrandObj(brand);

		}
		if(g.getCategory()!=-1){
			Dictionary type= ds.getDictionaryById(g.getCategory());
			g.setTypeObj(type);

		}
		if(g.getUnit()!=-1){
			Dictionary unit= ds.getDictionaryById(g.getUnit());
			g.setUnitObj(unit);
		}
		return g;
	}


}














