package org.lq.sm.service.iml;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import org.lq.sm.dao.OrderListDao;
import org.lq.sm.dao.impl.OrderListDaoImpl;
import org.lq.sm.entity.Goods;
import org.lq.sm.entity.OrderList;
import org.lq.sm.entity.TypeID;
import org.lq.sm.service.GoodsService;
import org.lq.sm.service.OrderListService;
import org.lq.sm.util.CastUtil;
/**
 * 进货表业务实现
 * @author 赵红娇
 * org.lq.sm.service.iml
 * @date 2020年8月12日下午6:56:14
 */
public class OrderListServiceImpl implements OrderListService{
	OrderListDao orderListDao=new OrderListDaoImpl();
	GoodsService goodsService = new GoodsServiceImpl();
	@Override
	public boolean saveOrderList(OrderList d) {
		//事务
		boolean bool = orderListDao.save(d)>0;
		if(bool) {
			 Goods g = goodsService.getById(d.getgId());
			 g.setInventory(g.getInventory()+d.getNumber());
			goodsService.updateGoods(g);
		}
		return bool;
	}

	@Override
	public boolean updateOrderListById(OrderList d) {
		return orderListDao.update(d)>0;
	}

	@Override
	public boolean deleteOrderListById(int id) {
		return orderListDao.delete(id)>0;
	}

	@Override
	public List<OrderList> findOrderListAll() {
		return orderListDao.findAll();
	}

	@Override
	public OrderList getOrderListById(int id) {
		return orderListDao.getById(id);
	}

	@Override
	public List<OrderList> getByTypeId(TypeID tid) {
		return orderListDao.getByTypeId(tid.getId());
	}

	@Override
	public List<OrderList> getByDateName(String type) {
		List<OrderList> list = null;
		Date start = null;
		Date end = null;
		switch (type) {
		case "显示所有":
			list = findOrderListAll();
			break;
		case "显示当前":
			Calendar c5 = Calendar.getInstance();
			start = new Date(c5.getTimeInMillis());
			Calendar c4 = Calendar.getInstance();
			c4.add(Calendar.DAY_OF_YEAR, 1);;
			end = new Date(c4.getTimeInMillis());
			list = orderListDao.getOrderByDate(start, end);
			break;
		case "显示当月":
			Calendar c = Calendar.getInstance();
			c.add(Calendar.MONTH, 0);
			c.set(Calendar.DAY_OF_MONTH, 0);
			//获取当前月的第一天
			start = new Date(c.getTimeInMillis());
			Calendar c2 = Calendar.getInstance();
			c2.set(Calendar.DAY_OF_MONTH, c2.getActualMaximum(Calendar.DAY_OF_MONTH));
			end = new Date(c2.getTimeInMillis());
			list = orderListDao.getOrderByDate(start, end);
			break;
		case "显示当年":
			Calendar c6 = Calendar.getInstance();
			int year = c6.get(Calendar.YEAR);
			start = new Date(year-1900,0,0);
			end = new Date(year+1 - 1900, 0, 0);
			list = orderListDao.getOrderByDate(start, end);
			break;
		}
		
		return list;
	}

	@Override
	public List<OrderList> getOrderByDate(Date start, Date end) {
		return orderListDao.getOrderByDate(start, end);
	}



}
