package org.lq.sm.service.iml;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.lq.sm.dao.VipDao;
import org.lq.sm.dao.impl.VipDaoImpl;
import org.lq.sm.entity.Vip;
import org.lq.sm.service.DictionaryService;
import org.lq.sm.service.VipService;

/**
 * 	会员业务层实现
 * @author 郑奥宇
 * @phone 15993025053
 * 
 * @package org.lq.sm.service.iml
 * @date 2020年8月13日上午9:53:40
 * 
 */
public class VipServiceImpl implements VipService{

	private VipDao vd = new VipDaoImpl();
	/**
	 * 	添加
	 */
	@Override
	public boolean saveVip(Vip t) {
		return vd.save(t)>0;
	}
	/**
	 * 修改
	 */
	@Override
	public boolean updateVip(Vip t) {
		return vd.update(t)>0;
	}
	/**
	 * 通过id删除
	 */
	@Override
	public boolean deleteVipById(int id) {
		return vd.delete(id)>0;
	}
	/**
	 * 查询全部
	 */
	@Override
	public List<Vip> findVipAll() {
		DictionaryService ds = new DictionaryServiceImpl();
		List<Vip> list = vd.findAll();
		for (Vip vip : list) {
			vip.setLevelObj(ds.getDictionaryById(vip.getLevel()));
		}
				
		return list;
	}
	/**
	 * 通过id查询
	 */
	@Override
	public Vip getById(int id) {
		return vd.getById(id);
	}
	@Override
	public List<Vip> findVipLike(String value) {
		return vd.findVipLike("%"+value+"%");
	}
	@Override
	public String getByIdRuntenHTML(int id) {
		String dir= System.getProperty("user.dir");
		StringBuilder text = new StringBuilder();
		try {
			BufferedReader  br = new BufferedReader(new FileReader(dir+"/resources/vip.html"));
			String line = "";
			while((line=br.readLine())!=null) {
				text.append(line);
			}
			br.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		Vip v =	getById(id);
		System.out.println(v.getName());
		String content = text.toString();
		content = content.replace("${no}", v.getNo()+"");
		content = content.replace("${name}", v.getName());
		content = content.replace("${card}", v.getCard()+"");
		content = content.replace("${birthday}", v.getBirthday()+"");
		content = content.replace("${phoneNumber}", v.getPhoneNumber()+"");
		content = content.replace("${address}", v.getAddress()+"");
		content = content.replace("${level}", v.getLevel()+"");
		content = content.replace("${integral}", v.getIntegral()+"");
		content = content.replace("${numberSales}", v.getNumberSales()+"");
		content = content.replace("${baseamount}", v.getBaseamount()+"");
		content = content.replace("${desc}", v.getDesc()+"");
		return content;
	}
	@Override
	public List<Vip> findVipByValue(String type,String value) {
		//"所有", "卡号", "用户名", "级别", "拼音"
		List<Vip> list = null;
		switch (type) {
		case "卡号":
			list =vd.findVipByCard(value);
			break;
		case "用户名":
			list = vd.findVipByName(value);
			break;
		case "级别":
			list = vd.findVipByLevel(value);
			break;
		default:
			list = vd.findAll();
			break;
		}
		DictionaryService ds = new DictionaryServiceImpl();
		for (Vip vip : list) {
			vip.setLevelObj(ds.getDictionaryById(vip.getLevel()));
		}
		return list;
	}

}
