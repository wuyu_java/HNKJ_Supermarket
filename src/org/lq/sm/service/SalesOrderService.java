package org.lq.sm.service;

import java.util.List;
import java.util.Map;

import org.lq.sm.entity.SalesOrder;
/**
 * 挂单业务接口
 * @author 宋铀
 * @phone	1192030079
 * 
 * @package	org.lq.sm.service
 * @date 2020年8月14日下午12:19:45
 */
public interface SalesOrderService {
	/**
	 * 	添加挂单
	 * @param t
	 * @return
	 */
	boolean save(SalesOrder t);
	/**
	 * 修改挂单
	 * @param t
	 * @return
	 */
	boolean update(SalesOrder t);
	/**
	 * 删除挂单
	 * @param id
	 * @return
	 */
	boolean delete(int id);
	/**
	 * 查询全部挂单
	 * @return
	 */
	List<SalesOrder> findAll();
	/**
	 * 通过ID查询挂单
	 * @param id
	 * @return
	 */
	SalesOrder getById(int id);
	
	
	/**
	 * 获取挂单
	 * @return
	 */
	public List<SalesOrder> getMountSaleOrder();
	
	/**
	 * 获取销售订单
	 * @return
	 */
	public List<SalesOrder> getSalesSaleOrder();
	
	public Map<String,Integer> getMountSaleOrderMap();
	
	
	
	

}
