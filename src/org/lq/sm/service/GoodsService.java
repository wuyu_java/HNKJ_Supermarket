package org.lq.sm.service;

import java.util.List;

import org.lq.sm.entity.Goods;

/**
 *  	商品操作业务接口
 * @author 杨培林
 * @phone 13603738558
 * 
 * @package org.lq.sm.service
 * @date 2020年8月11日上午9:34:57
 *
 */
public interface GoodsService {
	/**
	 * 	添加
	 * @param t
	 * @return
	 */
	boolean saveGoods(Goods t);
	/**
	 * 修改
	 * @param t
	 * @return
	 */
	boolean updateGoods(Goods t);
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	boolean deleteGoodsById(int id);
	/**
	 * 查询全部
	 * @return
	 */
	List<Goods> findAll();
	/**
	 * 通过ID查询
	 * @param id
	 * @return
	 */
	Goods getById(int id);
	
	/**
	 * 根据产品类别查询
	 * @param cid
	 * @return
	 */
	List<Goods> findGoodsByCategoryID(int cid);
	
	/**
	 * 商品查询操作
	 * @param name
	 * @return
	 */
	List<Goods> query(String name);
	
	Goods getGoodsByIdInDication(int id);
}
