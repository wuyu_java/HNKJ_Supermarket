package org.lq.sm.service;

import java.util.List;

import org.lq.sm.entity.Vip;

/**
 * 	员工业务定义接口
 * @author 杨培林
 * @phone 13603738558
 * 
 * @package org.lq.sm.service
 * @date 2020年8月10日下午2:39:58
 *
 */
public interface VipService {
	/**
	 * 	添加
	 * @param t
	 * @return
	 */
	boolean saveVip(Vip t);
	/**
	 * 修改
	 * @param t
	 * @return
	 */
	boolean updateVip(Vip t);
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	boolean deleteVipById(int id);
	/**
	 * 查询全部
	 * @return
	 */
	List<Vip> findVipAll();
	/**
	 * 通过ID查询
	 * @param id
	 * @return
	 */
	Vip getById(int id);
	/**
	 * 查询对象,拼接html代码
	 * @param id
	 * @return
	 */
	String getByIdRuntenHTML(int id);
	
	public List<Vip> findVipLike(String value);
	
	public List<Vip> findVipByValue(String type,String value);
	
	
}
