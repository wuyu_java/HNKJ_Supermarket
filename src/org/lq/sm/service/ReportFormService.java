package org.lq.sm.service;

import java.util.List;
import java.util.Map;

import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

public interface ReportFormService {
	/**
	 * 获取品牌商品个数统计
	 * @return
	 */
	DefaultCategoryDataset getBrandCount();

	/**
	 * 获取商品类型的个数统计
	 * @return
	 */
	DefaultPieDataset getGoodsTypeCount();
	/**
	 * 获取商品总数量
	 * @return
	 */
	DefaultCategoryDataset getInventory();
	/**
	 * 获取商品总价钱
	 * @return
	 */
	DefaultPieDataset getTotalPrice();
}
