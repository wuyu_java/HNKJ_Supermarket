package org.lq.sm.service;

import java.util.List;
import java.util.Map;

import org.lq.sm.entity.Dictionary;
import org.lq.sm.entity.TypeID;
/**
 *  字典类业务层接口定义
 * @author 杨培林
 * @phone 13603738558
 * 
 * @package org.lq.sm.service
 * @date 2020年8月10日上午10:31:40
 *
 */
public interface DictionaryService {
	
	
	/** 添加
	 * @param t
	 * @return
	 */
	boolean saveDictionary(Dictionary d);
	/**
	 * 修改
	 * @param t
	 * @return
	 */
	boolean updateDictionaryById(Dictionary d);
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	boolean deleteDictionaryById(int id);
	/**
	 * 查询全部
	 * @return
	 */
	List<Dictionary> findDictionaryAll();
	/**
	 * 通过ID查询
	 * @param id
	 * @return
	 */
	Dictionary getDictionaryById(int id);
	
	
	/**
	 * 	根据TypeID查询数据字典集合
	 * @param tid
	 * @return
	 */
	List<Dictionary> findDictionaryByTypeID(TypeID t);

	
	boolean addDictionary(TypeID t,String dic);
	/**
	 * 	根据条形码,返回map集合
	 * @param code
	 * @return
	 */
	Map<String,String> getMapByJson(String code);
	
}
