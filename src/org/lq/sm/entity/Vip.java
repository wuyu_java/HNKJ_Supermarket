package org.lq.sm.entity;

import java.util.Date;

/**
 * 会员类
 * @author 郑奥宇
 * @phone 15993025053
 * 
 * @package org.lq.sm.entity
 * @date 2020年8月8日下午10:32:20
 * 
 */
public class Vip {
	private int no;//序号
	private int card;//卡号
	private String name;//姓名
	private String gender;//性别
	private Date birthday;//生日
	private String phoneNumber;//手机号
	private String address;//地址
	private int level;//级别
	private String discount;//折扣
	private int integral;//积分
	private Date handleCardDate;//办卡日期
	private String baseamount;//消费金额
	private int numberSales;//消费次数
	private String desc;//备注
	
	private Dictionary levelObj;
	
	public Dictionary getLevelObj() {
		return levelObj;
	}
	public void setLevelObj(Dictionary levelObj) {
		this.levelObj = levelObj;
	}
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public int getCard() {
		return card;
	}
	public void setCard(int card) {
		this.card = card;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public String getDiscount(){
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public int getIntegral() {
		return integral;
	}
	public void setIntegral(int integral) {
		this.integral = integral;
	}
	public Date getHandleCardDate(){
		return handleCardDate;
	}
	public void setHandleCardDate(Date handleCardDate) {
		this.handleCardDate = handleCardDate;
	}
	public String getBaseamount() {
		return baseamount;
	}
	public void setBaseamount(String baseamount) {
		this.baseamount = baseamount;
	}
	public int getNumberSales() {
		return numberSales;
	}
	public void setNumberSales(int numberSales) {
		this.numberSales = numberSales;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	//重写toString方法
	@Override
	public String toString() {
		return "Vip [no=" + no + ", card=" + card + ", name=" + name + ", gender=" + gender + ", birthday=" + birthday
				+ ", phoneNumber=" + phoneNumber + ", address=" + address + ", level=" + level + ", discount="
				+ discount + ", integral=" + integral + ", handleCardDate=" + handleCardDate + ", baseamount="
				+ baseamount + ", numberSales=" + numberSales + ", desc=" + desc + "]";
	}
	
}
