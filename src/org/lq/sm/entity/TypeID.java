package org.lq.sm.entity;

/**
 * 	字典类型
 * @author 杨培林
 * @phone 13603738558
 * 
 * @package org.lq.sm.entity
 * @date 2020年8月8日下午2:36:14
 *
 */
public enum TypeID {
	类别(0),品牌(1),单位(2),会员级别(3),默认(-1)
	;
	private int id;
	private TypeID(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
}
