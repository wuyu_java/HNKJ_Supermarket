package org.lq.sm.entity;

/**
 * 
 * @author 六组刘明昕
 * @date 18:01:42
 */
public class Sale {
	private int id;			//序号
	private int sale_id;	//销售单号
	private int card_id;	//会员卡号
	private int integral;	//积分
	private String name;	//销售员姓名
	private int level;	//会员等级
	private int this_integral;	//本次积分
	private int number_count;	//数量合计
	private double original_price_count;	//原价合计
	private double discount;	//优惠金额
	private double current_price_count;		//实收金额
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSale_id() {
		return sale_id;
	}
	public void setSale_id(int sale_id) {
		this.sale_id = sale_id;
	}
	public int getCard_id() {
		return card_id;
	}
	public void setCard_id(int card_id) {
		this.card_id = card_id;
	}
	public int getIntegral() {
		return integral;
	}
	public void setIntegral(int integral) {
		this.integral = integral;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int i) {
		this.level = i;
	}
	public int getThis_integral() {
		return this_integral;
	}
	public void setThis_integral(int this_integral) {
		this.this_integral = this_integral;
	}
	public int getNumber_count() {
		return number_count;
	}
	public void setNumber_count(int number_count) {
		this.number_count = number_count;
	}
	public double getOriginal_price_count() {
		return original_price_count;
	}
	public void setOriginal_price_count(double original_price_count) {
		this.original_price_count = original_price_count;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	public double getCurrent_price_count() {
		return current_price_count;
	}
	public void setCurrent_price_count(double current_price_count) {
		this.current_price_count = current_price_count;
	}
	@Override
	public String toString() {
		return "Sale [id=" + id + ", sale_id=" + sale_id + ", card_id=" + card_id + ", integral=" + integral + ", name="
				+ name + ", level=" + level + ", this_integral=" + this_integral + ", number_count=" + number_count
				+ ", original_price_count=" + original_price_count + ", discount=" + discount + ", current_price_count="
				+ current_price_count + "]";
	}
	
}