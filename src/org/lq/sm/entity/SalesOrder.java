package org.lq.sm.entity;

import java.sql.Timestamp;
import java.util.Date;

/**
 * 挂单实体类
 * @author 宋铀
 * @phone	1192030079
 * 
 * @package	org.lq.sm.entity
 * @date 2020年8月14日上午11:28:09
 */
public class SalesOrder {
	
	private int id;//序号
	private String orderid;//订单编号
	private	int eid;//销售员id
	private int vid;//会员编号
	private int gid;//商品id
	private double price;// 价钱
	private int count;//数量
	private double totalPrice;//总价
	private int integral;//积分
	private int status;//0:代表是挂单,未结算 1 : 代表已经结算
	private Timestamp createdate;
	
	private Vip vip;
	private Goods goods;
	private Employee emp;
	
	
	public Employee getEmp() {
		return emp;
	}
	public void setEmp(Employee emp) {
		this.emp = emp;
	}
	public Vip getVip() {
		return vip;
	}
	public void setVip(Vip vip) {
		this.vip = vip;
	}
	public Goods getGoods() {
		return goods;
	}
	public void setGoods(Goods goods) {
		this.goods = goods;
	}
	
	public Timestamp getCreatedate() {
		return createdate;
	}
	public void setCreatedate(Timestamp createdate) {
		this.createdate = createdate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public int getEid() {
		return eid;
	}
	public void setEid(int eid) {
		this.eid = eid;
	}
	public int getVid() {
		return vid;
	}
	public void setVid(int vid) {
		this.vid = vid;
	}
	public int getGid() {
		return gid;
	}
	public void setGid(int gid) {
		this.gid = gid;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public int getIntegral() {
		return integral;
	}
	public void setIntegral(int integral) {
		this.integral = integral;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public void setStatus(SalesOrderStatus status) {
		this.status = status.getSatatus();
	}
	@Override
	public String toString() {
		return "SalesOrder [id=" + id + ", orderid=" + orderid + ", eid=" + eid + ", vid=" + vid + ", gid=" + gid
				+ ", price=" + price + ", count=" + count + ", totalPrice=" + totalPrice + ", integral=" + integral
				+ ", status=" + status + "]";
	}
	
	
	
	
}
