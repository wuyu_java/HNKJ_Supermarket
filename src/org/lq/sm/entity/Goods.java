package org.lq.sm.entity;
/**
 * 
 * @author 冯志远
 * @phone 13253819466
 * 
 * @package org.lq.entity
 * @date 2020年8月8日下午5:19:43
 *
 */
public class Goods {

	private int id;//序号
	private	String goods_number;//编号
	private	String name;//名称
	private	String code;//拼音简码
	private	int category;//类别
	private	int brand;//品牌
	private	int unit;//单位
	private double purchase_price; //进价
	private double sell_price;//售价
	private double trade_price;//批发价
	private double cost_price;//成本
	private int inventory;//库存
	private int integral;//积分
	private int status;//是否下架
	private int max_inventory;//最大库存
	private int min_inventory;//最小库存
	private int purchase_num;//进货数量
	private int sell_num;//售出数量
	private String icon;
	private String manuName;
	private String manuAddress;
	private String note;
	
	//------------扩展对象-----------------
	private Dictionary brandObj;
	private Dictionary typeObj;
	private Dictionary unitObj;
	
	private int count=1;
	
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	//--------------------------------get/set方法--------------------------------
	public Goods() {
		Dictionary def = new Dictionary();
		def.setId(-1);
		def.setName("--请选择--");
		brandObj = def;
		typeObj = def;
		unitObj = def;
	}
	
	//=========================重写tostring方法===================================
	
	public String getIcon() {
		return icon;
	}
	public Dictionary getBrandObj() {
		return brandObj;
	}
	public void setBrandObj(Dictionary brandObj) {
		this.brandObj = brandObj;
	}
	public Dictionary getTypeObj() {
		return typeObj;
	}
	public void setTypeObj(Dictionary typeObj) {
		this.typeObj = typeObj;
	}
	public Dictionary getUnitObj() {
		return unitObj;
	}
	public void setUnitObj(Dictionary unitObj) {
		this.unitObj = unitObj;
	}
	public String getManuName() {
		return manuName;
	}
	public void setManuName(String manuName) {
		this.manuName = manuName;
	}
	public String getManuAddress() {
		return manuAddress;
	}
	public void setManuAddress(String manuAddress) {
		this.manuAddress = manuAddress;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getGoods_number() {
		return goods_number;
	}
	public void setGoods_number(String goods_number) {
		this.goods_number = goods_number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public int getCategory() {
		return category;
	}
	public void setCategory(int category) {
		this.category = category;
	}
	public int getBrand() {
		return brand;
	}
	public void setBrand(int brand) {
		this.brand = brand;
	}
	public int getUnit() {
		return unit;
	}
	public void setUnit(int unit) {
		this.unit = unit;
	}
	public double getPurchase_price() {
		return purchase_price;
	}
	public void setPurchase_price(double purchase_price) {
		this.purchase_price = purchase_price;
	}
	public double getSell_price() {
		return sell_price;
	}
	public void setSell_price(double sell_price) {
		this.sell_price = sell_price;
	}
	public double getTrade_price() {
		return trade_price;
	}
	public void setTrade_price(double trade_price) {
		this.trade_price = trade_price;
	}
	public double getCost_price() {
		return cost_price;
	}
	public void setCost_price(double cost_price) {
		this.cost_price = cost_price;
	}
	public int getInventory() {
		return inventory;
	}
	public void setInventory(int inventory) {
		this.inventory = inventory;
	}
	public int getIntegral() {
		return integral;
	}
	public void setIntegral(int integral) {
		this.integral = integral;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getMax_inventory() {
		return max_inventory;
	}
	public void setMax_inventory(int max_inventory) {
		this.max_inventory = max_inventory;
	}
	public int getMin_inventory() {
		return min_inventory;
	}
	public void setMin_inventory(int min_inventory) {
		this.min_inventory = min_inventory;
	}
	public int getPurchase_num() {
		return purchase_num;
	}
	public void setPurchase_num(int purchase_num) {
		this.purchase_num = purchase_num;
	}
	public int getSell_num() {
		return sell_num;
	}
	public void setSell_num(int sell_num) {
		this.sell_num = sell_num;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	@Override
	public String toString() {
		return "Goods [id=" + id + ", goods_number=" + goods_number + ", name=" + name + ", code=" + code
				+ ", category=" + category + ", brand=" + brand + ", unit=" + unit + ", purchase_price="
				+ purchase_price + ", sell_price=" + sell_price + ", trade_price=" + trade_price + ", cost_price="
				+ cost_price + ", inventory=" + inventory + ", integral=" + integral + ", status=" + status
				+ ", max_inventory=" + max_inventory + ", min_inventory=" + min_inventory + ", purchase_num="
				+ purchase_num + ", sell_num=" + sell_num + "]";
	}
	
	
}
