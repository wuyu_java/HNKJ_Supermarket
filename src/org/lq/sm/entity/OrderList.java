package org.lq.sm.entity;
/**
 * 
 * @author 秦洪涛
 *2020年8月8日下午5:20:29
 */

import java.io.Serializable;
import java.sql.Timestamp;

public class OrderList implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;//序号
	private int orderNo;//单号
	private int gId;//商品编号
	private String orderName;//名称
	private String type;//类别
	private String brand;//品牌
	private int number;//数量
	private Double pursePrice;//进价
	private Double totalPrice;//进价总金额
	private Double sellPrice;//售价
	private Double tradePrice;//批发价
	private Timestamp date;//日期
	private int supplier;//供货商
	private Double onsale;//特价
	private String remark;//备注
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(int orderNo) {
		this.orderNo = orderNo;
	}
	
	public String getOrderName() {
		return orderName;
	}
	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public Double getPursePrice() {
		return pursePrice;
	}
	public void setPursePrice(Double pursePrice) {
		this.pursePrice = pursePrice;
	}
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public Double getSellPrice() {
		return sellPrice;
	}
	public void setSellPrice(Double sellPrice) {
		this.sellPrice = sellPrice;
	}
	public Double getTradePrice() {
		return tradePrice;
	}
	public void setTradePrice(Double tradePrice) {
		this.tradePrice = tradePrice;
	}
	public Timestamp getDate() {
		return date;
	}
	public void setDate(Timestamp date) {
		this.date = date;
	}
	public Double getOnsale() {
		return onsale;
	}
	public void setOnsale(Double onsale) {
		this.onsale = onsale;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public int getgId() {
		return gId;
	}
	public void setgId(int gId) {
		this.gId = gId;
	}
	public int getSupplier() {
		return supplier;
	}
	public void setSupplier(int supplier) {
		this.supplier = supplier;
	}
	@Override
	public String toString() {
		return "OrderList [id=" + id + ", orderNo=" + orderNo + ", gId=" + gId + ", orderName=" + orderName + ", type="
				+ type + ", brand=" + brand + ", number=" + number + ", pursePrice=" + pursePrice + ", totalPrice="
				+ totalPrice + ", sellPrice=" + sellPrice + ", tradePrice=" + tradePrice + ", date=" + date
				+ ", supplier=" + supplier + ", onsale=" + onsale + ", remark=" + remark + "]";
	}
	
	
	
}
