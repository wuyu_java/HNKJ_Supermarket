package org.lq.sm.entity;

/**
 * 	销售订单状态
 * @author 杨培林
 * @phone 13603738558
 * 
 * @package org.lq.sm.entity
 * @date 2020年8月14日下午1:47:07
 *
 */
public enum SalesOrderStatus {

	挂单(0),结算(1)
	;
	private int satatus;
	SalesOrderStatus(int status) {
		this.satatus = status;
	}
	
	public int getSatatus() {
		return satatus;
	}
}
