package org.lq.sm.entity;
/**
 * @author 骆杨
 */


public class Dictionary {
	private int id;//序号
	private String name;//名称
	private int typeid;//类型编号
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getTypeid() {
		return typeid;
	}
	public void setTypeid(int typeid) {
		this.typeid = typeid;
	}
	public void setTypeid(TypeID type) {
		this.typeid = type.getId();
	}
	@Override
	public String toString() {
		return name;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj==null) {
			return false;
		}else if(!(obj instanceof Dictionary)) {
			return false;
		}
		
		Dictionary d = (Dictionary) obj;
		
		return this.name.equals(d.getName());
	}

}









