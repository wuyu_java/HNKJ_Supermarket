package org.lq.sm.dao;

import java.util.List;
import java.util.Map;

import org.lq.sm.entity.SalesOrder;

/**
 * 挂单管理数据访问接口
 * @author 宋铀
 * @phone	1192030079
 * 
 * @package	org.lq.sm.dao
 * @date 2020年8月14日上午11:35:35
 */
public interface SalesOrderDao extends BaseDao<SalesOrder> {

	
	public List<SalesOrder> getSaleOrderByStatus(int status);
	
	/**
	 * 获取订单数量
	 * @return
	 */
	public Map<String,Integer> getOrderCount(int status);
	
}
