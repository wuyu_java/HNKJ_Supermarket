package org.lq.sm.dao;

import java.util.List;

import org.lq.sm.entity.Vip;

public interface VipDao extends BaseDao<Vip>{

	/**
	 * 修改积分
	 * @param id
	 * @return
	 */
	int updateScore(int id);
	
	List<Vip> findVipLike(String value);
	List<Vip> findVipByName(String value);
	List<Vip> findVipByCard(String value);
	List<Vip> findVipByLevel(String value);
}
