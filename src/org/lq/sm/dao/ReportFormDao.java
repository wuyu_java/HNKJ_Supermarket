package org.lq.sm.dao;

import java.util.List;
import java.util.Map;

/**
 * 报表数据统计
 * @author 杨培林
 * @phone 13603738558
 * 
 * @package org.lq.sm.dao
 * @date 2020年8月15日上午10:21:47
 *
 */
public interface ReportFormDao {
	
	/**
	 * 获取品牌商品个数统计
	 * @return
	 */
	Map<String,Integer> getBrandCount();

	/**
	 * 获取商品类型的个数统计
	 * @return
	 */
	Map<String,Integer> getGoodsTypeCount();
	/**
	 * 获取商品总数量
	 * @return
	 */
	List<Map<String,Object>> getInventory();
	/**
	 * 获取商品总价钱
	 * @return
	 */
	Map<String,Integer> getTotalPrice();
	
}













