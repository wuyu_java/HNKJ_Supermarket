package org.lq.sm.dao;

import org.lq.sm.entity.Employee;
/**
 * 
 * @author 岳鑫
* @phone 159
 *@data 5:14:45
*@package org.lq.sm.dao
*
*超市员工的操作功能
 */

public interface EmployeeDao extends BaseDao<Employee> {

	
	
}
