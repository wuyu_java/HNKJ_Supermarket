package org.lq.sm.dao;

import java.util.List;
/**
 * 	通用泛型接口
 * @author 杨培林
 * @phone 13603738558
 * 
 * @package org.lq.sm.dao
 * @date 2020年8月8日下午5:53:26
 * 
 * @param <T>
 */
public interface BaseDao<T> {
	/**
	 * 	添加
	 * @param t
	 * @return
	 */
	int save(T t);
	/**
	 * 修改
	 * @param t
	 * @return
	 */
	int update(T t);
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	int delete(int id);
	/**
	 * 查询全部
	 * @return
	 */
	List<T> findAll();
	/**
	 * 通过ID查询
	 * @param id
	 * @return
	 */
	T getById(int id);
	

}
