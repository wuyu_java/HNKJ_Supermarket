package org.lq.sm.dao;

import java.util.List;

import org.lq.sm.entity.Goods;

/**
 * 商品类接口
 * @author 3组 李淑帆
 *
 */
public interface GoodsDao extends BaseDao<Goods> {

	/**
	 * 根据商品名称模糊查询
	 * @param name
	 * @return
	 */
	List<Goods> getByName(String name);

	List<Goods> getByPinYin(String name);
	List<Goods> getByNumber(String name);

	List<Goods> findGoodsByCategoryID(int cid);
}
