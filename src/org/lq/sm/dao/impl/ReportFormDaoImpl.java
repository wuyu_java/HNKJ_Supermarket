package org.lq.sm.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.lq.sm.dao.BaseDao;
import org.lq.sm.dao.ReportFormDao;
import org.lq.sm.util.JDBCUtil;
/**
 * 报表统计
 * @author 杨培林
 * @phone 13603738558
 * 
 * @package org.lq.sm.dao.impl
 * @date 2020年8月15日上午10:25:45
 *
 */
public class ReportFormDaoImpl implements ReportFormDao {
	private PreparedStatement ps;
	private Connection conn;
	private ResultSet rs;

	@Override
	public Map<String, Integer> getBrandCount() {
		Map<String, Integer> map = new HashMap<String, Integer>();
		conn = JDBCUtil.getConn();
		try {
			ps = conn.prepareStatement("select * from v_brandcount");
			rs = ps.executeQuery();
			while(rs.next()) {
				map.put(rs.getString("name"), rs.getInt("num"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.closeAll(rs, ps, conn);
		}
		
		return map;
	}

	@Override
	public Map<String, Integer> getGoodsTypeCount() {
		Map<String, Integer> map = new HashMap<String, Integer>();
		conn = JDBCUtil.getConn();
		try {
			ps = conn.prepareStatement("select * from v_goodstypecount");
			rs = ps.executeQuery();
			while(rs.next()) {
				map.put(rs.getString("name"), rs.getInt("num"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.closeAll(rs, ps, conn);
		}
		
		return map;
	}

	@Override
	public List<Map<String, Object>> getInventory() {
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		Map<String,Object> map = null;
		conn = JDBCUtil.getConn();
		try {
			ps = conn.prepareStatement("select * from v_inventory");
			rs = ps.executeQuery();
			while(rs.next()) {
				map = new HashMap<String, Object>();
				map.put("id",rs.getInt("id"));
				map.put("name",rs.getString("name"));
				map.put("inventory",rs.getInt("inventory"));
				map.put("s_count",rs.getInt("s_count"));
				map.put("o_count", rs.getInt("o_count"));
				
				list.add(map);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.closeAll(rs, ps, conn);
		}
		return list;
	}

	@Override
	public Map<String, Integer> getTotalPrice() {
		Map<String, Integer> map = new HashMap<String, Integer>();
		conn = JDBCUtil.getConn();
		try {
			ps = conn.prepareStatement("select * from v_totalprice");
			rs = ps.executeQuery();
			while(rs.next()) {
				map.put("goodsintotalprice",rs.getInt("goodsintotalprice"));
				map.put("saletotalprice",rs.getInt("saletotalprice"));
				map.put("purchasetotalprice",rs.getInt("purchasetotalprice"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.closeAll(rs, ps, conn);
		}
		return map;
	}
	
	
}
