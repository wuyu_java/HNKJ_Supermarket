package org.lq.sm.dao.impl;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.lq.sm.dao.OrderListDao;
import org.lq.sm.entity.OrderList;
import org.lq.sm.util.Instantiation;
import org.lq.sm.util.JDBCUtil;
/**
 * 进货订单表
 * @author 秦洪涛
 *2020年8月10日上午8:46:36
 */
public class OrderListDaoImpl implements OrderListDao,Instantiation<OrderList> {

	@Override
	public int save(OrderList t) {
		int num=0;
		String sql="insert into orderlist (orderNo,"
				+ "gid,"
				+ "orderName,"
				+ "type,"
				+ "brand,"
				+ "number,"
				+ "pursePrice,"
				+ "totalPrice,"
				+ "sellPrice,"
				+ "tradePrice,"
				+ "date,"
				+ "supplier,"
				+ "onsale,"
				+ "remark) "
				+ "values(?,?,?,?,?,?,?,?,?,?,now(),?,?,?)";
		try {
			num=JDBCUtil.executeUpdate(
					sql,
					t.getOrderNo(),
					t.getgId(),
					t.getOrderName(),
					t.getType(),
					t.getBrand(),
					t.getNumber(),
					t.getPursePrice(),
					t.getTotalPrice(),
					t.getSellPrice(),
					t.getTradePrice(),
//					t.getDate(),
					t.getSupplier(),
					t.getOnsale(),
					t.getRemark()
					);
		} catch (SQLException e) {
			e.printStackTrace();
		}
				return num;
	}

	@Override
	public int delete(int id) {
		int num=0;
		try {
			num = JDBCUtil.executeUpdate("delete from orderlist where id=?", id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	@Override
	public int update(OrderList t) {
		int num=0;
		String sql="update orderlist set orderNo=?,"
				+ "gid=?,"
				+ "orderName=?,"
				+ "type=?,"
				+ "brand=?,"
				+ "number=?,"
				+ "pursePrice=?,"
				+ "totalPrice=?,"
				+ "sellPrice=?,"
				+ "tradePrice=?,"
//				+ "date=?,"
				+ "supplier=?,"
				+ "onsale=?,"
				+ "remark=?  where id=?"
				;
		try {
			num=JDBCUtil.executeUpdate(
					sql,
					t.getOrderNo(),
					t.getgId(),
					t.getOrderName(),
					t.getType(),
					t.getBrand(),
					t.getNumber(),
					t.getPursePrice(),
					t.getTotalPrice(),
					t.getSellPrice(),
					t.getTradePrice(),
//					t.getDate(),
					t.getSupplier(),
					t.getOnsale(),
					t.getRemark(),
					t.getId()
					);
		} catch (SQLException e) {
			e.printStackTrace();
		}
				return num;
	}

	@Override
	public List<OrderList> findAll() {
		List<OrderList> list=new ArrayList<>();
		String sql="select * from orderlist";
		try {
			list= JDBCUtil.executeQuery(sql, this);
			for (OrderList orderList : list) {
				System.out.println(orderList);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return list;
	}

	@Override
	public OrderList getById(int id) {
		List<OrderList> list=new ArrayList<>();
		String sql="select * from orderlist where id=?";
		try {
			list= JDBCUtil.executeQuery(sql, this,id);
			for (OrderList orderList : list) {
				System.out.println(orderList);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null ;
		
//		return list;
	}

	@Override
	public OrderList instance(ResultSet rs) {
		OrderList ol=null;
		try {
			ol = new OrderList();
			ol.setBrand(rs.getString("brand"));
			ol.setDate(rs.getTimestamp("date"));
			ol.setId(rs.getInt("id"));
			ol.setNumber(rs.getInt("number"));
			ol.setOnsale(rs.getDouble("onsale"));
			ol.setgId(rs.getInt("gId"));
			ol.setOrderName(rs.getString("orderName"));
			ol.setOrderNo(rs.getInt("orderNo"));
			ol.setPursePrice(rs.getDouble("pursePrice"));
			ol.setRemark(rs.getString("remark"));
			ol.setSellPrice(rs.getDouble("sellPrice"));
			ol.setSupplier(rs.getInt("supplier"));
			ol.setTotalPrice(rs.getDouble("totalPrice"));
			ol.setTradePrice(rs.getDouble("tradePrice"));
			ol.setType(rs.getString("type"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return ol;
	}

	@Override
	public List<OrderList> getByTypeId(int tid) {
		return null;
	}

	@Override
	public List<OrderList> getOrderByDate(Date start, Date end) {
		
		try {
			return JDBCUtil.executeQuery("select * FROM orderlist where date >= ? AND date <= ?", this, start,end);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	} 

}
