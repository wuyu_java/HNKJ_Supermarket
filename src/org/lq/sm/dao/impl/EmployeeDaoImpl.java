package org.lq.sm.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.lq.sm.dao.EmployeeDao;
import org.lq.sm.entity.Employee;
import org.lq.sm.util.Instantiation;
import org.lq.sm.util.JDBCUtil;
/**
 * 
 *@author  黄兴栋
 *@time  2020年8月9日
 *@package  org.lq.sm.dao.impl
 *
 */
public class EmployeeDaoImpl implements EmployeeDao,Instantiation<Employee> {

	/**
	 * 添加
	 */
	@Override
	public int save(Employee t) {
		int num = 0;
		String sql = "insert into employee(name,phone,salary,commratio,time,authority,remarks,icon) values(?,?,?,?,?,?,?,?)";
		try {
			num = JDBCUtil.executeUpdate(sql, t.getName(),t.getPhone(),
					t.getSalary(),t.getCommratio(),t.getTime(),t.getAuthority(),t.getRemarks(),t.getIcon());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}
	
	/**
	 * 修改
	 */
	@Override
	public int update(Employee t) {
		int num = 0;
		String sql = "update employee set name=?,phone=?,salary=?,commratio=?,time=?,authority=?,remarks=?,icon = ? where empid =?";
		try {
			num = JDBCUtil.executeUpdate(sql, t.getName(),t.getPhone(),
					t.getSalary(),t.getCommratio(),t.getTime(),t.getAuthority(),t.getRemarks(),t.getIcon(),t.getEmpid());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	/**
	 * 删除
	 */
	@Override
	public int delete(int id) {
		int num = 0;
		String sql = "delete from employee where empid = ?";
		try {
			num = JDBCUtil.executeUpdate(sql,id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	/**
	 * 查询所有
	 */
	@Override
	public List<Employee> findAll() {
		List<Employee> list = new ArrayList<>();
		String sql = "select * from employee";
		try {
			list = JDBCUtil.executeQuery(sql,this);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * 根据id查询
	 */
	@Override
	public Employee getById(int id) {
		List<Employee> list = new ArrayList<>();
		String sql = "select * from employee where empid = ?";
		try {
			list = JDBCUtil.executeQuery(sql, this,id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Employee emp = null;
		if(list.size()>0) {
			emp = list.get(0);
		}
		return emp;
	}

	@Override
	public Employee instance(ResultSet rs) {
		Employee emp = new Employee();
		try {
			emp.setEmpid(rs.getInt("empid"));
			emp.setName(rs.getString("name"));
			emp.setPhone(rs.getString("phone"));
			emp.setTime(rs.getDate("time"));
			emp.setAuthority(rs.getString("authority"));
			emp.setCommratio(rs.getString("commratio"));
			emp.setSalary(rs.getInt("salary"));
			emp.setRemarks(rs.getString("remarks"));
			emp.setIcon(rs.getString("icon"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return emp;
	}

}
