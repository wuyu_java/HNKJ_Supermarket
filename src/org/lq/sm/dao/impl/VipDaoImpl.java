package org.lq.sm.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

import org.lq.sm.dao.VipDao;
import org.lq.sm.entity.Vip;
import org.lq.sm.util.Instantiation;
import org.lq.sm.util.JDBCUtil;


/**
 * 
 * 实现VipDao接口
 * @author 郑奥宇
 * @phone 15993025053
 * 
 * @package org.lq.sm.dao.impl
 * @date 2020年8月9日下午5:27:59
 * 
 */
public class VipDaoImpl implements VipDao,Instantiation<Vip> {

	/*
	 * 添加会员信息
	 */
	@Override
	public int save(Vip t) {
		int num = 0;
		String sql ="insert into vip values (null,?,?,?,now(),?,?,?,?,?,now(),?,?,?)";
		try {
			num = JDBCUtil.executeUpdate(sql,t.getCard(),t.getName(),t.getGender(),t.getPhoneNumber(),t.getAddress(),t.getLevel(),t.getDiscount(),t.getIntegral(),t.getBaseamount(),t.getNumberSales(),t.getDesc());
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return num;
	}
	/*
	 * 修改会员信息
	 */
	@Override
	public int update(Vip t) {
		String sql = "update vip set card=?,name=?,gender=?,birthday=?,phonenumber=?,address=?,level=?,discount=?,integral=?,handlecarddate=?,baseamount=?,numbersales=?,description=? where no = ?";
		int num = 0;
		try {
			num = JDBCUtil.executeUpdate(sql, t.getCard(),t.getName(),t.getGender(),t.getBirthday(),t.getPhoneNumber(),t.getAddress(),t.getLevel(),t.getDiscount(),t.getIntegral(),t.getHandleCardDate(),t.getBaseamount(),t.getNumberSales(),t.getDesc(),t.getNo());
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return num;
	}
	/*
	 * 删除会员信息
	 */
	@Override
	public int delete(int id) {
		String sql = "delete from vip where no =?";
		int num = 0 ;
		try {
			num = JDBCUtil.executeUpdate(sql, id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}
	/*
	 * 查询所有会员信息
	 */
	@Override
	public List<Vip> findAll() {
		String sql = "select *from vip";
		List<Vip> list = null;
		try {
			list = JDBCUtil.executeQuery(sql, new Instantiation<Vip>() {
				public Vip instance(ResultSet rs) {
					Vip v = new Vip();
					try {
						v.setNo(rs.getInt("no"));
						v.setCard(rs.getInt("card"));
						v.setName(rs.getString("name"));
						v.setGender(rs.getString("gender"));
						v.setBirthday(rs.getDate("birthday"));
						v.setPhoneNumber(rs.getString("phonenumber"));
						v.setLevel(rs.getInt("level"));
						v.setAddress(rs.getString("address"));
						v.setDiscount(rs.getString("discount"));
						v.setIntegral(rs.getInt("integral"));
						v.setHandleCardDate(rs.getDate("handlecarddate"));
						v.setBaseamount(rs.getString("baseamount"));
						v.setNumberSales(rs.getInt("numbersales"));
						v.setDesc(rs.getString("description"));
					} catch (SQLException e) {
						e.printStackTrace();
					}
					return v;
				}
			});
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	/*
	 * 通过 序号查询会员信息
	 */
	@Override
	public Vip getById(int no) {
		String sql = "select *from vip where no = ?";
		List<Vip> list = null;
		try {
			list = JDBCUtil.executeQuery(sql, new Instantiation<Vip>() {
				public Vip instance(ResultSet rs) {
					Vip v = new Vip();
					try {
						v.setNo(rs.getInt("no"));
						v.setCard(rs.getInt("card"));
						v.setName(rs.getString("name"));
						v.setGender(rs.getString("gender"));
						v.setBirthday(rs.getDate("birthday"));
						v.setPhoneNumber(rs.getString("phonenumber"));
						v.setLevel(rs.getInt("level"));
						v.setAddress(rs.getString("address"));
						v.setDiscount(rs.getString("discount"));
						v.setIntegral(rs.getInt("integral"));
						v.setHandleCardDate(rs.getDate("handlecarddate"));
						v.setBaseamount(rs.getString("baseamount"));
						v.setNumberSales(rs.getInt("numbersales"));
						v.setDesc(rs.getString("description"));

					} catch (SQLException e) {
						e.printStackTrace();
					}
					return v;
				}
			},no);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		//判断 如果有值返回对象,否则返回空
		if (list.size()>0) {
			return list.get(0);
		}
		return null;
	}
	/*
	 * 通过序号修改会员积分
	 */
	@Override
	public int updateScore(int no) {
		String sql = "update vip set integral = ? where no =?";	
		Scanner in = new Scanner(System.in);
		int score = Integer.parseInt(in.next());
		int num = 0;
		try {
			num = JDBCUtil.executeUpdate(sql,score,no);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		in.close();
		return num;
	}
	@Override
	public List<Vip> findVipLike(String value) {
		String sql = "select * from vip where card like ? or name like ? or phonenumber like ?";
		try {
			return JDBCUtil.executeQuery(sql, new Instantiation<Vip>() {
					public Vip instance(ResultSet rs) {
						Vip v = new Vip();
						try {
							v.setNo(rs.getInt("no"));
							v.setCard(rs.getInt("card"));
							v.setName(rs.getString("name"));
							v.setGender(rs.getString("gender"));
							v.setBirthday(rs.getDate("birthday"));
							v.setPhoneNumber(rs.getString("phonenumber"));
							v.setLevel(rs.getInt("level"));
							v.setAddress(rs.getString("address"));
							v.setDiscount(rs.getString("discount"));
							v.setIntegral(rs.getInt("integral"));
							v.setHandleCardDate(rs.getDate("handlecarddate"));
							v.setBaseamount(rs.getString("baseamount"));
							v.setNumberSales(rs.getInt("numbersales"));
							v.setDesc(rs.getString("description"));

						} catch (SQLException e) {
							e.printStackTrace();
						}
						return v;
					}
				},value,value,value);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public List<Vip> findVipByName(String value) {
		try {
			return JDBCUtil.executeQuery("select * from vip where name like ?", this, "%"+value+"%");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public List<Vip> findVipByCard(String value) {
		try {
			return JDBCUtil.executeQuery("select * from vip where card like ?", this, "%"+value+"%");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public List<Vip> findVipByLevel(String value) {
		try {
			return JDBCUtil.executeQuery("select * from vip where `level` in (select id from dictionary where name LIKE ?)", this, "%"+value+"%");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	@Override
	public Vip instance(ResultSet rs) {
		Vip v = new Vip();
		try {
			v.setNo(rs.getInt("no"));
			v.setCard(rs.getInt("card"));
			v.setName(rs.getString("name"));
			v.setGender(rs.getString("gender"));
			v.setBirthday(rs.getDate("birthday"));
			v.setPhoneNumber(rs.getString("phonenumber"));
			v.setLevel(rs.getInt("level"));
			v.setAddress(rs.getString("address"));
			v.setDiscount(rs.getString("discount"));
			v.setIntegral(rs.getInt("integral"));
			v.setHandleCardDate(rs.getDate("handlecarddate"));
			v.setBaseamount(rs.getString("baseamount"));
			v.setNumberSales(rs.getInt("numbersales"));
			v.setDesc(rs.getString("description"));

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return v;
	}

}
