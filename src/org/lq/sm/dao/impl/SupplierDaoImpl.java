package org.lq.sm.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.lq.sm.dao.SupplierDao;
import org.lq.sm.entity.Supplier;
import org.lq.sm.util.Instantiation;
import org.lq.sm.util.JDBCUtil;
/**
 * 供应商接口实现类
 * @author 马秋阳
 * @phone 14783803157
 * @date 2020年8月8日下午11:19:11
 * @package org.lq.sm.dao.impl
 */
public class SupplierDaoImpl implements SupplierDao,Instantiation<Supplier> {

	@Override
	/**
	 * 添加
	 */
	public int save(Supplier t) {
		int num = 0;
		String sql = "insert into supplier values(null,?,?,?,?,?,?,?,?)";
		try {
			num = JDBCUtil.executeUpdate(sql, t.getAbbre(),t.getName(),t.getAddress(),t.getContacts(),t.getTnumber(),
					t.getCnumber(),t.getACnumber(),t.getRemarks());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	@Override
	/**
	 * 修改
	 */
	public int update(Supplier t) {
		int num = 0;
		String sql = "update supplier set abbre=? , name=? , address=? , contacts=? , Tnumber=? , Cnumber=? , "
				+ "ACnumber=? , remarks=? where id = ?";
		try {
			num = JDBCUtil.executeUpdate(sql, t.getAbbre(),t.getName(),t.getAddress(),t.getContacts(),t.getTnumber(),
						t.getCnumber(),t.getACnumber(),t.getRemarks(),t.getId());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	@Override
	/**
	 * 删除
	 */
	public int delete(int id) {
		int num = 0;
		String sql = "delete from supplier where id = ?";
		try {
			num = JDBCUtil.executeUpdate(sql, id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}

	@Override
	/**
	 * 查找全部
	 */
	public List<Supplier> findAll() {
		String sql = "select * from supplier";
		List<Supplier> list = null;
		try {
			list = JDBCUtil.executeQuery(sql, this);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	/**
	 * 按照Id查找
	 */
	public Supplier getById(int id) {
		String sql = "select * from supplier where id = ?";
		List<Supplier> list = null;
		try {
			 list = JDBCUtil.executeQuery(sql, this,id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list.get(0);
	}

	@Override
	public List<Supplier> likeSupplierByName(String name) {
		try {
			return JDBCUtil.executeQuery("select * from supplier where name like ? or abbre like ? ", this, "%"+name+"%","%"+name+"%");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Supplier instance(ResultSet rs) {
		Supplier supplier = new Supplier();
		try {
			supplier.setId(rs.getInt("id"));
			supplier.setAbbre(rs.getNString("abbre"));
			supplier.setName(rs.getNString("name"));
			supplier.setAddress(rs.getNString("address"));
			supplier.setContacts(rs.getNString("contacts"));
			supplier.setTnumber(rs.getNString("Tnumber"));
			supplier.setCnumber(rs.getNString("Cnumber"));
			supplier.setACnumber(rs.getNString("ACnumber"));
			supplier.setRemarks(rs.getNString("remarks"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return supplier;
	}
}
