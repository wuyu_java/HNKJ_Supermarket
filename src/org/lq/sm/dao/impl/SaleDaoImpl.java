package org.lq.sm.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.lq.sm.dao.SaleDao;
import org.lq.sm.entity.Sale;
import org.lq.sm.util.Instantiation;
import org.lq.sm.util.JDBCUtil;
/**
 * 
 * @author 6组邹倩倩
 *
 */
public class SaleDaoImpl implements SaleDao {
	
	/**
	 * 添加销售记录
	 */
	@Override
	public int save(Sale t) {
		int num = 0;//声明变量标识是否添加成功
		String sql = "insert into sale values(null,?,?,?,?,?,?,?,?,?,?)";//要使用的sql语句
		try {
			//调用JDBCUtil的增删改方法
			num = JDBCUtil.executeUpdate(sql, t.getSale_id(),t.getCard_id(),t.getIntegral(),t.getName(),t.getLevel(),t.getThis_integral(),t.getNumber_count(),t.getOriginal_price_count(),t.getDiscount(),t.getCurrent_price_count());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num;
	}
	/**
	 * 修改销售记录
	 */
	@Override
	public int update(Sale t) {
		int num = 0;//声明变量标识是否添加成功
		//要使用的sql语句
		String sql = "update sale set integral=?,level=?,this_integral=?,number_count=?,original_price_count=?,discount=?,current_price_count=?  where id=?";
		try {
			//调用JDBCUtil的增删改方法
			num = JDBCUtil.executeUpdate(sql,t.getIntegral(),t.getLevel(),t.getThis_integral(),t.getNumber_count(),t.getOriginal_price_count(),t.getDiscount(),t.getCurrent_price_count(),t.getId());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return num;
	}
	/**
	 * 删除销售记录
	 */
	@Override
	public int delete(int id) {
		int num = 0;//声明变量标识是否添加成功
		//要使用的sql语句
		String sql = "delete from sale where id=?";
		try {
			//调用JDBCUtil的增删改方法
			num = JDBCUtil.executeUpdate(sql,id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return num;
	}
	/**
	 * 查询所有销售记录
	 */
	@Override
	public List<Sale> findAll() {
		//声明要返回的集合
		List<Sale> list = null;
		//要使用的sql语句
		String sql = "select * from sale";
		try {
			//调用JDBCUtil的查询方法
			list = JDBCUtil.executeQuery(sql, new Instantiation<Sale>() {
				public Sale instance(ResultSet rs) {
					Sale s = new Sale();
					try {
						s.setId(rs.getInt("id"));
						s.setCard_id(rs.getInt("card_id"));
						s.setSale_id(rs.getInt("sale_id"));
						s.setName(rs.getString("name"));
						s.setIntegral(rs.getInt("integral"));
						s.setLevel(rs.getInt("level"));
						s.setThis_integral(rs.getInt("this_integral"));
						s.setNumber_count(rs.getInt("number_count"));
						s.setOriginal_price_count(rs.getDouble("original_price_count"));
						s.setDiscount(rs.getDouble("discount"));
						s.setCurrent_price_count(rs.getDouble("current_price_count"));
					} catch (SQLException e) {
						e.printStackTrace();
					}
					return s;
				};
			});
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	/**
	 * 根据id查询销售记录
	 */
	@Override
	public Sale getById(int id) {
		//声明集合
		List<Sale> list = null;
		//声明要返回的Sale
		Sale sale = null;
		//要使用的sql语句
		String sql = "select * from sale where id = ?";
		try {
			//调用JDBCUtil查询方法，返回一个集合
			list = JDBCUtil.executeQuery(sql, new Instantiation<Sale>() {
				public Sale instance(ResultSet rs) {
					Sale s = new Sale();
					try {
						s.setId(rs.getInt("id"));
						s.setCard_id(rs.getInt("card_id"));
						s.setSale_id(rs.getInt("sale_id"));
						s.setName(rs.getString("name"));
						s.setIntegral(rs.getInt("integral"));
						s.setLevel(rs.getInt("level"));
						s.setThis_integral(rs.getInt("this_integral"));
						s.setNumber_count(rs.getInt("number_count"));
						s.setOriginal_price_count(rs.getDouble("original_price_count"));
						s.setDiscount(rs.getDouble("discount"));
						s.setCurrent_price_count(rs.getDouble("current_price_count"));
					} catch (SQLException e) {
						e.printStackTrace();
					}
					return s;
				};
			},id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//将集合中的第一个Sale赋值给要返回的Sale
		sale = list.get(0);
		return sale;
	}

}
