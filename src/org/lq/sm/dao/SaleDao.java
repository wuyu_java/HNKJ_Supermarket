package org.lq.sm.dao;

import org.lq.sm.entity.Sale;
/**
 * 
 * @author 6组邹倩倩
 *	销售类数据接口
 */
public interface SaleDao extends BaseDao<Sale> {

}
