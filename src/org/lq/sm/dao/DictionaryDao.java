
package org.lq.sm.dao;

import java.util.List;

import org.lq.sm.entity.Dictionary;
/**
 * 
 * @author 岳鑫
 * @phone 159
 *@data 5:12:47
 *@package org.lq.sm.dao
 *	字典操作功能
 */
/**
 * 
 * @author 岳鑫
 * @phone 159
 *@data 5:12:47
 *@package org.lq.sm.dao
 *	字典操作功能
 */

public interface DictionaryDao extends BaseDao<Dictionary>{
	/**
	 * 	根据TypeID查询数据字典集合
	 * @param tid
	 * @return
	 */
	List<Dictionary> findDictionaryByTypeID(int tid);
	
	Dictionary getDictionaryByTypeIDAndName(String name,int tid);
	

}

