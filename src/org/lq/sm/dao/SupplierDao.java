package org.lq.sm.dao;

import java.util.List;

import org.lq.sm.entity.Supplier;
/**
 * 供应商接口
 * @author 马秋阳
 * @phone 14783803157
 * @date 2020年8月9日上午7:56:10
 * @package org.lq.sm.dao
 */
public interface SupplierDao extends BaseDao<Supplier> {
	
	
	/**
	 *	通过名称模糊查询
	 * @param name
	 * @return
	 */
	List<Supplier> likeSupplierByName(String name);

}
