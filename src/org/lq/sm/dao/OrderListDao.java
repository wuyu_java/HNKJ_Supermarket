package org.lq.sm.dao;
import java.sql.Date;
import java.util.List;

/**
 *
 */
import org.lq.sm.entity.OrderList;

public interface OrderListDao extends BaseDao<OrderList>{
	/**
	 * 按照类别查找
	 * @return
	 */
	public List<OrderList> getByTypeId(int tid);
	
	public List<OrderList> getOrderByDate(Date start,Date end);
}
