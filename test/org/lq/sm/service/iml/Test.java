package org.lq.sm.service.iml;

import java.awt.Font;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

public class Test {

	public static void main(String[] args) {
		ReportFormServiceImpl impl = new ReportFormServiceImpl();
		DefaultCategoryDataset dataset = impl.getInventory();
		//设置中文
		StandardChartTheme chartTheme = new StandardChartTheme("CN");
		//设置标题字体
		chartTheme.setExtraLargeFont(new Font("隶书", Font.BOLD, 20));
		//设置图例的字体
		chartTheme.setRegularFont(new Font("宋体", Font.PLAIN, 15));
		//设置纵向轴字体
		chartTheme.setLargeFont(new Font("宋体", Font.PLAIN, 15));

		//设置主题样式
		ChartFactory.setChartTheme(chartTheme);
		JFreeChart  chart = ChartFactory.createBarChart3D(
				"品牌商品数量统计", // 图表标题 
				"品牌名称", //目录轴的显示标题X
				"数量", //数值轴的显示标签Y
				dataset,//数据集
				PlotOrientation.VERTICAL,//图表的方向 : 水平,垂直
				true,//是否显示图例
				false,//是否生成工具(窗体中)
				false//是否生成URL连接 (网页中)
				);

		ChartFrame cf = new ChartFrame("水果销售报表分析", chart);
		cf.pack();
		cf.setVisible(true);
	}

}
