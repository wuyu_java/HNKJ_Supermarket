package org.lq.sm.chart;

import java.awt.Font;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.title.LegendTitle;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.general.DefaultPieDataset;
import org.lq.sm.service.iml.ReportFormServiceImpl;

public class PieCharDemo {
	
	public static void main(String[] args) {
		
		DefaultPieDataset dataset = new DefaultPieDataset();
		dataset.setValue("苹果", 100);
		dataset.setValue("梨子", 200);
		dataset.setValue("葡萄", 200);
		dataset.setValue("香蕉", 500);
		ReportFormServiceImpl impl = new ReportFormServiceImpl();
		JFreeChart chart = ChartFactory.createPieChart3D("水果产量图", impl.getTotalPrice(), 
				true,// 显示图例
				false, false);
		Font font = new Font("黑体", Font.BOLD, 20);
		Font font2 = new Font("宋体", Font.PLAIN, 15);
		
		LegendTitle legend = chart.getLegend();
		legend.setItemFont(font2);
		TextTitle title = chart.getTitle();
		title.setFont(font);
		PiePlot plot = (PiePlot) chart.getPlot();
		plot.setLabelFont(font2);
		//将报表显示到窗体中
		ChartFrame cf = new ChartFrame("饼形图", chart);
		cf.pack();
		cf.setVisible(true);
		
	}

}
