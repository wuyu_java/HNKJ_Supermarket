package org.lq.sm.chart;

import java.awt.Font;
import java.io.FileOutputStream;
import java.io.IOException;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

public class Demo {
	public static void main(String[] args) {
		/**
		 * 数据源
		 */
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		dataset.addValue(100, "北京", "苹果");
		dataset.addValue(120, "天津", "苹果");
		dataset.addValue(180, "新乡", "苹果");
		dataset.addValue(260, "北京", "西瓜");
		dataset.addValue(280, "天津", "西瓜");
		dataset.addValue(100, "新乡", "西瓜");
		dataset.addValue(60, "北京", "葡萄");
		dataset.addValue(360, "天津", "葡萄");
		dataset.addValue(320, "新乡", "葡萄");
		dataset.addValue(260, "北京", "香蕉");
		dataset.addValue(160, "天津", "香蕉");
		dataset.addValue(250, "新乡", "香蕉");
		dataset.addValue(100, "北京", "荔枝");
		dataset.addValue(200, "天津", "荔枝");
		dataset.addValue(300, "新乡", "荔枝");
		
		//设置中文
		StandardChartTheme chartTheme = new StandardChartTheme("CN");
		//设置标题字体
		chartTheme.setExtraLargeFont(new Font("隶书", Font.BOLD, 20));
		//设置图例的字体
		chartTheme.setRegularFont(new Font("宋体", Font.PLAIN, 15));
		//设置纵向轴字体
		chartTheme.setLargeFont(new Font("宋体", Font.PLAIN, 15));
		
		//设置主题样式
		ChartFactory.setChartTheme(chartTheme);
		JFreeChart  chart = ChartFactory.createBarChart3D(
				"水果产量图", // 图表标题 
				"水果", //目录轴的显示标题X
				"产量", //数值轴的显示标签Y
				dataset,//数据集
				PlotOrientation.VERTICAL,//图表的方向 : 水平,垂直
				true,//是否显示图例
				false,//是否生成工具(窗体中)
				false//是否生成URL连接 (网页中)
				);
		
//		try {
//			ChartUtilities.writeChartAsJPEG(new FileOutputStream("d:\\a.jpg"), chart, 800, 600);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}

		ChartFrame cf = new ChartFrame("水果销售报表分析", chart);
		cf.pack();
		cf.setVisible(true);
		
	}

}
