package org.lq.sm.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.google.gson.Gson;

public class HttpUtilTest {

	@Test
	public void test() {
//		String json = HttpUtil.getHTTP("6902088605655");
		String json = "{                  \"showapi_res_error\": \"\",                  \"showapi_res_id\": \"5f3250748d57ba656e8f19a3\",                  \"showapi_res_code\": 0,                  \"showapi_res_body\": {\"sptmImg\":\"\",\"spec\":\"105g\",\"remark\":\"查询成功！\",\"showapi_fee_code\":-1,\"img\":\"\",\"code\":\"6902088605655\",\"ycg\":\"\",\"manuName\":\"联合利华(中国)有限公司\",\"ret_code\":\"0\",\"imgList\":[],\"flag\":true,\"price\":\"\",\"trademark\":\"中华\",\"manuAddress\":\"安徽省合肥经济技术开发区锦绣大道88号\",\"note\":\"备注：经查，该商品条码已在中国物品编码中心注册,但用户未公开该数据。\",\"goodsName\":\"中华健齿白清新薄荷味牙膏105g\",\"goodsType\":\"服装、箱包、个人护理用品>>个人护理用品>>牙用护理品>>牙膏\"}                }                ";
		System.out.println(json);
		Gson gson = new Gson();
		HashMap map = gson.fromJson(json, new HashMap().getClass());
		Map body = (Map) map.get("showapi_res_body");
		System.out.println(body.get("manuName"));
		System.out.println(body.get("trademark"));
		System.out.println(body.get("manuAddress"));
		System.out.println(body.get("note"));
		System.out.println(body.get("goodsName"));
		System.out.println(body.get("goodsType"));
		System.out.println(body.get("price"));
		String s = "服装、箱包、个人护理用品>>个人护理用品>>牙用护理品>>牙膏";
		String[] ss = s.split(">>");
		System.out.println(Arrays.toString(ss));
	}

}
