package org.lq.sm.test;

import java.util.regex.Pattern;

public class Reg {
	
	public static void main(String[] args) {
		//\d 代表数字0~9  {代表重复的次数}
		// \w 代表 字母A-Z/a-z/0~9/_
		// . 代表任意字符  * : 代表的是0到多次 {0,}  
		String content = "汉字"; //条形码
		String pattern = "[\\u4e00-\\u9fa5]*";
		//判断字符串是否满足正则表达式语法中的描述
		boolean ismatch = Pattern.matches(pattern, content);
		System.out.println(ismatch);
		
		// \d 代表任意数字
		// \w [a-zA-Z0-9_]
		// . 代表任意字符
		//{} 重复次数
		// {1,3} 最少一次,最多3次
		//{1,}最少一次,最多没有限制
		//{5,}
		// * 代表重复0或任意多次  {0,}
		// + 代表最少1次,任意多次 {1,}
		// ? 可以0次或1次 {0,1}
		// [\\u4e00-\\u9fa5] 所有汉字
		
		
		
	}

}
