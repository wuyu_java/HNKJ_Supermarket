package org.lq.sm.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpDemo {
	
	public static void main(String[] args) {
		
		try {
			//创建远程URL连接对象
			URL url = new URL("https://codequery.market.alicloudapi.com/querybarcode?code=6944947300134");
			//通过远程URL连接对象打开一个连接,强制转成HttpURLConnection
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestProperty("Authorization", "APPCODE caa27356a5d14509853cd5e26d0a0956");
			//发送请求
			conn.connect();
//			if(200 == conn.getResponseCode()) {
				InputStream is = conn.getInputStream();
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				String line = "";
				while((line=br.readLine())!=null) {
					System.out.println(line);
				}
//			}
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
