package org.lq.sm.test;

import java.util.Arrays;

import org.junit.Test;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

public class pinyin {
	
	@Test
	public void test() {
//		String[] py = PinyinHelper.toHanyuPinyinStringArray('和');
//		System.out.println(Arrays.toString(py));
		//格式化
		HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
		format.setCaseType(HanyuPinyinCaseType.UPPERCASE);//转换成大写
		format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);//去除声调
		
		String str = "河南科技学院-abc";//hnkjxy
		String pinyin = "";
		for (int i = 0; i < str.length(); i++) {
			try {
				String [] py = PinyinHelper.toHanyuPinyinStringArray(str.charAt(i),format);
				String head = py[0];
				pinyin+=head.charAt(0);
			} catch (Exception e) {
//				e.printStackTrace();
				pinyin+=str.charAt(i);
			}
		}
		System.out.println(pinyin);
	}

}
