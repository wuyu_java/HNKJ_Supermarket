package org.lq.sm.test;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.lq.sm.util.AutoNo;

public class OrderNoDemo {

	public static void main(String[] args) {
		/*
		 * 202008121
		 * 202008122
		 * 202008123
		 * ...
		 * 
		 * 202008121N
		 * 
		 * 2020081312
		 * 2020081313
		 * 2020081314
		 * 
		 * 订单编号属于数据库
		 * 我们获取到订单编号最大的数字 + 1
		 * 
		 * 每次查询数据库
		 * 
		 * 将数字本地化
		 *  存储成一个文件
		 *	输出一个带类型的文件 
		 */
		
//		String date ="20200812";
//		int num = 1;
//		num++;
//		String no = date+num;
//		System.out.println(no);

		
		
		try {
			String dir = System.getProperty("user.dir");
			DataInputStream dis = new DataInputStream(new FileInputStream(dir+"/data/number.bat"));
			long no = dis.readLong();
			System.out.println(no);
			DataOutputStream oos = new DataOutputStream(new FileOutputStream(dir+"/data/number.bat"));
			oos.writeLong(1);
			oos.close();
			dis.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
//		
//		System.out.println(OrderListNo.getOrderListNo());
		
	}

}
