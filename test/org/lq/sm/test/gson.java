package org.lq.sm.test;

import java.lang.reflect.Field;
import java.util.HashMap;

import org.lq.sm.entity.Dictionary;

import com.google.gson.Gson;

public class gson {
	
	public static void main(String[] args) {
		
		Dictionary d = new Dictionary();
		d.setId(1001);
		d.setName("吃飯");
		d.setTypeid(1);
		
		
//		Class<? extends Dictionary> c = d.getClass();
//		StringBuilder sbu = new StringBuilder("{") ;
//		Field[] fields = c.getDeclaredFields();
//		for (Field field : fields) {
//			sbu.append("\""+field.getName()+"\":");
//			field.setAccessible(true);
//			try {
//				sbu.append("\""+field.get(d)+"\",");
//			} catch (IllegalArgumentException | IllegalAccessException e) {
//				e.printStackTrace();
//			}
//		}
//		
//		sbu.replace(sbu.length()-1, sbu.length(), "}");
//		
//		System.out.println(sbu);
		//將java对象转换字符,进行网络传输
//		System.out.println(d);
		//{"id":1001,"name":"吃飯","typeid":1}
		
		//{"属性":值,属性:值,属性:[值1,值2,值3...]}
		//GSON 是Google公司创建的一个java对象和json字符串之间进行转换的一个工具包
		Gson g = new Gson();
		String str = g.toJson(d);
		System.out.println(str);
//		
		String json = "{\"id\":1001,\"name\":\"吃飯\",\"typeid\":1}";
//		
//		System.out.println(json);
//		
		Dictionary dic = g.fromJson(json, Dictionary.class);
		System.out.println(dic.getId()+"\t"+dic.getName()+"\t"+dic.getTypeid());
		
		HashMap map = g.fromJson(json, new HashMap().getClass());
		System.out.println(map);
		System.out.println(map.get("name"));
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
