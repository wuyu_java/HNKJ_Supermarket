/*
 * 供应商表
 */
CREATE TABLE `supplier` (
  `id` int(11) NOT NULL auto_increment COMMENT '序号',
  `abbre` varchar(255) default NULL COMMENT '简称',
  `name` varchar(255) default NULL COMMENT '名称',
  `address` varchar(255) default NULL COMMENT '地址',
  `contacts` varchar(255) default NULL COMMENT '联系人',
  `Tnumber` varchar(20) default NULL COMMENT '电话号',
  `Cnumber` varchar(20) default NULL COMMENT '手机号',
  `ACnumber` varchar(20) default NULL COMMENT '银行卡号',
  `remarks` varchar(255) default NULL COMMENT '备注',
  PRIMARY KEY  (`id`)
) ;
/**
 * 员工表
 */
CREATE TABLE `employee` (
  `empid` int(11) NOT NULL AUTO_INCREMENT COMMENT '员工编号',
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `phone` varchar(255) DEFAULT NULL COMMENT '电话',
  `salary` decimal(10,2) DEFAULT NULL COMMENT '底薪',
  `commratio` varchar(255) DEFAULT NULL COMMENT '抽成比例',
  `time` datetime DEFAULT NULL COMMENT '入职时间',
  `authority` varchar(255) DEFAULT NULL COMMENT '权限',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`empid`)
) ;

/**
 * 字典表
 */
CREATE TABLE `dictionary` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `typeid` int(11) DEFAULT NULL COMMENT '类型编号',
  PRIMARY KEY (`id`)
) ;

/**
 * 销售表
 */
CREATE TABLE `sale` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '序号',
  `sale_id` int NOT NULL COMMENT '销售单号',
  `card_id` int NOT NULL COMMENT '会员卡号',
  `integral` int DEFAULT NULL COMMENT '积分',
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `level` int DEFAULT NULL COMMENT '级别',
  `this_integral` int DEFAULT NULL COMMENT '本次积分',
  `number_count` int DEFAULT NULL COMMENT '数量合计',
  `original_price_count` double(16,2) DEFAULT NULL COMMENT '原价合计',
  `discount` double(16,2) DEFAULT NULL COMMENT '优惠金额',
  `current_price_count` double(16,2) DEFAULT NULL COMMENT '实收金额',
  PRIMARY KEY (`id`) USING BTREE
);

/**
 * 会员表
 */
CREATE TABLE `vip` (
`no`  int(50) AUTO_INCREMENT COMMENT '序号' ,
`card`  int(50) NOT NULL COMMENT '卡号' ,
`name`  varchar(50) NULL COMMENT '姓名' ,
`gender`  varchar(2) NULL COMMENT '性别' ,
`birthday`  date NULL COMMENT '生日' ,
`phonenumber`  varchar(100) NULL COMMENT '电话号码' ,
`address`  varchar(50) NULL COMMENT '地址' ,
`level`  varchar(50) NULL COMMENT '级别' ,
`discount`  varchar(50) NULL COMMENT '折扣' ,
`integral`  varchar(50) NULL COMMENT '积分' ,
`handlecarddate`  datetime NULL COMMENT '办卡日期' ,
`baseamount`  varchar(50) NULL COMMENT '消费金额' ,
`numbersales`  int(50) NULL COMMENT '销售次数' ,
`description`  varchar(50) NULL COMMENT '备注' ,
PRIMARY KEY (`no`)
);

/**
 * 商品表
 */
CREATE TABLE `goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `goods_number` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '编号',
  `name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '名称',
  `code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '拼音简码',
  `category` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '类别',
  `brand` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '品牌',
  `unit` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '单位',
  `purchase_price` double(10,2) DEFAULT NULL COMMENT '进价',
  `sell_price` double(10,2) DEFAULT NULL COMMENT '售价',
  `trade_price` double(10,2) DEFAULT NULL COMMENT '批发价',
  `cost_price` double(10,2) DEFAULT NULL COMMENT '成本价',
  `inventory` int(10) DEFAULT NULL COMMENT '库存',
  `integral` int(10) DEFAULT NULL COMMENT '积分',
  `status` int(10) DEFAULT NULL COMMENT '是否下架',
  `max_inventory` int(10) DEFAULT NULL COMMENT '最大库存',
  `min_inventory` int(10) DEFAULT NULL COMMENT '最小库存',
  `purchase_num` int(10) DEFAULT NULL COMMENT '进货数量',
  `sell_num` int(10) DEFAULT NULL COMMENT '售出数量',
  PRIMARY KEY (`id`)
);

/**
 * 订单表
 */
CREATE TABLE `orderList` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `orderNo` int(20) DEFAULT NULL COMMENT '单号',
  `orderId` int(20) DEFAULT NULL COMMENT '编号',
  `orderName` varchar(255) DEFAULT NULL COMMENT '名称',
  `type` varchar(255) DEFAULT NULL COMMENT '类别',
  `brand` varchar(255) DEFAULT NULL COMMENT '品牌',
  `number` int(20) DEFAULT NULL COMMENT '数量',
  `pursePrice` double(10,2) DEFAULT NULL COMMENT '进价',
  `totalPrice` double(10,2) DEFAULT NULL COMMENT '进价总金额',
  `sellPrice` double(10,2) DEFAULT NULL COMMENT '售价',
  `tradePrice` double(10,2) DEFAULT NULL COMMENT '批发价',
  `date` timestamp NULL DEFAULT NULL COMMENT '日期',
  `supplier` varchar(255) DEFAULT NULL COMMENT '供货商',
  `onsale` double(255,2) DEFAULT NULL COMMENT '特价',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
);



/**
 * 订单
 */
CREATE TABLE `salesorder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编号',
  `eid` int(11) DEFAULT NULL COMMENT '销售员id',
  `vid` int(11) DEFAULT NULL COMMENT '会员编号',
  `gid` int(11) DEFAULT NULL COMMENT '商品id',
  `price` decimal(10,2) DEFAULT NULL COMMENT '价钱',
  `count` int(11) DEFAULT NULL COMMENT '数量',
  `totalPrice` decimal(10,2) DEFAULT NULL COMMENT '总价',
  `integral` int(11) DEFAULT NULL COMMENT '积分',
  `status` int(2) DEFAULT NULL COMMENT '0:代表是挂单,未结算 1 : 代表已经结算',
  `createdate` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '订单时间',
  PRIMARY KEY (`id`)
)

