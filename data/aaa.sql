-- MySQL dump 10.13  Distrib 5.7.27, for Win64 (x86_64)
--
-- Host: localhost    Database: hnkj_supermarket
-- ------------------------------------------------------
-- Server version	5.7.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `hnkj_supermarket`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hnkj_supermarket` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;

USE `hnkj_supermarket`;

--
-- Table structure for table `dictionary`
--

DROP TABLE IF EXISTS `dictionary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dictionary` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `typeid` int(11) DEFAULT NULL COMMENT '类型编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dictionary`
--

LOCK TABLES `dictionary` WRITE;
/*!40000 ALTER TABLE `dictionary` DISABLE KEYS */;
INSERT INTO `dictionary` VALUES (5,'食品',0),(6,'水果',0),(7,'王中王',1),(8,'康师傅',1),(9,'洗漱用品',0),(10,'斤',2),(11,'个',2),(12,'件',2),(13,'箱',2),(14,'联合利华',1),(15,'支',2),(16,'服装、箱包、个人护理用品',0),(17,'个人护理用品',0),(18,'牙用护理品',0),(19,'牙膏',0),(20,'中华',1),(21,'消杀用品',0),(23,'食品、饮料和烟草',0),(24,'饮料',0),(25,'非酒精饮料',0),(26,'软饮料',0),(27,'可口可乐零度',1),(28,'瓶',2),(29,'普通会员',3),(30,'银卡会员',3),(31,'金卡会员',3),(32,'砖石会员',3);
/*!40000 ALTER TABLE `dictionary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `empid` int(11) NOT NULL AUTO_INCREMENT COMMENT '员工编号',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '姓名',
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '电话',
  `salary` decimal(10,2) DEFAULT NULL COMMENT '底薪',
  `commratio` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '抽成比例',
  `time` datetime DEFAULT NULL COMMENT '入职时间',
  `authority` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '权限',
  `icon` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`empid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,'无语','13603738558',3000.00,'1','2020-08-10 00:00:00','1','icons/1597043824184-001.jpg','大萨达撒多撒'),(2,'李四','456789123',666.00,'1','2020-08-10 00:00:00','1','/icons/1597043938931-002.jpg','大萨达'),(3,'宋铀','666666',5000.00,'1','2020-08-10 00:00:00','1','/icons/1597044260033-003.jpg','大萨达多'),(4,'岳鑫','4567891523',4000.00,'0','2020-08-10 00:00:00','1','/icons/1597044328664-085849_8587.png','发的发生的'),(5,'测试22','11111',500.00,'1.0','2020-08-10 00:00:00','1','/icons/1597046647143-5de12f2.png','大萨达'),(6,'马秋阳','8888',2.00,'0','2020-08-10 00:00:00','1','/icons/1597044964729-005.jpg','的是非得失'),(7,'赵红娇','4564648',8888.00,'1','2020-08-10 15:37:29','1','/icons/1597045039641-0022.jpg','大萨达');
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `goods`
--

DROP TABLE IF EXISTS `goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `goods_number` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '编号',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '名称',
  `code` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '拼音简码',
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '商品图片',
  `category` int(11) DEFAULT NULL COMMENT '类别',
  `brand` int(11) DEFAULT NULL COMMENT '品牌',
  `unit` int(11) DEFAULT NULL COMMENT '单位',
  `purchase_price` double(10,2) DEFAULT NULL COMMENT '进价',
  `sell_price` double(10,2) DEFAULT NULL COMMENT '售价',
  `trade_price` double(10,2) DEFAULT NULL COMMENT '批发价',
  `cost_price` double(10,2) DEFAULT NULL COMMENT '成本价',
  `inventory` int(10) DEFAULT NULL COMMENT '库存',
  `integral` int(10) DEFAULT NULL COMMENT '积分',
  `status` int(10) DEFAULT NULL COMMENT '是否下架',
  `max_inventory` int(10) DEFAULT NULL COMMENT '最大库存',
  `min_inventory` int(10) DEFAULT NULL COMMENT '最小库存',
  `purchase_num` int(10) DEFAULT NULL COMMENT '进货数量',
  `sell_num` int(10) DEFAULT NULL COMMENT '售出数量',
  `manuName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '生产厂家',
  `manuAddress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '生产地址',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goods`
--

LOCK TABLES `goods` WRITE;
/*!40000 ALTER TABLE `goods` DISABLE KEYS */;
INSERT INTO `goods` VALUES (3,'4891338004145','黑人牙膏','HRYG',NULL,9,14,15,8.00,15.00,10.00,9.00,96,1,1,500,50,0,0,NULL,NULL,NULL),(4,'6902088605655','中华健齿白清新薄荷味牙膏105g','ZHJCBQXBHWYG105g',NULL,19,20,15,8.00,15.00,10.00,9.00,549,1,1,500,50,0,0,NULL,NULL,NULL),(6,'6928804018196','680ml可口可乐零度汽水','680mlKKKLLDQS',NULL,26,27,-1,1.50,3.00,2.50,2.00,94,1,1,1000,100,0,0,'太古中萃发展有限公司','北京市建国门外大街十九号国际大厦27层4号','firm_code：1000040000176；checkResult：1；备注：经查，该商品条码已在中国物品编码中心注册logout_flag：0；login_date：Nov 13 2013 12:00:00:000AM；valid_date：Nov 13 2019 12:00:00:000AM；关键字：680ml可口可乐零度汽水；上市时间：2019-04-01；英文名称：680ml Coke Zero；'),(7,'1234567891234','力士香皂','LSXZ','/goods-imgs/1597454243585play.png',17,8,11,1.00,3.00,1.50,1.30,500,1,1,1000,50,0,0,'','','啊啊啊啊'),(8,'665478941152','中华大牙膏','ZHDYG','/goods-imgs/1597454424423001.jpg',19,20,11,8.00,15.00,13.00,9.00,50,2,1,100,10,0,0,'中华','河南省新乡市','大萨达撒		'),(9,'65478912354','整箱可口可乐','ZXKKKL',NULL,26,27,13,60.00,80.00,75.00,70.00,-3,5,1,500,10,0,0,'','','');
/*!40000 ALTER TABLE `goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orderlist`
--

DROP TABLE IF EXISTS `orderlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orderlist` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `orderNo` int(20) DEFAULT NULL COMMENT '单号',
  `gid` int(20) DEFAULT NULL COMMENT '商品编号',
  `orderName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '类别',
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '品牌',
  `number` int(20) DEFAULT NULL COMMENT '数量',
  `pursePrice` double(10,2) DEFAULT NULL COMMENT '进价',
  `totalPrice` double(10,2) DEFAULT NULL COMMENT '进价总金额',
  `sellPrice` double(10,2) DEFAULT NULL COMMENT '售价',
  `tradePrice` double(10,2) DEFAULT NULL COMMENT '批发价',
  `date` timestamp NULL DEFAULT NULL COMMENT '日期',
  `supplier` int(11) DEFAULT NULL COMMENT '供货商',
  `onsale` double(255,2) DEFAULT NULL COMMENT '特价',
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orderlist`
--

LOCK TABLES `orderlist` WRITE;
/*!40000 ALTER TABLE `orderlist` DISABLE KEYS */;
INSERT INTO `orderlist` VALUES (2,202008122,6,'无语','软饮料','可口可乐零度',100,1.50,150.00,3.00,2.50,'2019-07-01 12:54:04',2,NULL,'firm_code：1000040000176；checkResult：1；备注：经查，该商品条码已在中国物品编码中心注册logout_flag：0；login_date：Nov 13 2013 12:00:00:000AM；valid_date：Nov 13 2019 12:00:00:000AM；关键字：680ml可口可乐零度汽水；上市时间：2019-04-01；英文名称：680ml Coke Zero；'),(3,202008123,4,'无语','牙膏','中华',500,8.00,4000.00,15.00,10.00,'2020-08-12 13:04:10',1,NULL,''),(4,202008124,3,'无语','洗漱用品','联合利华',100,8.00,800.00,15.00,10.00,'2020-07-12 13:04:47',1,NULL,''),(5,2020081440,4,'无语','牙膏','中华',50,8.00,400.00,15.00,10.00,'2020-08-14 06:16:23',1,NULL,'');
/*!40000 ALTER TABLE `orderlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sale`
--

DROP TABLE IF EXISTS `sale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sale` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `sale_id` int(11) NOT NULL COMMENT '销售单号',
  `card_id` int(11) NOT NULL COMMENT '会员卡号',
  `integral` int(11) DEFAULT NULL COMMENT '积分',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '姓名',
  `level` int(11) DEFAULT NULL COMMENT '级别',
  `this_integral` int(11) DEFAULT NULL COMMENT '本次积分',
  `number_count` int(11) DEFAULT NULL COMMENT '数量合计',
  `original_price_count` double(16,2) DEFAULT NULL COMMENT '原价合计',
  `discount` double(16,2) DEFAULT NULL COMMENT '优惠金额',
  `current_price_count` double(16,2) DEFAULT NULL COMMENT '实收金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sale`
--

LOCK TABLES `sale` WRITE;
/*!40000 ALTER TABLE `sale` DISABLE KEYS */;
/*!40000 ALTER TABLE `sale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salesorder`
--

DROP TABLE IF EXISTS `salesorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salesorder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编号',
  `eid` int(11) DEFAULT NULL COMMENT '销售员id',
  `vid` int(11) DEFAULT NULL COMMENT '会员编号',
  `gid` int(11) DEFAULT NULL COMMENT '商品id',
  `price` decimal(10,2) DEFAULT NULL COMMENT '价钱',
  `count` int(11) DEFAULT NULL COMMENT '数量',
  `totalPrice` decimal(10,2) DEFAULT NULL COMMENT '总价',
  `integral` int(11) DEFAULT NULL COMMENT '积分',
  `status` int(2) DEFAULT NULL COMMENT '0:代表是挂单,未结算 1 : 代表已经结算',
  `createdate` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '订单时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salesorder`
--

LOCK TABLES `salesorder` WRITE;
/*!40000 ALTER TABLE `salesorder` DISABLE KEYS */;
INSERT INTO `salesorder` VALUES (1,'2020081436',1,3,4,15.00,1,15.00,1,1,'2020-08-14 14:33:05'),(2,'2020081436',1,3,3,15.00,1,15.00,1,1,'2020-08-14 14:33:05'),(3,'2020081436',1,3,6,3.00,1,3.00,1,1,'2020-08-14 14:33:05'),(4,'2020081438',1,2,3,15.00,3,45.00,3,1,'2020-08-14 14:33:05'),(5,'2020081443',-1,0,6,3.00,4,12.00,4,1,'2020-08-14 14:33:05'),(12,'2020081478',-1,0,6,3.00,1,3.00,1,0,'2020-08-14 15:48:03'),(13,'2020081478',-1,0,3,15.00,1,15.00,1,0,'2020-08-14 15:48:04'),(14,'2020081582',2,3,6,3.00,1,3.00,1,1,'2020-08-15 08:58:58'),(16,'2020081587',-1,0,9,80.00,1,80.00,5,1,'2020-08-15 11:54:54'),(17,'2020081587',-1,0,9,80.00,1,80.00,5,1,'2020-08-15 11:54:55'),(18,'20200817107',-1,0,9,80.00,1,80.00,5,1,'2020-08-17 10:51:46');
/*!40000 ALTER TABLE `salesorder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `abbre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '简称',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '地址',
  `contacts` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '联系人',
  `Tnumber` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '电话号',
  `Cnumber` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '手机号',
  `ACnumber` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '银行卡号',
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier`
--

LOCK TABLES `supplier` WRITE;
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
INSERT INTO `supplier` VALUES (1,'盼盼','福建省长汀盼盼食品有限公司','福建省泉州市','蔡丕鹏',' 05976810880','12345678912','6666666','大萨达'),(2,'达利园2','福建达利食品集团有限公司','福建2','许世辉',' 0595-87335077','123456789','666666','大萨达撒');
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_brandcount`
--

DROP TABLE IF EXISTS `v_brandcount`;
/*!50001 DROP VIEW IF EXISTS `v_brandcount`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_brandcount` AS SELECT 
 1 AS `name`,
 1 AS `num`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_goodstypecount`
--

DROP TABLE IF EXISTS `v_goodstypecount`;
/*!50001 DROP VIEW IF EXISTS `v_goodstypecount`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_goodstypecount` AS SELECT 
 1 AS `name`,
 1 AS `num`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_inventory`
--

DROP TABLE IF EXISTS `v_inventory`;
/*!50001 DROP VIEW IF EXISTS `v_inventory`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_inventory` AS SELECT 
 1 AS `id`,
 1 AS `NAME`,
 1 AS `inventory`,
 1 AS `s_count`,
 1 AS `o_count`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_totalprice`
--

DROP TABLE IF EXISTS `v_totalprice`;
/*!50001 DROP VIEW IF EXISTS `v_totalprice`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_totalprice` AS SELECT 
 1 AS `goodsintotalprice`,
 1 AS `saletotalprice`,
 1 AS `purchasetotalprice`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `vip`
--

DROP TABLE IF EXISTS `vip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vip` (
  `no` int(50) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `card` int(50) NOT NULL COMMENT '卡号',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '姓名',
  `gender` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '性别',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `phonenumber` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '电话号码',
  `address` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '地址',
  `level` int(11) DEFAULT NULL COMMENT '级别',
  `discount` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '折扣',
  `integral` int(11) DEFAULT NULL COMMENT '积分',
  `handlecarddate` datetime DEFAULT NULL COMMENT '办卡日期',
  `baseamount` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '消费金额',
  `numbersales` int(50) DEFAULT NULL COMMENT '销售次数',
  `description` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`no`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vip`
--

LOCK TABLES `vip` WRITE;
/*!40000 ALTER TABLE `vip` DISABLE KEYS */;
INSERT INTO `vip` VALUES (1,10007,'王五',NULL,'2020-08-13','13603738558','河南省新乡市',29,'1',0,'2020-08-13 10:09:31',NULL,0,'没啥后台!'),(2,10008,'无语',NULL,'2020-08-13','13603738558','河南省新乡市牧野区',32,'10',500,'2020-08-13 10:10:19',NULL,0,'很有后台'),(3,10009,'李四',NULL,'2020-08-13','123456789','啊啊啊',30,'1',0,'2020-08-13 10:18:56',NULL,0,'啊啊啊');
/*!40000 ALTER TABLE `vip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `hnkj_supermarket`
--

USE `hnkj_supermarket`;

--
-- Final view structure for view `v_brandcount`
--

/*!50001 DROP VIEW IF EXISTS `v_brandcount`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_brandcount` AS select `dictionary`.`name` AS `name`,count(1) AS `num` from (`goods` join `dictionary`) where ((`dictionary`.`typeid` = 1) and (`dictionary`.`id` = `goods`.`brand`)) group by `dictionary`.`name` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_goodstypecount`
--

/*!50001 DROP VIEW IF EXISTS `v_goodstypecount`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_goodstypecount` AS select `dictionary`.`name` AS `name`,count(1) AS `num` from (`goods` join `dictionary`) where ((`dictionary`.`typeid` = 0) and (`dictionary`.`id` = `goods`.`category`)) group by `dictionary`.`name` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_inventory`
--

/*!50001 DROP VIEW IF EXISTS `v_inventory`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_inventory` AS select `g`.`id` AS `id`,`g`.`name` AS `NAME`,`g`.`inventory` AS `inventory`,`a`.`s_count` AS `s_count`,`b`.`o_count` AS `o_count` from ((((select `hnkj_supermarket`.`salesorder`.`gid` AS `gid`,sum(`hnkj_supermarket`.`salesorder`.`count`) AS `s_count` from `hnkj_supermarket`.`salesorder` where (`hnkj_supermarket`.`salesorder`.`status` = 1) group by `hnkj_supermarket`.`salesorder`.`gid`)) `a` join (select `hnkj_supermarket`.`orderlist`.`gid` AS `gid`,sum(`hnkj_supermarket`.`orderlist`.`number`) AS `o_count` from `hnkj_supermarket`.`orderlist` group by `hnkj_supermarket`.`orderlist`.`gid`) `b`) join `hnkj_supermarket`.`goods` `g`) where ((`a`.`gid` = `b`.`gid`) and (`g`.`id` = `a`.`gid`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_totalprice`
--

/*!50001 DROP VIEW IF EXISTS `v_totalprice`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_totalprice` AS select `a`.`goodsintotalprice` AS `goodsintotalprice`,`b`.`saletotalprice` AS `saletotalprice`,`c`.`purchasetotalprice` AS `purchasetotalprice` from ((((select sum(`hnkj_supermarket`.`orderlist`.`totalPrice`) AS `goodsintotalprice` from `hnkj_supermarket`.`orderlist`)) `a` join (select sum(`hnkj_supermarket`.`salesorder`.`totalPrice`) AS `saletotalprice` from `hnkj_supermarket`.`salesorder` where (`hnkj_supermarket`.`salesorder`.`status` = 1)) `b`) join (select sum((`hnkj_supermarket`.`salesorder`.`count` * `hnkj_supermarket`.`goods`.`purchase_price`)) AS `purchasetotalprice` from (`hnkj_supermarket`.`salesorder` join `hnkj_supermarket`.`goods`) where (`hnkj_supermarket`.`salesorder`.`gid` = `hnkj_supermarket`.`goods`.`id`)) `c`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-17 15:30:15
